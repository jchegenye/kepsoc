<?php

namespace JCHEGENYE;

/*use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

class User extends Eloquent  implements Authenticatable
{
    use AuthenticableTrait;*/

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Auth\Passwords\CanResetPassword;

class User extends Eloquent  implements Authenticatable, \Illuminate\Contracts\Auth\CanResetPassword
{

    use AuthenticableTrait,CanResetPassword;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    

    public function roles(){}

    public function hasRole($name){}

    public function can($permission){} 

    public function ability($roles, $permissions, $options){}

    public function attachRole($role){}
}
