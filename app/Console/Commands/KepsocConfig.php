<?php

namespace JCHEGENYE\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use JCHEGENYE\JTech\Model\Admin\Workbench\UserPermission;
use JCHEGENYE\JTech\Model\Users\User;
use JCHEGENYE\JTech\ReusableCodes\DateFormats;
use JCHEGENYE\JTech\ReusableCodes\GenerateVerificationCode;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpFoundation\Request;

class KepsocConfig extends Command{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'kepsoc:initialise';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialise Kepsoc website';

    /**
     * KepsocCong constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @param Request $code
     */
    public function fire(Request $code)
    {
        $this->info('Initialising the website ...');

        $file = app_path().'/initialise.yml';

        if ( ! file_exists($file))
        {
            $this->error('The file initialise.yml does not exist!');
        }
        else{
            //Load the YAML file and parse it.
            $array = Yaml::parse(file_get_contents($file));

            foreach ($array as $key => $value) {
                $name = $key;

                //Lets loop through the value array to get the other details.
                $root_name = $value['name'];
                $root_uid = $value['uid'];
                $root_username = $value['username'];
                $root_email = $value['email'];
                $root_password = $value['password'];

                //Check if Root user already exists
                $query = User::where('email','=','chegenyejackson@gmail.com')->first();

                //Also fetch the permissions
                $permissions = $this->getAllPermissions();

                //Lets Get the DATES class we had set
                $date = new DateFormats();
                $Date1 = $date->date();

                //Lets as well get the auto-generated code from a class we created
                $new_code = new GenerateVerificationCode();
                $code = $new_code->generateVerifyCode($code);

                //Only create a root user non existing.
                if (empty($query)) {
                    $this->info('Root user does not exist. We are creating one ...');

                        $user = new User;

                            $user->name = $root_name;
                            $user->uid = $root_uid;
                            $user->username = $root_username;
                            $user->email = $root_email;
                            $user->password = Hash::make($root_password);

                            $user->role = [
                                'member_role',
                                'access_to_members_list',
                                'access_to_member_profile',
                                'access_to_admin_routes',
                                'access_to_workbench',
                                'can_give_permissions',
                                'can_approve_a_member',
                                'can_lock_user',
                                'can_delete_an_account'
                            ];
                            $user->user_status = 'member';
                            $user->signed_date = $Date1['Date1'];
                            $user->verification_token = $code;
                            $user->confirmation_code = '1';

                        $user->save();

                    $this->info('Root created ' . $name);
                }else{
                    $this->info('Root user exists. We are updating permissions ...');

                    $user = User::where('email','=','chegenyejackson@gmail.com')
                        ->first();
                    $user->uid = $root_uid;
                    $user->role = [
                        'member_role',
                        'access_to_members_list',
                        'access_to_member_profile',
                        'access_to_admin_routes',
                        'access_to_workbench',
                        'can_give_permissions',
                        'can_approve_a_member',
                        'can_lock_user',
                        'can_delete_an_account'
                    ];
                    $user->user_status = 'member';
                    $user->save();

                }
            }
        }
    }

    public function getAllPermissions(){


    }


}