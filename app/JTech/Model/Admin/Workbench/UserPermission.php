<?php namespace JCHEGENYE\JTech\Model\Admin\Workbench;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class UserPermission extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['machine_name'];

    /**
     * The database collection used by the model.
     *
     * @var string
     */
    protected $table = "permissions";

}