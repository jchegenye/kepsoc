<?php namespace JCHEGENYE\JTech\Model\Admin\Workbench;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Role extends Eloquent
{

    /**
     * The database collection used by the model.
     *
     * @var string
     */
    protected $collection = "roles";

}