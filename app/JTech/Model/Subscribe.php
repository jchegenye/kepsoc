<?php namespace JCHEGENYE\JTech\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Subscribe extends Eloquent
{
	
/**
	* The database collection used by the model.
    *
    * @var string
    */
    protected $collection = "subscriptions";

}
