<?php namespace JCHEGENYE\JTech\Model\Photos;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class ProfilePhoto extends Eloquent implements StaplerableInterface{
    use EloquentTrait;

    protected $collection = 'profile_photos';

    protected $fillable = ['profile_pic','user_id'];

    public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('profile_pic', [
            'styles' => [
                'medium' => '300x300',
                'display' => '200x200',
                'thumb' => '100x100'
            ]
        ]);

        parent::__construct($attributes);
    }
}