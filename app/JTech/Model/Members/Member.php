<?php namespace JCHEGENYE\JTech\Model\Members;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Member extends Eloquent implements StaplerableInterface {
    use EloquentTrait;
    use SoftDeletes;

    /**
     * Lets use Soft Delete since we do not want to delete records permanently
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The connection name for the model.
     *
     * @var string
     */

    protected $collection = "members";

    /**
     * Member constructor for Photos
     * @param array $attributes
     */
    public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('profile_pic', [
            'styles' => [
                'medium' => '300x300',
                'thumb' => '100x100'
            ]
        ]);

        parent::__construct($attributes);
    }

}