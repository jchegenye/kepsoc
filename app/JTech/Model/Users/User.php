<?php namespace JCHEGENYE\JTech\Model\Users;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Eloquent {
    use SoftDeletes;

    /**
     * Lets use Soft Delete since we do not want to delete records permanently
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    //only allow the following items to be mass-assigned to our model
    protected $fillable = array('uid');

    /**
     * The database collection used by the model.
     *
     * @var string
     */
    protected $collection = "users";

}