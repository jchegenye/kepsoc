<?php namespace JCHEGENYE\JTech\Reports {

    use JCHEGENYE\JTech\Model\Subscribe;
    use JCHEGENYE\JTech\Model\Users\User;

    /**
     * @author Jackson Asumu Chegenye
     *         0711494289
     *         chegenyejackson@gmail.com
     * @version 0.0.1
     * @copyright 2015-2016 j-tech.tech
     *
     * @File Handles Statistics
     */

    class Statistics {

        /**
         * @return array of Statistics
         */
        public function countStatistics(){

            //Get all users
                $allUsers = User::all();
            //Get verified and un-verified users
                $verifiedUser = User::where('confirmation_code', '=', '1')->get();
                $unverifiedUser = User::where('confirmation_code', '=', '0')->get();
            //Get verified and un-verified subscribers
                $verifiedSubscriber = Subscribe::where('confirmation_code', '=', '1')->get();
                $unverifiedSubscriber = Subscribe::where('confirmation_code', '=', '0')->get();

            //Get all Members
                $all_members = User::whereIn('role', ['can_access_member_list'])
                    ->paginate(5);

            //Get all Subscribers
                $all_subscribers = Subscribe::all();

            //Query all Statistics Count
                $statistics = [
                    'all_users' => $allUsers,
                    'all_member' => $all_members,
                    'all_subscriber' => $all_subscribers,
                    'verified' =>  $verifiedUser,
                    'unverified' =>  $unverifiedUser,
                    'verifiedSubscriber' => $verifiedSubscriber,
                    'unverifiedSubscriber' => $unverifiedSubscriber,

                ];

                return $statistics;
        }

    }
}