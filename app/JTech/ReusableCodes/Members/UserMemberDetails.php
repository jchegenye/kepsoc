<?php namespace JCHEGENYE\JTech\ReusableCodes\Members{

    use Illuminate\Support\Facades\Auth;
    use JCHEGENYE\JTech\Model\Members\Member;
    use JCHEGENYE\JTech\Model\Users\User;

    /**
     * @author Jackson Asumu Chegenye
     *         0711494289
     *         chegenyejackson@gmail.com
     * @version 0.0.1
     * @copyright 2015-2016 j-tech.tech
     *
     * @File returns member's details/data/information . Can be used GLOBALLY in this project
     */

    class UserMemberDetails {

        /**
         * This function returns individual logged in user/member
         * @return array
         */
        public function getUserDetails(){

            $users = Auth::user();
            $userDetails = [
                'user' => $users,
            ];
            return $userDetails;
        }

        /**
         * This function returns all members updated profile + signed details
         * @return array
         */
        public function getMembersDetails(){

            //Get an array of users by 'uid' alone

            //Return an array of profiles where mid = uid

            //if this is successful, then members statistics will be generated form here

            $members = Member::orderBy('mid', 'ASC')->paginate(10);
            $users = User::where('user_status', '=', 'member')->get();

            $data = [
                'member' => $members,
                'profile' => $users
            ];
            return $data;
        }

        public function getAllUserDetails(){

            $users = User::orderBy('uid', 'DES')->paginate(5);

            $data = [
                'users' => $users
            ];

            return $data;

        }

        
    }
}