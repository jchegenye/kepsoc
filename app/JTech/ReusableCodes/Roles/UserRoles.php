<?php namespace JCHEGENYE\JTech\ReusableCodes\Roles {

    /**
     * @author Jackson Asumu Chegenye
     *         0711494289
     *         chegenyejackson@gmail.com
     * @version 0.0.1
     * @copyright 2015-2016 j-tech.tech
     *
     * @File Handles user roles
     */

    class UserRoles {
        /**
         * Here, we set all our Roles Here.
         *
         **/
        public function roles(){

            $roles = array(
                'super_admin_role' => 'super_admin',
                'member_role' => 'member'
            );

            return $roles;
        }
    }
}