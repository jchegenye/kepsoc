<?php namespace JCHEGENYE\Http\Controllers\Gallery;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;
use JCHEGENYE\Http\Controllers\Controller;
use JCHEGENYE\JTech\Model\Users\User;

class GalleryController extends Controller{

    public function getGalleryPage(){


        $photosGrpOne = array(
            'photo1' => File::get('images/gallery/all01.jpg'),
            'photo2' => File::get('images/gallery/all02.jpg'),
            'photo3' => File::get('images/gallery/all03.jpg'),
            'photo4' => File::get('images/gallery/all04.jpg'),
            'photo5' => File::get('images/gallery/all05.jpg'),
            'photo6' => File::get('images/gallery/all06.jpg'),
            'photo7' => File::get('images/gallery/all07.jpg'),
            'photo8' => File::get('images/gallery/all08.jpg'),
            'photo9' => File::get('images/gallery/all09.jpg'),
            'photo10' => File::get('images/gallery/all10.jpg'),
        );

        $getPhotos = [
            'galleryPhotosGrpOne' => $photosGrpOne,
        ];

        return view('layouts/pages/gallery')->with($getPhotos);
    }


}