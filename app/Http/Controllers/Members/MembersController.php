<?php namespace JCHEGENYE\Http\Controllers\Members;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use JCHEGENYE\Http\Controllers\Controller;
use JCHEGENYE\JTech\Model\Members\Member;
use JCHEGENYE\JTech\Model\Users\User;
use JCHEGENYE\JTech\ReusableCodes\DateFormats;
use JCHEGENYE\JTech\ReusableCodes\Members\UserMemberDetails;

/**
 * @author Jackson Asumu Chegenye
 *         0711494289
 *         chegenyejackson@gmail.com
 * @version 0.0.1
 * @copyright 2015-2016 j-tech.tech
 *
 * @File Handles Members
 */

class MembersController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
        
    }

    /**
     * Show THE MEMBERS DASHBOARD
     * @return $this
     */
    public function membersDashboard(){

        $getUser = new  UserMemberDetails();
        $memberData = $getUser->getUserDetails();

        Session::flash('successful', $this->welcomeMessage());
        return View('layouts.pages.members.members-dashboard')->with($memberData);

    }

    /**
     * Show THE MEMBERS LIST
     * @return $this
     */
    public function membersList(){

        if(Auth::user()->user_status == 'member'){

            $getUsersAndMembers = new  UserMemberDetails();
            $userData = $getUsersAndMembers->getUserDetails();
            $memberData = $getUsersAndMembers->getMembersDetails();

            Session::flash('successful', $this->welcomeMessage());
            return view('layouts.pages.members.members-list')->with($userData)->with($memberData);

        }else{

            Session::flash('unauthorised', $this->notMember());
            return redirect()->back();
        }

    }

    /**
     * Show THE MEMBERS PROFILE
     * @param $uid
     * @return $this
     */
    public function memberProfile($uid){

        if(Auth::user()->user_status == 'member'){

            $user = User::where('uid', '=', (int)$uid)
                ->where('confirmation_code','=','1')
                ->first();

            $members = Member::where('uid', '=', $uid)
                ->first();

            return View('layouts.pages.members.member-profile')
                ->with(['memberProfile' => $user, 'updatedMemberProfile' => $members]);

        }else{

            Session::flash('unauthorised', $this->notMember());
            return redirect()->back();

        }

    }

    public function editProfilePage($uid){

        if(Auth::user()->uid  == $uid){

            $user = User::where('uid', '=', (int)$uid)
                ->where('confirmation_code','=','1')
                ->first();

            $members = Member::where('uid', '=', $uid)
                ->first();

            return View('layouts.pages.members.profile.edit_profile_page')
                ->with(['memberProfile' => $user, 'updatedMemberProfile' => $members]);

        }else{

            Session::flash('unauthorised', $this->notAuthorised());
            return Redirect::to('members/dash');

        }


    }


    public function storeProfile(Request $request , $uid){

        $members = Member::where('uid', '=', $uid)->first();
        if(empty($members->education_percentage)) {

            $validator1 = Validator::make($request->all(), [
                'course' => 'required',
                'institution' => 'required',
                'study_start_date' => 'required',
                /*'study_start_date' => 'required|date_format:Y-m-d|before:study_end_date',*/
                /*'study_end_date' => 'date_format:Y-m-d',*/
                'privacy_study' => 'required',
                'education_description' => 'required',
            ]);

            if ($validator1->passes()) {

                $date = new DateFormats();
                $Date1 = $date->date();
                $members_mid = Member::orderBy('mid', 'DESC')->take(1)->get();

                $member = new Member;
                //Lets manually generate unique MEMBER ID and store it
                if ($members_mid->isEmpty()) {
                    $member->mid = 1;
                } else {
                    foreach ($members_mid as $count) {
                        $mid = $count->mid;
                        $member->mid = $mid + 1;
                    }
                }

                $member->uid = Input::get('uid');
                $member->percentage = 20;
                $member->education_percentage = 20;
                $member->profile_created_date = $Date1['Date1'];
                $member->course = Input::get('course');
                $member->institution = Input::get('institution');
                $member->study_start_date = Input::get('study_start_date');
                $member->study_end_date = Input::get('study_end_date');
                $member->graduated = Input::get('graduated');
                $member->privacy_study = Input::get('privacy_study');
                $member->education_description = Input::get('education_description');
                $member->save();

                Session::flash('profile_successful', $this->successfulUpdate());
                return redirect()->back();

            }else{

                return redirect()->back()->withErrors($validator1)->withInput();
            }


        }elseif($members->percentage == 20){

            $validator2 = Validator::make($request->all(), [
                'work_place' => 'required|min:3',
                'position' => 'required|min:3',
                'start_work_date' => 'required',
                /*'start_work_date' => 'required|date_format:Y-m-d|before:end_work_date',
                'end_work_date' => 'required|date_format:Y-m-d',*/
                'privacy_work' => 'required',
            ]);

            if ($validator2->passes()) {

                $date = new DateFormats();
                $Date1 = $date->date();

                $members = Member::where('uid', '=', $uid)->first();
                if ($members->percentage == 20) {
                    $members->percentage = $members->percentage + 20;
                }

                $members->work_percentage = $members->education_percentage + 20;
                $members->profile_created_date = $Date1['Date1'];
                $members->position = Input::get('position');
                $members->work_place = Input::get('work_place');
                $members->start_work_date = Input::get('start_work_date');
                $members->end_work_date = Input::get('end_work_date');
                $members->current_work = Input::get('current_work');
                $members->privacy_work = Input::get('privacy_work');
                $members->save();

                Session::flash('profile_successful', $this->successfulUpdate());
                return redirect()->back();

            }else{
                return redirect()->back()->withErrors($validator2)->withInput();
            }

        }elseif($members->percentage == 40){

            $validator3 = Validator::make($request->all(), [
                'skills' => 'required|min:10',
                'privacy_professional' => 'required',
            ]);

            if ($validator3->passes()) {

                $date = new DateFormats();
                $Date1 = $date->date();

                $members = Member::where('uid', '=', $uid)->first();
                if($members->percentage == 40){
                    $members->percentage = $members->percentage+20;
                }

                $members->profile_created_date = $Date1['Date1'];
                $members->proffessional_percentage = $members->work_percentage+20;
                $members->skills = Input::get('skills');
                $members->privacy_professional = Input::get('privacy_professional');
                $members->save();

                Session::flash('profile_successful', $this->successfulUpdate());
                return redirect()->back();
            }else{
                return redirect()->back()->withErrors($validator3)->withInput();
            }

        }elseif($members->percentage == 60){

            $validator4 = Validator::make($request->all(), [
                'phone_number' => 'required|numeric|min:10',
                'profile_pic' => 'required|mimes:jpeg,bmp,png,jpg|image|max:1024',
                'residence' => 'required',
                'languages' => 'required',
                'gender' => 'required',
            ]);

            if ($validator4->passes()) {

                $date = new DateFormats();
                $Date1 = $date->date();

                $members = Member::where('uid', '=', $uid)->first();
                if($members->percentage == 60){
                    $members->percentage = $members->percentage+20;
                }

                $members->profile_created_date = $Date1['Date1'];
                $members->basic_percentage = $members->proffessional_percentage+20;
                $members->phone_number = Input:: get('phone_number');
                $members->profile_pic = Input::file('profile_pic');
                $members->residence = Input:: get('residence');
                $members->birthday = Input:: get('birthday');
                $members->languages = Input:: get('languages');
                $members->gender = Input:: get('gender');
                $members->save();

                Session::flash('profile_successful', $this->successfulUpdate());
                return redirect()->back();
            }else{
                return redirect()->back()->withErrors($validator4)->withInput();
            }

        }elseif($members->percentage == 80) {

            $validator5 = Validator::make($request->all(), [
                'about_you' => 'required|min:10',
            ]);

            if ($validator5->passes()) {

                $date = new DateFormats();
                $Date1 = $date->date();

                $members = Member::where('uid', '=', $uid)->first();
                if ($members->percentage == 80) {
                    $members->percentage = $members->percentage + 20;
                }

                $members->profile_created_date = $Date1['Date1'];
                $members->about_you_percentage = $members->basic_percentage + 20;
                $members->about_you = Input::get('about_you');
                $members->save();

                Session::flash('profile_successful', $this->successfulUpdate());
                return redirect()->back();
            }else{
                return redirect()->back()->withErrors($validator5)->withInput();
            }
        }

    }

    /**
     * Displays Strings set in Lang
     * @return string
     */
    protected function welcomeMessage(){
        return Lang::has('users.members.successful_profile_update')
            ? Lang::get('users.members.successful_profile_update') :
            'Welcome, ';
    }
    protected function successfulUpdate(){
        return Lang::has('users.members.successful_profile_update')
            ? Lang::get('users.members.successful_profile_update') :
            'Your profile has been updated!';
    }
    protected function notAuthorised(){
        return Lang::has('users.members.unauthorised')
            ? Lang::get('users.members.unauthorised') :
            'You are not authorized to access this resource!';
    }
    protected function notMember(){
        return Lang::has('users.members.not_member')
            ? Lang::get('users.members.not_member') :
            'You are not authorized to access this resource since your not a member!';
    }

    protected function alreadyUpdated(){
        return Lang::has('users.members.already_updated')
            ? Lang::get('users.members.already_updated') :
            'Your profile is already updated!';
    }
    protected function completeEducationFirst(){
        return Lang::has('users.members.complete_edu_first')
            ? Lang::get('users.members.complete_edu_first') :
            'Kindly complete the EDUCATION DETAILS first!';
    }
    protected function unsuccessfulUpdate(){
        return Lang::has('users.members.unsuccessful_profile_update')
            ? Lang::get('users.members.unsuccessful_profile_update') :
            'Sorry, your profile could NOT be updated!';
    }

}