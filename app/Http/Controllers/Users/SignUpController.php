<?php namespace JCHEGENYE\Http\Controllers\Users;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

use JCHEGENYE\Http\Controllers\Controller;
use JCHEGENYE\JTech\Model\Users\User;
use JCHEGENYE\JTech\ReusableCodes\DateFormats;
use JCHEGENYE\JTech\ReusableCodes\GenerateVerificationCode;
use Exception;
use JCHEGENYE\JTech\ReusableCodes\Roles\UserRoles;
use Symfony\Component\HttpFoundation\Request;
/**
     * @author Jackson Asumu Chegenye
    *         0711494289
    *         chegenyejackson@gmail.com
    * @version 0.0.1
    * @copyright 2015-2016 j-tech.tech
    *
    * @File Handles SignUps
    */

class SignUpController extends Controller {

    public function registerPage(){

        return view('auth.register');

    }

    /**
     * @Function Registers users
     * @return mixed
     */
    public function registerUser(Request $code){

        $rules = [
            'name' => 'required|unique:users,name|min:4',
            'email' => 'email|unique:users,email|required',
            'username' => 'required|unique:users,username|min:6|max:20',
            'password' => 'required|min:6|max:20|unique:users,password',
            'password_confirmation' => 'required|same:password',
            'g-recaptcha-response' => 'required',
        ];

        $validator = Validator::make (Input::all(), $rules);

        if ($validator -> passes()) {

            //Try to connect to internet first
            try {

                //Lets as well get the auto-generated code from a class we created
                $new_code = new GenerateVerificationCode();
                $code = $new_code->generateVerifyCode($code);

                $data = array(
                    'name' => Input::get('name'),
                    'email' => Input::get('email'),
                    'username' => Input::get('username'),
                    'password' => Input::get('password'),
                    'confirm_url' => URL::to('/') . '/account/verify/' . $code,
                    '_token' => Input::get('_token'),
                );

                if (app()->environment() == 'production') {

                    $sendto = getenv('SUPPORT_EMAIL');
                    $email = Input::get('email');

                    Mail::send('emails.users.successful-signup', $data, function ($message) use ($email, $sendto) {
                        $message->from($sendto, 'Kenya Professional Society Of Criminology');
                        $message->to($email)->subject('SUCCESSFUL SIGN UP AT KEPSOC');
                    });

                }

                //return an exception error if there was no internet connections to send mail.
                } catch (Exception $e) {

                if ($e instanceof \Swift_SwiftException) {

                    Session::flash('unsuccessful', 'Sorry, we could NOT sign you. Check your INTERNET CONNECTIONS and try again!');
                    return redirect()->back()->withInput();
                }

            }
            //Store email once there is connection and send an email
            {
                //Lets Get the DATES class we had set
                $date = new DateFormats();
                $Date1 = $date->date();

                //Lets Get the ROLES class we had set
                $user_role = new UserRoles();
                $role = $user_role->roles();

                //Fetch the first USER ID
                $users_uid = User::orderBy('uid', 'DESC')->take(1)->get();

                //Finally we get to store all our documents here
                $signups = new User;

                //Lets auto generate unique USER ID
                if ($users_uid->isEmpty()) {
                    $signups->uid=1;
                }
                else {
                    foreach ($users_uid as $count) {
                        $uid = $count->uid;
                        $signups->uid = $uid+1;
                    }
                }
                $signups->name = Input::get('name');
                $signups->username = Input::get('username');
                $signups->email = Input::get('email');
                $signups->password = Hash::make(Input::get('password'));
                $signups->remember_token = Input::get('_token');
                $signups->role = $role = ['member_role'];
                /*$signups->role = $role = ['member_role', 'super_admin_role'];*/
                $signups->signed_date = $Date1['Date1'];
                $signups->verification_token = $code;
                $signups->confirmation_code = '0';
                $signups->save();

                // redirect
                Session::flash('successful', 'Thank you, Kindly check your email to verify your sign up');
                return Redirect::to('/login');
            }

        } else {
            return Redirect::to('/auth/register')->withErrors($validator);
        }

    }

    //Allow users to verify there accounts
    public function verifyAccount($token){
        $user = User::where('verification_token','=',$token)->first();

        if (isset($user)) {

            //Lets Get the DATES class we had set
            $date = new DateFormats();
            $Date1 = $date->date();

            //Confirm the account
            $user->confirmation_code = '1';
            $user->confirmed_date = $Date1['Date1'];
            $user->save();

            Session::flash('successful', 'Yes! We have verified your account successfully.');
            return Redirect::to('/login');

        }
        else {
            Session::flash('unsuccessful', 'Sorry the confirmation token is either broken or it has expired.');
            return Redirect::to('/auth/register');
        }
    }

}