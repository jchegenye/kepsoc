<?php namespace JCHEGENYE\Http\Controllers\Users;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;
use JCHEGENYE\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;


/**
 * @author Jackson Asumu Chegenye
 *         0711494289
 *         chegenyejackson@gmail.com
 * @version 0.0.1
 * @copyright 2015-2016 j-tech.tech
 *
 * @File Handles Login
 */

class LoginUsController extends Controller {

    public function loginPage(){

        return view('auth.login');

    }

   /* public function  postLogin() {

        $rules = array(
            'email' => 'required|email|max:255',
            'password' => 'required|max:255',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            return Redirect::back()
                ->withErrors($validator)
                ->withInput(Input::except('password'));

        }
        else{
            // create our user data for the authentication
            $userdata = array(
                'email'   => Input::get('email'),
                'password'  => Input::get('password')
            );

            // attempt to do the login
            if (Auth::attempt($userdata,true)) {

                $user['email'] = Auth::user()->email;
                $user['username'] = Auth::user()->username;
                $user['uid'] = Auth::user()->uid;
                $user['id'] = Auth::user()->id;

                $user_data = json_encode($user);

                return Redirect::intended('/reporter/' . Auth::user()->uid)
                    ->withCookie(Cookie::make('user_cookie', $user_data,600000));
            }
            else {
                // validation not successful, send back to form
                return Redirect::to('auth/login')
                    ->withErrors(['unsuccessful' => $this->getFailedMessages(), ])
                    ->withInput(Input::except('password'));

            }
        }

    }*/

    /*protected function getFailedMessages()
    {
        return Lang::has('auth.failed')
            ? Lang::get('auth.failed')
            : 'These credentials do not match our records.';
    }*/

    public function doLogout()
    {
        Auth::logout();

        $cookie = Cookie::forget('user_cookie');

        return Redirect::to('/')->withCookie($cookie);
    }

}