<?php namespace JCHEGENYE\Http\Controllers\Users;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use JCHEGENYE\Http\Controllers\Controller;
use JCHEGENYE\JTech\Model\Users\User;
use Exception;
use JCHEGENYE\JTech\ReusableCodes\DateFormats;

/**
 * @author Jackson Asumu Chegenye
 *         0711494289
 *         chegenyejackson@gmail.com
 * @version 0.0.1
 * @copyright 2015-2016 j-tech.tech
 *
 * @File Handles SignUps
 */

class ForgotPasswordController extends Controller {

    public function resetPage(){

        return view('auth.password-reset');

    }

    /**
     * Process a password reset request from an anonymous user.
     */
    public function request(){

        $rules = array(
            'email' => 'required|email|max:255',
            'g-recaptcha-response' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            return Redirect::back()
                ->withErrors($validator)
                ->withInput(Input::except('password'));

        }
        else{

            $user = User::where('email', '=', Input::get('email'))->first();

            if($user){

               /* try {
                    $email = $user->email;
                    $sendto = getenv('SUPPORT_EMAIL');
                    $data = array(
                        'username' => $user->username,
                        'reset_url' => URL::to('/') . '/password/reset/' . $user->remember_token,
                    );
                    Mail::send('auth.emails.password-reset-link', $data, function ($message) use ($email, $sendto) {
                        $message->from($sendto, 'Kenya Professional Society Of Criminology');
                        $message->to($email)->subject('PASSWORD RESET LINK AT KEPSOC');
                    });*/

                    Session::flash('successful', 'We found, your email BUT THIS FEATURE IS STILL UNDER DEVELOPMENT'/*$this->getSuccessMessagesReset()*/);
                    return Redirect::to('/login');

                /*} catch (Exception $e) {

                    if ($e instanceof \Swift_SwiftException) {

                        Session::flash('email_error_connection', $this->getFailedMessagesSendEmail());
                        return redirect()->back();
                    }

                }*/

            }else{

                Session::flash('unsuccessful', $this->getFailedMessagesReset());
                return redirect()->back()->withInput();

            }

        }

    }

        /**
         * Display a form to request for a password reminder.
         */
        public function getResetPage($token){

            $getToken = [
                'token' => $token
            ];

            return View::make('auth.reset-form')->with($getToken);

        }

        /*public function postReset($token){

            $reset = Input::all();
            $rules = array(
                'password' => 'required|min:6|max:20|unique:users,password',
                'confirm_password' => 'required|same:password',
                'g-recaptcha-response' => 'required',
            );

            $validator = Validator::make ($reset, $rules);

            if ($validator -> passes()) {

                try {

                    $user = User::where('remember_token','=',$token)->first();

                    $email = $user->email;
                    $sendto = getenv('SUPPORT_EMAIL');
                    $data = array(
                        'username' => $user->username,
                        'reset_url' => URL::to('/') . '/password/reset/' . $user->remember_token,
                    );
                    Mail::send('auth.emails.password-success-reset', $data, function ($message) use ($email, $sendto) {
                        $message->from($sendto, 'Kenya Professional Society Of Criminology');
                        $message->to($email)->subject('SUCCESSFUL PASSWORD RESET AT KEPSOC');
                    });

                    Session::flash('successful', $this->getSuccessMessagesPassReset());
                    return redirect::to('/login');

                } catch (Exception $e) {

                    if ($e instanceof \Swift_SwiftException) {

                        Session::flash('email_error_connection', $this->getFailedMessagesSendEmail());
                        return redirect()->back();
                    }

                }{

                    $userReset = User::where('remember_token','=',$token)->first();
                    if (isset($userReset)) {

                        $date = new DateFormats();
                        $Date1 = $date->date();

                        $userReset->password  = Hash::make(Input::get('password'));
                        $userReset->remember_token = Input::get('_token');
                        $userReset->confirmation_code = 0;
                        $userReset->password_reset_date = $Date1['Date1'];
                        $userReset->save();

                        Session::flash('successful', $this->getSuccessMessagesPassReset());
                        return Redirect::back();


                    }
                    else {
                        Session::flash('unsuccessful', $this->getFailedMessagesPassReset());
                        return Redirect::back();

                    }
                }

            } else {

                return redirect()->back()->withErrors($validator)->withInput();
            }

        }*/

    }