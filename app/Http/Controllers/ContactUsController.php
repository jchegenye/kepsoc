<?php namespace JCHEGENYE\Http\Controllers;

use JCHEGENYE\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Exception;
/**
  * @author Jackson Asumu Chegenye
  *         0711494289
  *         chegenyejackson@gmail.com
  * @version 0.0.1
  * @copyright 2015-2016 j-tech.tech
  *
  * @File Handles Contact Us Form
  */

class ContactUsController extends Controller {

  /**
     * Send contact details.
     *
     * @return 
     */

  public function getContuctUsDetails(){

    $data = Input::all();
    $rules = array (
        'name' => 'required|min:5',
        'email'  => 'required|email',
        'phonenumber' => 'required|numeric|min:10',
        'subject' => 'required|max:255',
        'bodyMessage' => 'required',
        'g-recaptcha-response' => 'required|recaptcha',
    );

    $validator  = Validator::make ($data, $rules);
    if ($validator -> passes()){

      try {

        $sendto = getenv('SUPPORT_EMAIL');
        $email_data = array(
          'name' => Input::get('name'),
          'email' => Input::get('email'),
          'phonenumber' => Input::get('phonenumber'),
          'subject' => Input::get('subject'),
          'bodyMessage' => Input::get('bodyMessage'),
          '_token' => Input::get('_token'),
        );

        if (app()->environment() == 'production') {

          $name = Input::get('name');
          $email = Input::get('email');
          Mail::send('emails.support', $email_data, function ($message) use ($name, $email, $sendto) {
            $message->to($sendto, 'Kenya Professional Society Of Criminology');
            $message->subject('Enquiry / Support');
            $message->from($email, $name);
          });

        }

      } catch (Exception $e) {

          if ($e instanceof \Swift_SwiftException) {
            return response()->view('errors.EmailError');
          }

      }

      {
        
        Session::flash('successfull', 'Thanks for contacting us, we will get back to you shortly!');
        return Redirect::to('/#contact');
        
      }
             
      
    }
    else{
      return Redirect::to('/#contact')
        ->withInput()
        ->withErrors($validator->messages());
    }
  }
}