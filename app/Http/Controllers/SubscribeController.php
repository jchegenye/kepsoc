<?php namespace JCHEGENYE\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\View;
use JCHEGENYE\JTech\Model\Subscribe;
use JCHEGENYE\JTech\ReusableCodes\DateFormats;
use JCHEGENYE\JTech\ReusableCodes\GenerateVerificationCode;
use Exception;

/**
  * @author Jackson Asumu Chegenye
  *         0711494289
  *         chegenyejackson@gmail.com
  * @version 0.0.1
  * @copyright 2015-2016 j-tech.tech
  *
  * @File Handles Subscriptions
  */

class SubscribeController extends Controller {

	/**
	 * Subscribe to newsletters
	 * @param Redirect $code
	 * @return \Illuminate\Http\Response
	 */

	public function storeEmails(Redirect $code){

	  	$subscribe = Input::all();
	    $rules = array(
			'sEmail' => 'email|unique:subscriptions,subscribed_email|required',
	    );
	    
		$validator = Validator::make ($subscribe, $rules);

			if ($validator -> passes()) {

				//Try to connect to internet first
				try {
					//Lets as well get the auto-generated code from a class we created
					$new_code = new GenerateVerificationCode();
					$code = $new_code->generateVerifyCode($code);

					$data = array(
						'subscribed_email' => Input::get('sEmail'),
						'subscribed_date' => Input::get('subscription_date'),
						'confirm_url' => URL::to('/') . '/myaccount/verify/' . $code,
						'_token' => Input::get('_token'),
					);

					//Then, let's send an email to the subscriber, with his/her verification code as well.
					$sendto = getenv('SUPPORT_EMAIL');
					$email = Input::get('sEmail');
					Mail::send('emails.subscription', $data, function ($message) use ($email, $sendto) {
						$message->from($sendto, 'Kenya Professional Society Of Criminology');
						$message->to($email)->subject('Newsletter Subscription');
					});
				//return an exception error if there was no internet connections to send mail.
				} catch (Exception $e) {

					if ($e instanceof \Swift_SwiftException) {
						return response()->view('errors.EmailError');
					}

				}
				//Store email once there is connection and send an email
				{
					//Lets Get the date class we had set
					$date = new DateFormats();
					$Date1 = $date->date();

					//Finally we get to store all our documents here
					$Subscribe = new Subscribe;
					$Subscribe->subscribed_email = Input::get('sEmail');
					$Subscribe->confirmation_code = '0';
					$Subscribe->subscribed_date = $Date1['Date1'];
					$Subscribe->verify_code = $code;
					$Subscribe->save();

					// redirect
					Session::flash('successfull_subscription', 'Thank you, Kindly check your email to verify your subscription');
					return Redirect::to('/#subscribe');
				}

			} else {
				return Redirect::to('/#subscribe')
					->withErrors($validator);
			}

	}


	/**
	 * @param Request $token
	 * @return mixed
     */
	public function getAccountToVerify($token){
		//Try to connect to internet first
		try {

			$user = Subscribe::where('verify_code','=',$token)
				->where('confirmation_code','=','0')
				->first();

			if (isset($user)) {

				$data = array(
					'email' => $user->subscribed_email,
					'subscribed_date' => $user->subscribed_date,
					'confirm_url' => URL::to('/') . '/account/verify/' . $user->verify_code
				);

			} return View::make('emails.subscribers.confirm-msg-success')->with($data);


		} catch(Exception $e) {

			return App::abort(404, 'Email verification link not found');
		}

	}

	/**
	 * @param $token
	 * @return mixed
     */
	public function verifyAccount($token){

		$user = Subscribe::where('verify_code','=',$token)
			->where('confirmation_code','=','0')
			->first();

		if (isset($user)) {

			try {

				$data = array(
					'email' => $user->subscribed_email,
					'subscribed_date' => $user->subscribed_date,
					'verified_at' => $user->verified_at,
				);
				$sendto = getenv('SUPPORT_EMAIL');
				$email = $user->subscribed_email;
				Mail::send('emails.subscribers.successfull-subscription', $data, function ($message) use ($email, $sendto) {
					$message->from($sendto, 'Kenya Professional Society Of Criminology');
					$message->to($email)->subject('Newsletter Subscription | Confirmation');
				});

			//return an exception error if there was no internet connections to send mail.
			}catch (Exception $e) {

				if ($e instanceof \Swift_SwiftException) {
					return response()->view('errors.EmailError');
				}

			}
			//Store email once there is connection and send an email
			{

				$data = array(
					'email' => $user->subscribed_email,
					'subscribed_date' => $user->subscribed_date,
					'verified_at' => $user->verified_at,
				);
				//Lets Get the date class we had set
				$date = new DateFormats();
				$Date1 = $date->date();

				//Confirm user account
				$user->verified_at = $Date1['Date1'];
				$user->confirmation_code = '1';
				$user->save();

				return View::make('emails.subscribers.successfull-subscription')->with($data);
			}

		}
	}

}