<?php namespace JCHEGENYE\Http\Controllers\Admin\Workbench;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use JCHEGENYE\Http\Controllers\Controller;
use JCHEGENYE\JTech\Model\Admin\Workbench\Role;
use JCHEGENYE\JTech\ReusableCodes\GenerateVerificationCode;

class RolesController extends Controller{

    /**
     * Show ROLE PAGE
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getRolesPage(){

        return view('admin.workbench.roles.create-role');

    }

    /**
     * store a new ROLE NAME
     * @return mixed
     */
    public function postRoleName(Request $code){

        $role = Input::all();
        $rules = array(
            'role' => 'unique:roles,name|required',
        );

        $validator = Validator::make ($role, $rules);

        if ($validator -> passes()) {

            //Lets as well get the auto-generated code from a class we created
            $new_code = new GenerateVerificationCode;
            $code = $new_code->generatePermissionsCode($code);

            $role = new Role();
            $role->name = Input::get('role');
            $role->save();

            Session::flash('successful', 'Saved!');
            return view('admin.workbench.roles.create-role');

        } else {
            return Redirect::to('create/role')
                ->withErrors($validator);
        }

    }

    /**
     * show stored ROLES
     * @return $this
     */
    public function getUserRoles(){

        $roles = Role::all();

        $data = array(
            'roles'=>$roles,
        );

        return View('admin.workbench.roles.view-user-roles')->with($data);

    }

}