<?php namespace JCHEGENYE\Http\Controllers\Admin\Workbench;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use JCHEGENYE\Http\Controllers\Controller;
use JCHEGENYE\JTech\Model\Admin\Workbench\Role;
use JCHEGENYE\JTech\Model\Admin\Workbench\UserPermission;
use JCHEGENYE\JTech\Model\Users\User;

class PermissionsController extends Controller{

    /**
     * @param $id
     * @return $this
     */
    public function showRolePermissions($id){

        $role = Role::find($id);

        $data = array(
            'role'=>$role,
        );

        return view('admin.workbench.roles.permissions')->with($data);

    }

    /**
     * @param $id
     * @return $this
     */
    public function editRolePermissions($id){

        $role = Role::find($id);

        $permissions = UserPermission::all();

        $data = array(
            'role'=>$role,
            'permissions' => $permissions,
        );
        return view('admin.workbench.roles.edit-permissions')->with($data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function updatePermission($id){
        if (Cache::has('role_' . $id))
        {
            $role = Cache::get('role_' . $id);
        }
        else{
            $role = Role::find($id);
            Cache::put('role_' . $id, $role, 360);
        }
        $role->permission_name = Input::get('permission_name');
        $role->save();

        Cache::forget('role_' . $id);
        return Redirect::to('users/roles');
    }


    /**
     * @param $uid
     * @return $this
     */
    public function userPermissions($uid){

        $permissions = UserPermission::all();
        $user = User::where('uid', '=', (int)$uid)->first();

        $userPerm = UserPermission::where('machine_name' , 'All', $user->role )->get();

        $data = array(
            'userPermission' => $userPerm,
            'user'=>$user,
            'permissions' => $permissions,
        );

        return view('admin.workbench.roles.user-permissions')->with($data);

    }

    public function updateUserPermission($uid){

        if (Cache::has('user_' . $uid)) {

            $user = Cache::get('user_' . $uid);

        }
        else{

            $user = User::where('uid', '=', (int)$uid)->first();
            Cache::put('user_' . $uid, $user, 360);
        }
            try {

                $data = array(
                    'user' => $user,
                );

                if (app()->environment() == 'production'){

                    $to = $user->email;
                    $from = getenv('SUPPORT_EMAIL');
                    Mail::send('emails.permissions', $data, function ($message) use ($to, $from) {
                        $message->from($from, 'Kenya Professional Society Of Criminology');
                        $message->to($to)->subject('Kepsoc Website | Permissions');
                    });

                }

            } catch (Exception $e) {

                if ($e instanceof \Swift_SwiftException) {
                    return response()->view('errors.EmailError');
                }

            }{

            $user->role = Input::get('permission');
            $user->save();

            Cache::forget('user_' . $uid);
            return Redirect::to('workbench/users');
           /* return Redirect::to('give/permission/to/' . $user->uid);*/

        }



    }



}