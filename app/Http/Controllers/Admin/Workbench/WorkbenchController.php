<?php namespace JCHEGENYE\Http\Controllers\Admin\Workbench;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use JCHEGENYE\Http\Controllers\Controller;
use JCHEGENYE\JTech\Model\Subscribe;
use JCHEGENYE\JTech\Model\Users\User;
use JCHEGENYE\JTech\Reports\Statistics;
use JCHEGENYE\JTech\ReusableCodes\Members\UserMemberDetails;

class WorkbenchController extends Controller{

    public function showStatistics(){

        $getStatisticsCount = new Statistics();
        $data = $getStatisticsCount->countStatistics();
        return view('admin.workbench.statistics')->with($data);

    }

    public function showUsers(){

        $users = new UserMemberDetails();
        $data = $users->getAllUserDetails();
        return view('admin.workbench.users')->with($data);

    }

    public function showMembers(){

        $getMembers = new UserMemberDetails();
        $data = $getMembers->getMembersDetails();
        return view('admin.workbench.members')->with($data);

    }

    public function showSubscribers(){

        $emails = Subscribe::orderBy('subscribed_date', 'ASC')->paginate(5);
        return view('admin.workbench.subscribers', ['subscriptions' => $emails]);

    }

    public function memberNoneMember($uid){

        $user = User::where('uid','=',(int)$uid)
            ->first();

        $data = array(
            'user' => $user,
        );

        if(empty($user->user_status)) {

            if (app()->environment() == 'production'){

                $to = $user->email;
                $from = getenv('SUPPORT_EMAIL');
                Mail::send('emails.member.member_approval', $data, function ($message) use ($to, $from) {
                    $message->from($from, 'Kenya Professional Society Of Criminology');
                    $message->to($to)->subject('Kepsoc Website | Member Approval');
                });

            }

            $user->user_status = 'member';
            $user->save();
            Session::flash('approved', 'Successfully Approved');
            return redirect()->back();

        }elseif($user->user_status == 'member'){

            if (app()->environment() == 'production'){

                $to = $user->email;
                $from = getenv('SUPPORT_EMAIL');
                Mail::send('emails.member.member_approval', $data, function ($message) use ($to, $from) {
                    $message->from($from, 'Kenya Professional Society Of Criminology');
                    $message->to($to)->subject('Kepsoc Website | Member Approval');
                });

            }

            $user->user_status = 'non_member';
            $user->save();
            Session::flash('declined', 'Successfully Declined');
            return redirect()->back();

        }elseif($user->user_status == 'non_member'){

            if (app()->environment() == 'production'){

                $to = $user->email;
                $from = getenv('SUPPORT_EMAIL');
                Mail::send('emails.member.member_decline', $data, function ($message) use ($to,
                    $from) {
                    $message->from($from, 'Kenya Professional Society Of Criminology');
                    $message->to($to)->subject('Kepsoc Website | Member Decline');
                });

            }

            $user->user_status = 'member';
            $user->save();
            Session::flash('approved', 'Successfully Approved');
            return redirect()->back();

        }

    }

    public function lockUnlock($uid){

        $user = User::where('uid','=',(int)$uid)
            ->first();

        $data = array(
            'user' => $user,
        );

        if(empty($user->user_access)) {

            if (app()->environment() == 'production'){

                $to = $user->email;
                $from = getenv('SUPPORT_EMAIL');
                Mail::send('emails.users.account_lock', $data, function ($message) use ($to,
                    $from) {
                    $message->from($from, 'Kenya Professional Society Of Criminology');
                    $message->to($to)->subject('Kepsoc Website | Account Lock');
                });

            }

            $user->user_access = 'locked';
            $user->save();
            Session::flash('locked', 'Successfully Locked!');
            return redirect()->back();

        }elseif($user->user_access == 'locked'){

            if (app()->environment() == 'production'){

                $to = $user->email;
                $from = getenv('SUPPORT_EMAIL');
                Mail::send('emails.users.account_lock', $data, function ($message) use ($to,
                    $from) {
                    $message->from($from, 'Kenya Professional Society Of Criminology');
                    $message->to($to)->subject('Kepsoc Website | Account Lock');
                });

            }

            $user->user_access = 'unlocked';
            $user->save();
            Session::flash('unlocked', 'Successfully Unlocked!');
            return redirect()->back();

        }elseif($user->user_access == 'unlocked'){

            if (app()->environment() == 'production'){

                $to = $user->email;
                $from = getenv('SUPPORT_EMAIL');
                Mail::send('emails.users.account_unlock', $data, function ($message) use ($to,
                    $from) {
                    $message->from($from, 'Kenya Professional Society Of Criminology');
                    $message->to($to)->subject('Kepsoc Website | Account Unlock');
                });

            }

            $user->user_access = 'locked';
            $user->save();
            Session::flash('locked', 'Successfully Locked!');
            return redirect()->back();

        }

    }

    /**
     * Trash : Soft deleted models will automatically be excluded from query results.
     * However, you we may force soft deleted models to appear by using withTrashed()
     *
     * @param $uid
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteAccount($uid){

        /*$user = User::where('uid', (int)$uid)->delete();*/

        $delete = User::withTrashed()
            ->where('uid', (int)$uid)
            ->delete();

        /*$restore = User::onlyTrashed()
            ->where('uid', (int)$uid)
            ->restore();*/
        if(empty($delete->deleted_at)) {

            Session::flash('trashed', 'Account successfully trashed!');
            return redirect()->back();

        }elseif($delete->deleted_at == null) {

            Session::flash('trashed', 'Account successfully trashed!');
            return redirect()->back();

        }
        elseif($delete->deleted_at){

            $delete->restore();

            Session::flash('already_trashed', 'Account already trashed!');
            return redirect()->back();

        }/*elseif($restore->deleted_at){





        }*/

        /*if($user) {

            Session::flash('trashed', 'Account successfully trashed!');
            return redirect()->back();

        }elseif($restore){

            $user->restore();

            Session::flash('already_trashed', 'Account already trashed!');
            return redirect()->back();

        }*/

    }

}