<?php

namespace JCHEGENYE\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class MemberRoleMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role This will store a string of Role given from the ROUTE
     * @return mixed
     */

    public function handle($request, Closure $next, $role)
    {
        if (!$request->user()->hasRole($role)) {

            foreach(Auth::user()->role as $individual_role) {
                if ($role == $individual_role) {
                    return $next($request);
                }
            }

        }

        Session::flash('unauthorised', $this->unauthorisedAccess());
        return redirect()->back();

    }

    /**
     * @return string
     */
    protected function unauthorisedAccess()
    {
        return Lang::has('users.members.unauthorised_access')
            ? Lang::get('users.members.unauthorised_access')
            : 'Unauthorized Access: You are not authorized to access this resource!';
    }

}