<?php

namespace JCHEGENYE\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Exception;
use JCHEGENYE\JTech\Model\Admin\Workbench\UserPermission;

class AdminRoleMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role This will store a string of Role given from the ROUTE
     * @return mixed
     */

    public function handle($request, Closure $next, $role)
    {

        if (!$request->user()->hasRole($role)) {

            foreach(Auth::user()->role as $individual_role) {

                if($role == $individual_role && Auth::user()->uid == 1){
                    return $next($request);
                }
            }

        }

        Session::flash('unauthorised', $this->unauthorisedAccess());
        return redirect()->back();

    }

    /**
     * @return string
     */
    protected function unauthorisedAccess()
    {
        return Lang::has('users.members.unauthorised')
            ? Lang::get('users.members.unauthorised')
            : 'Unauthorized Access: You are not authorized to access this resource!';
    }

}