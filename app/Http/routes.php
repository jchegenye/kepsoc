<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
**** ************************************ ****
I had to move this route to a group with thw 'web' middleware applied since
our landing page has some form that require POST with 'error' return message 
**** ************************************ ****
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(

	['middleware' => ['web']], function () {

	Route::get('/', function () {
	    return view('welcome');
	});

	Route::post('send-contact', [
		'uses' => 'ContactUsController@getContuctUsDetails'
	]);
	Route::post('send-email', [
		'uses' => 'SubscribeController@storeEmails'
	]);

	/*Start of Extra Pages*/
	Route::get('test', function () {
		return view('test');
	});
	Route::get('more-on-about-us', function () {
	    return view('layouts/pages/more-about-us');
	});
	Route::get('gallery', [
		'uses' => 'Gallery\GalleryController@getGalleryPage'
	]);
	/*End of Extra Pages */

	/*
	|--------------------------------------------------------------------------
	| Login Routes
	|--------------------------------------------------------------------------
	*/
	Route::get('login', [
		'uses' => 'Users\LoginUsController@loginPage'
	]);
	Route::post('auth/login', 'Auth\AuthController@postLogin');
	Route::get('auth/logout', array('uses' => 'Users\LoginUsController@doLogout', 'before' => 'auth'));

	/*
	|--------------------------------------------------------------------------
	| Sign up Routes
	|--------------------------------------------------------------------------
	*/
	Route::get('auth/register', [
		'uses' => 'Users\SignUpController@registerPage'
	]);
	Route::post('auth/register', [
  		'uses' => 'Users\SignUpController@registerUser'
	]);
	Route::get('/account/verify/{token}', [
		'uses' => 'Users\SignUpController@verifyAccount'
	]);

	/*
	|--------------------------------------------------------------------------
	| Password reset links Routes
	|--------------------------------------------------------------------------
	*/
	Route::get('auth/reset', [
		'uses' => 'Users\ForgotPasswordController@resetPage',
	]);
	Route::post('password/email', [
		'uses' => 'Users\ForgotPasswordController@request',
	]);
	Route::get('password/reset/{token}', [
		'uses' => 'Users\ForgotPasswordController@getResetPage',
	]);
	Route::post('password/reset/{token}', [
		'uses' => 'Users\ForgotPasswordController@postReset',
	]);

	/*
	|--------------------------------------------------------------------------
	| Authenticated Routes - Members
	|--------------------------------------------------------------------------
	*/
	Route::group(['middleware' => 'auth'], function()
	{

		Route::get('members/dash', [
			'uses' => 'Members\MembersController@membersDashboard',
			'middleware' => 'member:member_role'
		]);

		Route::get('members/list', [
			'uses' => 'Members\MembersController@membersList',
			'middleware' => 'member:access_to_members_list'
		]);
		Route::get('member/edit/profile/{uid}', [
			'uses' => 'Members\MembersController@editProfilePage',
			'middleware' => 'member:access_to_members_list'
		]);
		Route::get('members/profile/{uid}', [
			'uses' => 'Members\MembersController@memberProfile',
			'middleware' => 'member:access_to_members_list'
		]);
		Route::get('other/members/profile/{mid}', [
			'uses' => 'Members\MembersController@memberProfile',
			'middleware' => 'member:access_to_members_list'
		]);
		Route::post('add-profile/{uid}', [
			'uses' => 'Members\MembersController@storeProfile',
			'middleware' => 'member:access_to_members_list'
		]);
	});

	/*
	|--------------------------------------------------------------------------
	| Authenticated Routes - Admin ROLES
	|--------------------------------------------------------------------------
	*/
	Route::group(['middleware' => 'auth'], function()
	{

		//-----------------------------------------------------------------------------//
		Route::get('workbench/home', [
			'uses' => 'Admin\Workbench\WorkbenchController@showStatistics',
			'middleware' => 'admin:access_to_workbench'
		]);
		Route::get('workbench/users', [
			'uses' => 'Admin\Workbench\WorkbenchController@showUsers',
			'middleware' => 'admin:access_to_workbench'
		]);
		Route::get('workbench/members', [
			'uses' => 'Admin\Workbench\WorkbenchController@showMembers',
			'middleware' => 'admin:access_to_workbench'
		]);
		Route::get('workbench/subscribers', [
			'uses' => 'Admin\Workbench\WorkbenchController@showSubscribers',
			'middleware' => 'admin:access_to_workbench'
		]);

		Route::get('/user/status/{uid}', [
				'uses' => 'Admin\Workbench\WorkbenchController@memberNoneMember',
				'middleware' => 'admin:can_approve_a_member'
		]);
		Route::get('/lock/user/{uid}', [
				'uses' => 'Admin\Workbench\WorkbenchController@lockUnlock',
				'middleware' => 'admin:can_lock_user'
		]);
		Route::get('/trash/user/{uid}', [
				'uses' => 'Admin\Workbench\WorkbenchController@deleteAccount',
				'middleware' => 'admin:can_delete_an_account'
		]);

		//-----------------------------------------------------------------------------//
		Route::get('create/role', [
			'uses' => 'Admin\Workbench\RolesController@getRolesPage',
			'middleware' => 'admin:access_to_admin_routes'
		]);
		Route::post('save/role', [
			'uses' => 'Admin\Workbench\RolesController@postRoleName',
			'middleware' => 'admin:access_to_admin_routes'
		]);

		Route::get('users/roles', [
			'uses' => 'Admin\Workbench\RolesController@getUserRoles',
			'middleware' => 'admin:access_to_admin_routes'
		]);

		Route::get('give/permission/to/{uid}', [
			'uses' => 'Admin\Workbench\PermissionsController@userPermissions',
			'middleware' => 'admin:can_give_permissions'
		]);

		Route::put('weka/permissions/{uid}', array(
			'uses' => 'Admin\Workbench\PermissionsController@updateUserPermission',
			'as' => 'user_permissions_update',
			'middleware' => 'admin:can_give_permissions'
		));

		/*
		|--------------------------------------------------------------------------
		| Authenticated Routes - Admin PERMISSIONS
		|--------------------------------------------------------------------------
		*/

		Route::get('role/permissions/{id}', [
			'uses' => 'Admin\Workbench\PermissionsController@showRolePermissions',
			'middleware' => 'admin:access_admin_routes'
		]);
		Route::get('role/permissions/{id}/edit', [
			'uses' => 'Admin\Workbench\PermissionsController@editRolePermissions',
			'middleware' => 'admin:access_admin_routes'
		]);
		Route::put('workbench/role/{id}', array(
			'uses' => 'Admin\Workbench\PermissionsController@updatePermission',
			'as' => 'role_update',
			'middleware' => 'admin:access_admin_routes'
		));
	});

});
