<!DOCTYPE html>
<html lang="en">
<head>
    <!--##################################### start of HEAD ########################################-->
    <link rel="canonical" href="http://kepsoc.org/login" />

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf_token" content="{{ csrf_token() }}">

    <title>Kenya Professional Society Of Criminology | Password Reset</title>

    <meta name="copyright" content="j-tech is a registered trade mark Co. based in Kenya">
    <meta name="author" content="Chegenye Asumu Jackson">
    <meta name="reply-to" content="chegenyejackson@gmail.com">
    <meta name="web_author" content="https://j-tech.tech">
    <meta name="distribution" content="global">
    <meta name="robots" content="none, noindex, nofollow">
    <meta name="language" content="english, kiswahili">
    <meta name="rating" content="general, safe for kids">
    <meta name="identifier-URL" content="https://kepsoc.org/login">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <!-- Favicon -->
    <link rel="icon" href="{{ asset('http://kepsoc.org/favicon.ico') }}">
    <!--####################################### end of head ########################################-->

    <!--####################################### start of CSS #######################################-->
    @include('layouts.partials.css')
            <!--####################################### end of css #########################################-->

</head>

<body>

<section id="sign-up">
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>

            <div class="col-md-4 sign-up-form"><img src="../logo.png">
                <br><br>
                <P>Hi?</P>
                <br><br>
                <p>Your password reset at KEPSOC was successful.</p>
                <br><br>
                <p>If this is wrong, kindly contact KEPSOC Support Team IMMEDIATELY or call 0711494289</p>
                <br><br>
                <p>Regards,</p>
                <br>
                <p>Kepsoc Support Team</p>
                <br>
                <hr>
                <div class="col-md-12 text-center">
                    <p>Having issues? <a href="/#contact">Contact Us</a></p>
                </div>


            </div>

            <div class="col-md-4"></div>

        </div>
    </div>
</section>

</body>
</html>