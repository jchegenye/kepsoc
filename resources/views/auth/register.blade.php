<!DOCTYPE html>
<html lang="en">
    <head>
    <!--##################################### start of HEAD ########################################-->
        <link rel="canonical" href="http://kepsoc.org/sign-up" />

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf_token" content="{{ csrf_token() }}">

        <title>Kenya Professional Society Of Criminology | Sign Up</title>

        <meta name="copyright" content="j-tech is a registered trade mark Co. based in Kenya">
        <meta name="author" content="Chegenye Asumu Jackson">
        <meta name="reply-to" content="chegenyejackson@gmail.com">
        <meta name="web_author" content="https://j-tech.tech">
        <meta name="distribution" content="global">
        <meta name="robots" content="none, noindex, nofollow">
        <meta name="language" content="english, kiswahili">
        <meta name="rating" content="general, safe for kids">
        <meta name="identifier-URL" content="https://kepsoc.org/sign-up">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <!-- Favicon -->
        <link rel="icon" href="{{ asset('http://kepsoc.org/favicon.ico') }}">
        <!--####################################### end of head ########################################-->

        <!--####################################### start of CSS #######################################-->
        @include('layouts.partials.css')
        <!--####################################### end of css #########################################-->

        <!--####################################### start of Google Analytics ##########################-->
         {!! Analytics::render() !!}
        <!--####################################### end of Google Analytics ############################-->
       
    </head>

    <body>

      <!-- ------------------------------------ start of PRELOADER ---------------------------------- -->
          @include('layouts.partials.preload')
      <!-- ------------------------------------ end of PRELOADER ------------------------------------ -->


    <section id="sign-up">
        <div class="container">
            <div class="row">

                <div class="col-sm-12  col-xm-12 col-md-4 col-md-offset-4">
                    <div class="sign-up-form">
                        <img src="../logo.png" class="img-responsive">
                        @if(Session::has('information'))
                            <div class="alert alert-info info">
                                <img src="../logo.png" style="position: absolute; left: 0px; height: 100%; top: 0px; opacity: 1.0; padding: 0px; margin: initial; background-color: #fff; margin-left: inherit;">
                                <div class="alert-all-text">{{ Session::get('information') }}</div>
                            </div>
                        @endif
                        @if(Session::has('successful'))
                            <div class="alert alert-success success">
                                <img src="../logo.png" style="position: absolute; left: 0px; height: 100%; top: 0px; opacity: 1.0; padding: 0px; margin: initial; background-color: #fff; margin-left: inherit;">
                                <div class="alert-all-text">{{ Session::get('successful') }}</div>
                            </div>
                        @endif
                        @if(Session::has('unsuccessful'))
                            <div class="alert alert-danger danger">
                                <img src="../logo.png" style="position: absolute; left: 0px; height: 100%; top: 0px; opacity: 1.0; padding: 0px; margin: initial; background-color: #fff; margin-left: inherit;">
                                <div class="alert-all-text">{{ Session::get('unsuccessful') }}</div>
                            </div>
                        @endif

                        <form id="signup-form" action="{{url('auth/register')}}" method="POST" class="signup-form">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group ">
                              <input placeholder="Full Names"
                                     class="form-control @if ($errors->has('name')) has-error @endif"
                                     name="name" type="text" value="{{ old('name') }}"
                                     id="name">
                                <span class="error-block">
                                    @if ($errors->has('name')){{ $errors->first('name') }} @endif
                                </span>
                            </div>

                          <div class="form-group ">
                              <input placeholder="Email Address"
                                class="form-control @if ($errors->has('email')) has-error @endif"
                                autocomplete="false" name="email" type="email" id="email"
                                value="{{ old('email') }}">
                            <span class="error-block">
                                @if ($errors->has('email')){{ $errors->first('email') }} @endif
                            </span>
                          </div>

                        <div class="form-group ">
                            <input placeholder="User Name"
                                   class="form-control @if ($errors->has('username')) has-error @endif"
                                   name="username" type="text" value="{{ old('username') }}"
                                   id="username">
                            <span class="error-block">
                                @if ($errors->has('username')){{ $errors->first('username') }} @endif
                            </span>
                        </div>

                          <div class="form-group ">
                              <input placeholder="Password"
                                     class="form-control @if ($errors->has('password')) has-error @endif"
                                     name="password" type="password" value="{{ old('password') }}"
                                     id="password">
                            <span class="error-block">
                                @if ($errors->has('password')){{ $errors->first('password') }} @endif
                            </span>
                          </div>

                          <div class="form-group ">
                              <input placeholder="Confirm Password"
                                     class="form-control @if ($errors->has('password_confirmation')) has-error @endif"
                                     name="password_confirmation" type="password" value="{{ old('password_confirmation') }}"
                                     id="password_confirmation">
                            <span class="error-block">
                                @if ($errors->has('password_confirmation')){{ $errors->first('password_confirmation') }} @endif
                            </span>
                          </div>

                        @if(app()->environment() == 'production')
                            <div class="col-md-12">
                              <div class="form-group">
                                 {!! Recaptcha::render() !!}
                                      <span class="text text-danger">{{ $errors->first('g-recaptcha-response') }}</span>
                              </div>
                          </div>
                        @endif

                          <button id="btn-login" class="btn btn-lg btn-danger" type="submit">
                              SIGN UP
                          </button>
                        </form>

                        <hr>
                        <div class="col-md-12 text-center">
                            <p>
                              <a href="/">Go back</a>
                                Already a member? <a href="{{url('login')}}">login here</a>
                            </p>
                            <br>
                            <p>Having issues? <a href="/#contact">Contact Us</a></p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>   

      <!--####################################### start of SCRIPTS #######################################-->
          @include('layouts..partials.scripts')
      <!--####################################### end of SCRIPTS #########################################-->

    </body>
</html>