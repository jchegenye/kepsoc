        <script type="text/javascript" src="../../../js/jquery.min.js"></script><!-- jQuery -->
        <script type="text/javascript" src="../../../js/bootstrap.min.js"></script><!-- Bootstrap -->
        <script type="text/javascript" src="../../../js/jquery.parallax.js"></script><!-- Parallax -->
        <script type="text/javascript" src="../../../js/smoothscroll.js"></script><!-- Smooth Scroll -->
        <script type="text/javascript" src="../../../js/masonry.pkgd.min.js"></script><!-- masonry -->
        <script type="text/javascript" src="../../../js/jquery.fitvids.js"></script><!-- fitvids -->
        <script type="text/javascript" src="../../../js/owl.carousel.min.js"></script><!-- Owl-Carousel -->
        <script type="text/javascript" src="../../../js/jquery.counterup.min.js"></script><!-- CounterUp -->
        <script type="text/javascript" src="../../../js/waypoints.min.js"></script><!-- CounterUp -->
        <script type="text/javascript" src="../../../js/jquery.isotope.min.js"></script><!-- isotope -->
        <script type="text/javascript" src="../../../js/jquery.magnific-popup.min.js"></script><!-- magnific-popup -->
        <script type="text/javascript" src="../../../js/scripts.js"></script><!-- Scripts -->