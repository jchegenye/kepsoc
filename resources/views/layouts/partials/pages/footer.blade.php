        <footer id="footer">
            <div class="container">
                <div class="row">
                    <!-- /SOCIAL ICONS -->
                    <div class="col-md-4 ">
                        <p style="text-transform:capitalize;">The KEPSOC is an organisation whose aims and membership pursues and promotes scholarly, scientific and professional knowledge concerning the measurement, etiology, consequences, prevention, control and treatment of crime and delinquency.</p>
                    </div>
                    <div class="col-md-4 text-center">
                        <p>Copyright &copy; {{date('Y')}} Kepsoc. | All Rights Reserved.</p>
                    </div>
                    <!-- SOCIAL ICONS -->
                    <div class="col-md-4 footer-social-icons">
                        <span>Follow us on</span>
                        <a href=""><i class="fa fa-facebook"></i></a>
                        <a href=""><i class="fa fa-twitter"></i></a>
                        <a href=""><i class="fa fa-google-plus"></i></a>
                        <a href="whatsapp://send?text=Hi, kindly visit our website - Kenya Professional Society Of Criminology at http://kepsoc.org/"><i class="fa fa-whatsapp"></i></a>                       
                    </div>
                </div>
            </div>
        </footer>
