        <header id="header" class ="navbar-fixed-top">
            <nav class="navbar st-navbar ">
                <div class="container">
                    <div class="navbar-header " >
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#st-navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <a href="{{'/'}}" ><h3 class="logo">KEPSOC</h3></a>
                    </div>

                    <div class="collapse navbar-collapse" id="st-navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#header">Home</a></li>
                            <li><a href="#" data-toggle="dropdown">
                               Who we are?</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#about-us">About-Us</a></li>
                                    <li><a href="#our-team">Our Executive Board</a></li>
                                    <li><a href="{{url('/members/dash')}}">Our Members</a></li>
                                    <li><a href="#core-values">Our Core Values</a></li>
                                    <li><a href="#testimonial">Testimonial</a></li>
                                </ul>
                             </li>
                            <li><a href="#services">Services</a></li>
                            <li><a href="{{url('/gallery')}}">Gallery</a></li>
                            <li><a href="#contact">Contact</a></li>
                            <li class="st-navbar-joinus"><a href="{{'auth/register'}}" style="color: #23293F;">Join Us</a></li>
                            {{--<li style="background-color: rgb(35, 41, 63);"><a href="#" style="border-left:2px;">Downloads</a></li>--}}
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container -->
            </nav>
        </header>
