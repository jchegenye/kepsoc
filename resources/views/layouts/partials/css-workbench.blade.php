    <!-- Bootstrap CSS -->    
    <link href="../../../css/workbench/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="../../../css/workbench/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->

    <!-- font icon -->
    <link href="../../../css/workbench/elegant-icons-style.css" rel="stylesheet" />
    <link href="../../../css/workbench/font-awesome.min.css" rel="stylesheet" />    
    <link rel="stylesheet" href="../../../css/font-awesome.css" />
    
    <!-- owl carousel -->
    <link rel="stylesheet" href="../../../css/workbench/owl.carousel.css" type="text/css">
    <link href="../../../css/workbench/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    
    <!-- Custom styles -->
    <link rel="stylesheet" href="../../../css/workbench/fullcalendar.css">
    <link href="../../../css/workbench/widgets.css" rel="stylesheet">
    <link href="../../../css/workbench/style.css" rel="stylesheet">
    <link href="../../../css/workbench/style-responsive.css" rel="stylesheet" />
    <link href="../../../css/workbench/xcharts.min.css" rel=" stylesheet"> 
    <link href="../../../css/workbench/jquery-ui-1.10.4.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->