  <!-- javascripts -->
  <script src="../../../js/workbench/jquery.js"></script>
   <script src="../../../js/workbench/jquery-ui-1.10.4.min.js"></script>
  <script src="../../../js/workbench/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="../../../js/workbench/jquery-ui-1.9.2.custom.min.js"></script>
  <!-- bootstrap -->
  <script src="../../../js/workbench/bootstrap.min.js"></script>
  <!-- nice scroll -->
  <script src="../../../js/workbench/jquery.scrollTo.min.js"></script>
  <script src="../../../js/workbench/jquery.nicescroll.js" type="text/javascript"></script>
  <!-- charts scripts -->
  <script src="assets/jquery-knob/js/jquery.knob.js"></script>
  <script src="../../../js/workbench/jquery.sparkline.js" type="text/javascript"></script>
  <script src="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
  <script src="../../../js/workbench/owl.carousel.js" ></script>
  <!-- jQuery full calendar -->
  <script src="../../../js/workbench/fullcalendar.min.js"></script> <!-- Full Google Calendar - Calendar -->
	<script src="assets/fullcalendar/fullcalendar/fullcalendar.js"></script>
  <!--script for this page only-->
  <script src="../../../js/workbench/calendar-custom.js"></script>
	<script src="../../../js/workbench/jquery.rateit.min.js"></script>
  <!-- custom select -->
  <script src="../../../js/workbench/jquery.customSelect.min.js" ></script>
   
  <!--custome script for all page-->
  <script src="../../../js/workbench/scripts.js"></script>
  <!-- custom script for this page-->
  <script src="../../../js/workbench/sparkline-chart.js"></script>
  <script src="../../../js/workbench/easy-pie-chart.js"></script>
	<script src="../../../js/workbench/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="../../../js/workbench/jquery-jvectormap-world-mill-en.js"></script>
	<script src="../../../js/workbench/xcharts.min.js"></script>
	<script src="../../../js/workbench/jquery.autosize.min.js"></script>
	<script src="../../../js/workbench/jquery.placeholder.min.js"></script>
	<script src="../../../js/workbench/gdp-data.js"></script>	
	<script src="../../../js/workbench/morris.min.js"></script>
	<script src="../../../js/workbench/sparklines.js"></script>	
	<script src="../../../js/workbench/charts.js"></script>
	<script src="../../../js/workbench/jquery.slimscroll.min.js"></script>
  <!--custom tagsinput-->
  <script src="../../../js/workbench/jquery.tagsinput.js"></script>
  <!-- ck editor -->
  <script src="../../../js/workbench/ckeditor/ckeditor.js" type="text/javascript"></script>
  <!-- chartjs -->
  <script src="../../../js/workbench/chart-master/Chart.js"></script>
  <!-- custom chart script for this page only-->
  <script src="../../../js/workbench/chartjs-custom.js"></script>
  <script src="../../../js/workbench/modal.js"></script>
  
  <script>

      //knob
      $(function() {
        $(".knob").knob({
          'draw' : function () { 
            $(this.i).val(this.cv + '%')
          }
        })
      });

      //carousel
      $(document).ready(function() {
          $("#owl-slider").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });
	  
	  /* ---------- Map ---------- */
	$(function(){
	  $('#map').vectorMap({
	    map: 'world_mill_en',
	    series: {
	      regions: [{
	        values: gdpData,
	        scale: ['#000', '#000'],
	        normalizeFunction: 'polynomial'
	      }]
	    },
		backgroundColor: '#eef3f7',
	    onLabelShow: function(e, el, code){
	      el.html(el.html()+' (GDP - '+gdpData[code]+')');
	    }
	  });
	});



  </script>