{{-----------------------------------------------
----Profile Messages---
------------------------------------------------}}
@if(Session::has('profile_unsuccessful'))
    <div class="alert alert-danger danger">
        <img src="../../../logo.png" style="position: absolute; left: 0px; height: 100%; top: 0px; opacity: 1.0; padding: 0px; margin: initial; background-color: #fff; margin-left: inherit;">
        <div class="alert-all-text">{{ Session::get('profile_unsuccessful') }}</div>
    </div>
@endif
@if(Session::has('profile_successful'))
    <div class="alert alert-success success">
        <img src="../../../logo.png" style="position: absolute; left: 0px; height: 100%; top: 0px; opacity: 1.0; padding: 0px; margin: initial; background-color: #fff; margin-left: inherit;">
        <div class="alert-all-text">{{ Session::get('profile_successful') }}</div>
    </div>
@endif
@if(Session::has('unauthorised_access'))
    <div class="alert alert-info info">
        <img src="../../../logo.png" style="position: absolute; left: 0px; height: 100%; top: 0px; opacity: 1.0; padding: 0px; margin: initial; background-color: #fff; margin-left: inherit;">
        <div class="alert-all-text">{{ Session::get('unauthorised_access') }}</div>
    </div>
@endif

{{-----------------------------------------------
----Workbench Messages---
------------------------------------------------}}
@if(Session::has('unauthorised'))
    <div class="alert alert-info info">
        <img src="../logo.png" style="position: absolute; left: 0px; height: 100%; top: 0px; opacity: 1.0; padding: 0px; margin: initial; background-color: #fff; margin-left: inherit;">
        <div class="alert-all-text">{{ Session::get('unauthorised') }}</div>
    </div>
@endif
@if(Session::has('declined'))
    <div class="alert alert-danger danger">
        <img src="../logo.png" style="position: absolute; left: 0px; height: 100%; top: 0px; opacity: 1.0; padding: 0px; margin: initial; background-color: #fff; margin-left: inherit;">
        <div class="alert-all-text">{{ Session::get('declined') }}</div>
    </div>
@endif
@if(Session::has('approved'))
    <div class="alert alert-success success">
        <img src="../logo.png" style="position: absolute; left: 0px; height: 100%; top: 0px; opacity: 1.0; padding: 0px; margin: initial; background-color: #fff; margin-left: inherit;">
        <div class="alert-all-text">{{ Session::get('approved') }}</div>
    </div>
@endif

@if(Session::has('unlocked'))
    <div class="alert alert-danger danger">
        <img src="../logo.png" style="position: absolute; left: 0px; height: 100%; top: 0px; opacity: 1.0; padding: 0px; margin: initial; background-color: #fff; margin-left: inherit;">
        <div class="alert-all-text">{{ Session::get('unlocked') }}</div>
    </div>
@endif
@if(Session::has('locked'))
    <div class="alert alert-success success">
        <img src="../logo.png" style="position: absolute; left: 0px; height: 100%; top: 0px; opacity: 1.0; padding: 0px; margin: initial; background-color: #fff; margin-left: inherit;">
        <div class="alert-all-text">{{ Session::get('locked') }}</div>
    </div>
@endif
@if(Session::has('unauthorised'))
<div class="alert alert-info info">
    <img src="../logo.png" style="position: absolute; left: 0px; height: 100%; top: 0px; opacity: 1.0; padding: 0px; margin: initial; background-color: #fff; margin-left: inherit;">
    <div class="alert-all-text">{{ Session::get('unauthorised') }}</div>
</div>
@endif
@if(Session::has('already_trashed'))
    <div class="alert alert-danger danger">
        <img src="../logo.png" style="position: absolute; left: 0px; height: 100%; top: 0px; opacity: 1.0; padding: 0px; margin: initial; background-color: #fff; margin-left: inherit;">
        <div class="alert-all-text">{{ Session::get('already_trashed') }}</div>
    </div>
@endif
@if(Session::has('trashed'))
    <div class="alert alert-success success">
        <img src="../logo.png" style="position: absolute; left: 0px; height: 100%; top: 0px; opacity: 1.0; padding: 0px; margin: initial; background-color: #fff; margin-left: inherit;">
        <div class="alert-all-text">{{ Session::get('trashed') }}</div>
    </div>
@endif