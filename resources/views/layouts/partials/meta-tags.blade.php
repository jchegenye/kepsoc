
	<link rel="canonical" href="http://kepsoc.org" />

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>Kenya Professional Society Of Criminology | (KEPSOC)</title>

	<meta name="description" content="A Kenyan organisation whose aims and membership pursues and promotes scholarly, scientific and professional knowledge concerning the measurement, etiology, consequences, prevention, control and treatment of crime and delinquency.">
	<meta name="keywords" content="kenya, professional, society, criminology, organisation, crime ">
	<meta name="revisit-after" content="7 days">
	<meta name="copyright" content="j-tech is a registered trade mark Co. based in Kenya">
	<meta name="author" content="Chegenye Asumu Jackson">
	<meta name="reply-to" content="chegenyejackson@gmail.com">
	<meta name="web_author" content="https://j-tech.tech">
	<meta name="abstract" content="Kenya Professional Society Of Criminology | (KEPSOC)">
	<meta name="distribution" content="global">
	<meta name="robots" content="none, noindex, nofollow">
	<meta name="googlebot" content="all, index, follow">
	<meta name="language" content="english, kiswahili">
	<meta name="rating" content="general, safe for kids">
	<meta name="identifier-URL" content="https://kepsoc.org/">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<!-- Favicon -->
	<link rel="icon" href="{{ asset('http://kepsoc.org/favicon.ico') }}">

	<script type="text/javascript">
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
	</script>
