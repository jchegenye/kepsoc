        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <!-- Main CSS files -->
        <link rel="stylesheet" href="../../../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../../../css/owl.carousel.css" />
        <link rel="stylesheet" href="../../../css/magnific-popup.css" />
        <link rel="stylesheet" href="../../../css/font-awesome.css" />
        <link rel="stylesheet" href="../../../css/style.css" />
        <link rel="stylesheet" href="../../../css/responsive.css" />
