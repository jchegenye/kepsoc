    <!-- ABOUT US -->
    <section id="about-us">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <div class="about-us text-center">
                        <h1>About Us</h1>
                        <p>
                            The Kenya Professional Society Of Criminology - (KEPSOC) is an organisation whose aims and membership pursues and promotes scholarly, scientific and 
                                professional knowledge concerning the measurement, etiology, consequences, prevention, control and treatment 
                                of crime and delinquency.
                        </p>
                        <a href="{{'more-on-about-us'}}" class="btn btn-send">Find Out More</a>
                    </div>
                </div>
                <div class="col-sm-6 our-office">
                    <div id="office-carousel" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="{{ URL::asset('images/about/about01.jpg') }}" alt="" class="img-responsive">
                            </div>
                            <div class="item">
                                <img src="{{ URL::asset('images/about/about02.jpg') }}" alt="" class="img-responsive">
                            </div>
                            <div class="item">
                                <img src="{{ URL::asset('images/about/about03.jpg') }}" alt="" class="img-responsive">
                            </div>

                            <a class="office-carousel-left" href="#office-carousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                            <a class="office-carousel-right" href="#office-carousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
                        </div>
                    </div> <!--/#office-carousel-->
                </div>
            </div>
        </div>
    </section>
    <!-- /ABOUT US -->
