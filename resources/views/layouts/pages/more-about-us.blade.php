<!DOCTYPE html>
<html lang="en">
    <head>

        <!--##################################### start of HEAD ########################################-->
        <link rel="canonical" href="http://kepsoc.org/more-on-about-us" />

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf_token" content="{{ csrf_token() }}">

        <title>Kenya Professional Society Of Criminology | About Us</title>

        <meta name="copyright" content="j-tech is a registered trade mark Co. based in Kenya">
        <meta name="author" content="Chegenye Asumu Jackson">
        <meta name="reply-to" content="chegenyejackson@gmail.com">
        <meta name="web_author" content="https://j-tech.tech">
        <meta name="distribution" content="global">
        <meta name="robots" content="none, noindex, nofollow">
        <meta name="googlebot" content="all, index, follow">
        <meta name="language" content="english, kiswahili">
        <meta name="rating" content="general, safe for kids">
        <meta name="identifier-URL" content="https://kepsoc.org/more-on-about-us">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <!-- Favicon -->
        <link rel="icon" href="{{ asset('http://kepsoc.org/favicon.ico') }}">
        <!--####################################### end of head ########################################-->

        <!--####################################### start of CSS #######################################-->
        @include('layouts.partials.css')
        <!--####################################### end of css #########################################-->

        <!--####################################### start of Google Analytics ##########################-->
        {!! Analytics::render() !!}
        <!--####################################### end of Google Analytics ############################-->
        
    </head>

    <body>

        <!-- ------------------------------------ start of PRELOADER ---------------------------------- -->
            @include('layouts.partials.preload')
        <!-- ------------------------------------ end of PRELOADER ------------------------------------ -->

        <!-- ------------------------------------- start of Header ------------------------------------ -->
            @include('layouts.partials.pages.header-for-pages')
        <!-- ------------------------------------- end of Header -------------------------------------- -->

        <section id="our-mission-vision">
            <div class="container">
                <div class="row">
                <div class="water-mark-overlay"></div>
                    <div class="col-md-6">
                        <h2>Our Mission</h2>
                        <span class="st-border"></span>
                        <p style="line-height: 2.5;">
                            Ensure Sanity, Professionalism, Discipline, Ethical and Integrity standards, Adherence to legal order and Internationally Acclaimed Best Practices of Maintaining Social Order and Security, Crime Management, Administration of Justice, Constitutionalism and the Due Process of Law, Peaceful Co-existence, Cohesion and Liberty, in order that we may achieve Sustainable Growth and Development of our people and thus effectively Entrench both Individual and Collective Happiness amongst members of our Society and beyond.
                        </p>
                    </div>
                    <div class="col-md-6 ">
                        <h2>Our Vision</h2>
                        <p>
                            A secure, just, united and happy society in harmony with herself and with the global family.
                        </p>

                        <h2>Our Motto</h2>
                        <p>
                            Professionalism in Security and Crime Management for a Secure and Just Society.
                        </p>
                    </div>

                    <div class="col-md-6">
                        
                    </div>
                    
                    <div class="col-md-6">
                        <h2>Our Strategy</h2>
                        <p>
                            To architecture a team of professionals in the criminal justice and security administration that shall constructively engage with other professionals from various disciplines and institutions, harness and synergize their knowledge and experience to strive and foster our mission and vision.
                        </p>
                    </div>
                    <div class="col-md-12 share-social-icons ">
                        <span>Share with:</span>
                        <a href=""><i class="fa fa-facebook"></i></a>
                        <a href=""><i class="fa fa-twitter"></i></a>
                        <a href=""><i class="fa fa-google-plus"></i></a>
                        <a href="whatsapp://send?text=Hi, kindly our website - Kenya Professional Society Of Criminology: http://kepsoc.org/"><i class="fa fa-whatsapp"></i></a>
                    </div>
                </div>
            </div>
        </section>

        <!-- ------------------------------------- start of Footer ------------------------------------ -->
            <footer id="footer-about">
                <div class="container">
                    <div class="row">
                        <!-- /SOCIAL ICONS -->
                        <div class="col-md-4 ">
                            <p style="text-transform:capitalize;">The KEPSOC is an organisation whose aims and membership pursues and promotes scholarly, scientific and professional knowledge concerning the measurement, etiology, consequences, prevention, control and treatment of crime and delinquency.</p>
                        </div>
                        <div class="col-md-4 text-center">
                            <p>Copyright &copy; {{date('Y')}} Kepsoc. | All Rights Reserved.</p>
                        </div>
                        <!-- SOCIAL ICONS -->
                        <div class="col-md-4 footer-social-icons">
                            <span>Follow us on</span>
                            <a href=""><i class="fa fa-facebook"></i></a>
                            <a href=""><i class="fa fa-twitter"></i></a>
                            <a href=""><i class="fa fa-google-plus"></i></a>
                            <a href="whatsapp://send?text=Hi, kindly visit our website - Kenya Professional Society Of Criminology at http://kepsoc.org/"><i class="fa fa-whatsapp"></i></a>
                        </div>
                    </div>
                </div>
            </footer>
        <!-- ------------------------------------- end of Footer -------------------------------------- -->

        <!--####################################### start of SCRIPTS #######################################-->
            @include('layouts..partials.scripts')
        <!--####################################### end of SCRIPTS #########################################-->

    </body>
</html>