<section id="core-values">

    <div class="container">
        <div class="row">

            <div class="overlay-core-values"></div>
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <h2 class="text-center">Core Values</h2>
                <div class="st-testimonials">

                    <div class="item active text-center">
                        <div class="st-border"></div>
                        <div class="client-info">
                            <h1>Professionalism</h1>
                        </div>
                    </div>

                    <div class="item text-center">
                        <div class="st-border"></div>
                        <div class="client-info">
                            <h1>Safety</h1>
                        </div>
                    </div>
                    <div class="item text-center">
                        <div class="st-border"></div>
                        <div class="client-info">
                            <h1>Justice</h1>
                        </div>
                    </div>
                    <div class="item text-center">
                        <div class="st-border"></div>
                        <div class="client-info">
                            <h1>Liberty</h1>
                        </div>
                    </div>
                    <div class="item text-center">
                        <div class="st-border"></div>
                        <div class="client-info">
                            <h1>Equality and Equity</h1>
                        </div>
                    </div>
                    <div class="item text-center">
                        <div class="st-border"></div>
                        <div class="client-info">
                            <h1>Integrity</h1>
                        </div>
                    </div>
                    <div class="item text-center">
                        <div class="st-border"></div>
                        <div class="client-info">
                            <h1>Social Order</h1>
                        </div>
                    </div>
                    <div class="item text-center">
                        <div class="st-border"></div>
                        <div class="client-info">
                            <h1>Peace and Cohesion</h1>
                        </div>
                    </div>
                    <div class="item text-center">
                        <div class="st-border"></div>
                        <div class="client-info">
                            <h1>Teamwork</h1>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>