<section id="services">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title">
                    <h1>Services</h1>
                    <span class="st-border"></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 ">
                <p><i class="fa fa-hand-o-right"></i>
                    Pursue and promote, scholarly, scientific, and professional knowledge concerning the measurements, etiology, consequences, prevention, control and treatment of crime and delinquency.
                </p>
            </div>

            <div class="col-md-6 st-service">
                <p><i class="fa fa-hand-o-right"></i>
                    Establishment, recommendation and enforcement of professional and ethical standards amongst members and to both public and private players
                </p>
            </div>

            <div class="col-md-6 st-service">
                <p><i class="fa fa-hand-o-right"></i>
                    Endeavour to professionally work with the criminal justice system, security and intelligence, investigations and forensics, prosecution, penology, disaster and conflict management, training and research in order that we may achieve our mission and vision.
                </p>
            </div>

            <div class="col-md-6 st-service">
                <p><i class="fa fa-hand-o-right"></i>
                    Architecture a team of professionals in the criminal justice and security administration that shall constructively engage with other professionals from various disciplines and institutions.
                </p>
            </div>
        </div>
    </div>
</section>
