<!DOCTYPE html>
<html lang="en">
<head>

    <!--##################################### start of HEAD ########################################-->
    <link rel="canonical" href="" />

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf_token" content="{{ csrf_token() }}">

    <title>{!! Auth::user()->name !!} | Members List @ KEPSOC ({{get_current_user()}})</title>

    <meta name="copyright" content="j-tech is a registered trade mark Co. based in Kenya">
    <meta name="author" content="Chegenye Asumu Jackson">
    <meta name="reply-to" content="chegenyejackson@gmail.com">
    <meta name="web_author" content="https://www.j-tech.tech">
    <meta name="identifier-URL" content="https://kepsoc.org/members-dashboard">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <!-- Favicon -->
    <link rel="icon" href="{{ asset('http://kepsoc.org/favicon.ico') }}">
    <!--####################################### end of head ########################################-->

    <!--####################################### start of CSS #######################################-->
    @include('layouts.partials.css')
            <!--####################################### end of css #########################################-->

    <!--####################################### start of Google Analytics ##########################-->
    {!! Analytics::render() !!}
    <!--####################################### end of Google Analytics ############################-->

</head>

<body>

<!-- ------------------------------------ start of PRELOADER ---------------------------------- -->
@include('layouts.partials.preload')
<!-- ------------------------------------ end of PRELOADER ------------------------------------ -->

<div class="container-fluid">
    <div class="row">
        <div class="dashboard-menu">
            <div class="col-md-4">
                <a href="{{url('/auth/logout')}}"><i class="fa fa-power-off">Logout</i></a><br><br>
                <a href="{{url('members/profile/' . $user->uid)}}"><i class="fa fa-user">My Profile</i></a><br><br>
                <a href="{{url('/members/dash')}}"><i class="fa fa-dashboard"> DashBoard</i></a>
            </div>
            <div class="col-md-4">
                <p class="text-center"><img src="../../../logo.png"></p>
                <h3 class="text-center">
                    <b>MEMBERS LIST</b>
                </h3>
                <P class="text-center">
                    <i class="fa fa-users "></i>
                </P><hr>
            </div>
            <div class="col-md-4 text-center">
                @if(Session::has('successful'))
                    {{ Session::get('successful') }} {{$user->username}}
                @endif
            </div>
        </div>
    </div>
</div>

<section id="our-team">
    <div class="container-fluid">
        {!! $member->links() !!}
        <div class="row">

            @foreach($member as $members)
                @foreach($profile as $profiles)
                    @if($profiles->uid == $members->uid)
                        <div class="col-md-4 members-box">
                            <div class="col-md-4">
                                @if(empty($members->profile_pic_file_name))
                                    <img class="img-responsive" src="../images/members/sample.jpg" alt="">
                                @else
                                    <img class="img-responsive" src="{{$members->profile_pic->url()}}" alt="">
                                @endif
                            </div>
                            <div class="col-md-8">
                                <p>
                                    {{str_limit($profiles->name ,35)}}
                                </p>
                                <p>
                                    {{str_limit($members->position ,35)}}
                                </p>
                                <p>
                                    {{str_limit($members->course ,35)}}
                                </p>
                                <p>
                                    {{str_limit($members->residence ,35)}}
                                </p>
                                <a href="{{url('other/members/profile/' .$profiles->uid)}}"
                                   class="btn btn-send">Read More</a>
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-whatsapp"></i></a>
                            </div>
                        </div>
                    @endif
                @endforeach
            @endforeach

        </div>

    </div>
</section>

@include('layouts.pages.members.footer')

<!--####################################### start of SCRIPTS #######################################-->
    @include('layouts..partials.scripts')
<!--####################################### end of SCRIPTS #########################################-->

</body>
</html>