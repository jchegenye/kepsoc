
    <div class="panel-group m-bot20" id="accordion">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    @if(empty($updatedMemberProfile->education_percentage))
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                            Education Details
                        </a>
                    @elseif($updatedMemberProfile->education_percentage == 20)
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#">
                            Education Details
                        </a>
                    @endif
                </h4>
                @if(empty($updatedMemberProfile->education_percentage))
                    <div class="profile-status-pending"><i class=""></i>Pending</div>
                @elseif($updatedMemberProfile->education_percentage == 20 )
                    <div class="profile-status-completed"><i class=""></i>Completed by
                        {{$updatedMemberProfile->education_percentage}}%</div>
                @endif
            </div>
            @if(empty($updatedMemberProfile->education_percentage))
                <div id="collapseTwo" class="panel-collapse collapse in">
                    <div class="panel-body">
                        @include('layouts.pages.members.profile.education_details')
                    </div>
                </div>
            @elseif($updatedMemberProfile->education_percentage == 20)
                <div id="collapseTwo" class="panel-collapse collapse">
                    <div class="panel-body">
                        @include('layouts.pages.members.profile.education_details')
                    </div>
                </div>
            @endif
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    @if(empty($updatedMemberProfile->education_percentage))
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#">
                            Work Details
                        </a>
                    @elseif($updatedMemberProfile->percentage == 20)
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            Work Details
                        </a>
                    @elseif ($updatedMemberProfile->work_percentage == 40)
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#">
                            Work Details
                        </a>
                    @endif
                </h4>
                @if(empty($updatedMemberProfile->work_percentage))
                    <div class="profile-status-pending"><i class=""></i>Pending</div>
                @elseif($updatedMemberProfile->work_percentage == 40 )
                    <div class="profile-status-completed"><i class=""></i>Completed by
                        {{$updatedMemberProfile->work_percentage}}%</div>
                @endif

            </div>

            @if(empty($updatedMemberProfile->education_percentage))
                <div id="collapseOne" class="panel-collapse collapse">
                    <div class="panel-body">
                        @include('layouts.pages.members.profile.work_details')
                    </div>
                </div>
            @elseif($updatedMemberProfile->percentage == 20)
                <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body">
                        @include('layouts.pages.members.profile.work_details')
                    </div>
                </div>
            @elseif($updatedMemberProfile->work_percentage == 40)
                <div id="collapseOne" class="panel-collapse collapse">
                    <div class="panel-body">
                        @include('layouts.pages.members.profile.work_details')
                    </div>
                </div>
            @endif
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    @if(empty($updatedMemberProfile->work_percentage))
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#">
                            Professional Skills
                        </a>
                    @elseif($updatedMemberProfile->percentage == 40)
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                            Professional Skills
                        </a>
                    @elseif($updatedMemberProfile->proffessional_percentage == 60 )
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#">
                            Professional Skills
                        </a>
                    @endif
                </h4>
                @if(empty($updatedMemberProfile->proffessional_percentage))
                    <div class="profile-status-pending"><i class=""></i>Pending</div>
                @elseif($updatedMemberProfile->proffessional_percentage == 60)
                    <div class="profile-status-completed"><i class=""></i>Completed by
                        {{$updatedMemberProfile->proffessional_percentage}}%</div>
                @else
                    <div class="profile-status-pending"><i class=""></i>Pending</div>
                @endif
            </div>

            @if(empty($updatedMemberProfile->work_percentage))
                <div id="collapseThree" class="panel-collapse collapse">
                    <div class="panel-body">
                        @include('layouts.pages.members.profile.professional_skills')
                    </div>
                </div>
            @elseif($updatedMemberProfile->percentage == 40)
                <div id="collapseThree" class="panel-collapse collapse in">
                    <div class="panel-body">
                        @include('layouts.pages.members.profile.professional_skills')
                    </div>
                </div>
            @elseif($updatedMemberProfile->proffessional_percentage == 60)
                <div id="collapseThree" class="panel-collapse collapse">
                    <div class="panel-body">
                        @include('layouts.pages.members.profile.professional_skills')
                    </div>
                </div>
            @endif
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                @if(empty($updatedMemberProfile->basic_percentage))
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#">
                            Basic Info
                        </a>
                    </h4>
                @elseif($updatedMemberProfile->basic_percentage == 80)
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                            Basic Info
                        </a>
                    </h4>
                @endif

                @if(empty($updatedMemberProfile->basic_percentage))
                    <div class="profile-status-pending"><i class=""></i>Pending</div>
                @elseif($updatedMemberProfile->basic_percentage == 80)
                    <div class="profile-status-completed"><i class=""></i>Completed by
                        {{$updatedMemberProfile->basic_percentage}}%</div>
                @else
                    <div class="profile-status-pending"><i class=""></i>Pending</div>
                @endif
            </div>
            @if(empty($updatedMemberProfile->proffessional_percentage))
                <div id="collapseFour" class="panel-collapse collapse">
                    <div class="panel-body">
                        @include('layouts.pages.members.profile.basic_info')
                    </div>
                </div>
            @elseif($updatedMemberProfile->percentage == 60)
                <div id="collapseFour" class="panel-collapse collapse in">
                    <div class="panel-body">
                        @include('layouts.pages.members.profile.basic_info')
                    </div>
                </div>
            @elseif($updatedMemberProfile->work_percentage == 80)
                <div id="collapseFour" class="panel-collapse collapse">
                    <div class="panel-body">
                        @include('layouts.pages.members.profile.basic_info')
                    </div>
                </div>
            @endif
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    @if(empty($updatedMemberProfile->about_you_percentage))
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#">
                            About You
                        </a>
                    @elseif($updatedMemberProfile->about_you_percentage == 100)
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                            About You
                        </a>
                    @endif
                </h4>
                @if(empty($updatedMemberProfile->about_you_percentage))
                    <div class="profile-status-pending"><i class=""></i>Pending</div>
                @elseif($updatedMemberProfile->about_you_percentage == 100)
                    <div class="profile-status-completed"><i class=""></i>Completed by
                        {{$updatedMemberProfile->about_you_percentage}}%</div>
                @else
                    <div class="profile-status-pending"><i class=""></i>Pending</div>
                @endif
            </div>
            @if(empty($updatedMemberProfile->basic_percentage))
                <div id="collapseFive" class="panel-collapse collapse">
                    <div class="panel-body">
                        @include('layouts.pages.members.profile.about_you')
                    </div>
                </div>
            @elseif($updatedMemberProfile->percentage == 80)
                <div id="collapseFive" class="panel-collapse collapse in">
                    <div class="panel-body">
                        @include('layouts.pages.members.profile.about_you')
                    </div>
                </div>
            @elseif($updatedMemberProfile->about_you_percentage == 100)
                <div id="collapseFive" class="panel-collapse collapse">
                    <div class="panel-body">
                        @include('layouts.pages.members.profile.about_you')
                    </div>
                </div>
            @endif
        </div>

    </div>
