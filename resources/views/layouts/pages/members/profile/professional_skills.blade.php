{{--if a certain value was already saved else allow editing--}}
<form action="{{url('add-profile/'.$memberProfile->uid)}}" class="profile-form" name="profile-form" method="post">
    <div class="row">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-sm-6">
            <div class="form-group">
                <textarea name="skills" rows="3" placeholder="Enter the skills you have & separate each with commas*">{{ old('skills') }}</textarea>
                @if ($errors->has('skills'))
                    <span class="text text-danger">{{ $errors->first('skills') }}</span>
                @endif
            </div>
        </div>
        <div class="col-sm-6">

            <div class="form-group">
                <label>Select Privacy:</label>
                <input type="radio" name="privacy_professional" value="{{ old ('privacy_professional', 'Public') }}">Public
                <input type="radio" name="privacy_professional" value="{{ old ('privacy_professional', 'Private') }}"> Private
                @if ($errors->has('privacy_professional'))
                    <span class="text text-danger">{{ $errors->first('privacy_professional') }}</span>
                @endif
            </div>
        </div>


        <div class="col-sm-12">
            <input type="submit" name="submit" value="Save" class="btn btn-send">
        </div>
    </div>
</form>