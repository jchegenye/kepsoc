{{--if profile was already saved else allow update it --}}

{{--@ if(empty($updatedMemberProfile->percentage))
    <form action="{{url('update-profile/'.$memberProfile->uid)}}" class="profile-form" name="profile-form" method="post">
        @ elseif($updatedMemberProfile->percentage == 20)--}}
            <form action="{{url('add-profile/'.$memberProfile->uid)}}" class="profile-form" name="profile-form" method="post">
                {{-- @endif--}}

    <div class="row">
        <input type="hidden" name="uid" value="{{$memberProfile->uid}}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-sm-6">
            <div class="form-group">
                <input type="text" name="course" placeholder="Enter your most current course *" value="{{old('course')  }}">
                @if($errors->has('course'))
                    <span class="text text-danger">{{ $errors->first('course') }}</span>
                @endif
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <input type="text" name="institution" placeholder="Enter your most current institution?*" value="{{ old('institution') }}">
                @if ($errors->has('institution'))
                    <span class="text text-danger">{{ $errors->first('institution') }}</span>
                @endif
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Start Date:</label>
                <input type="date" name="study_start_date" placeholder="From*" value="{{ old ('study_start_date') }}">
                @if ($errors->has('study_start_date'))
                    <span class="text text-danger">{{ $errors->first('study_start_date') }}</span>
                @endif
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>End Date: <em>(Live this blank if still on session)</em></label>
                <input type="date" name="study_end_date" placeholder="To*" value="{{ old('study_end_date') }}">
                @if ($errors->has('study_end_date'))
                    <span class="text text-danger">{{ $errors->first('study_end_date') }}</span>
                @endif
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Graduated?</label>
                <input type="checkbox" name="graduated"  value="{{ old('graduated' , 'Graduated') }}">
                @if ($errors->has('graduated'))
                    <span class="text text-danger">{{ $errors->first('graduated') }}</span>
                @endif
            </div>

            <div class="form-group">
                <label>Select Privacy:</label>
                <input type="radio" name="privacy_study" value="{{ old ('privacy_study' , 'Public' )}}">Public
                <input type="radio" name="privacy_study" value="{{ old ('privacy_study' , 'Private')}}"> Private
                @if ($errors->has('privacy_study'))
                    <span class="text text-danger">{{ $errors->first('privacy_study') }}</span>
                @endif
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <textarea name="education_description" rows="3" placeholder="Description*">{{ old('education_description') }}</textarea>
                @if ($errors->has('education_description'))
                    <span class="text text-danger">{{ $errors->first('education_description') }}</span>
                @endif
            </div>
        </div>

        <div class="col-sm-12">
            <input type="submit" name="submit" value="Save" class="btn btn-send">
        </div>
    </div>
</form>