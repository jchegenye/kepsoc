{{--if a certain value was already saved else allow editing--}}
<form action="{{url('add-profile/'.$memberProfile->uid)}}" class="profile-form" name="profile-form" method="post">
    <div class="row">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-sm-12">
            <div class="form-group">
                <textarea name="about_you" rows="3" placeholder="Tell us a little about your self*" value="{{ old('about_you') }}"></textarea>
                @if ($errors->has('about_you'))
                    <span class="text text-danger">{{ $errors->first('about_you') }}</span>
                @endif
            </div>
        </div>
        <div class="col-sm-12">
            <input type="submit" name="submit" value="Save" class="btn btn-send">
        </div>
    </div>
</form>