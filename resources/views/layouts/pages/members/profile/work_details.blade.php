{{--if profile was already saved else allow update it --}}

{{--@ if(empty($updatedMemberProfile->percentage))
    <form action="{{url('update-profile/'.$memberProfile->uid)}}" class="profile-form" name="profile-form" method="post">
        @ elseif($updatedMemberProfile->percentage == 20)--}}
            <form action="{{url('add-profile/'.$memberProfile->uid)}}" class="profile-form" name="profile-form" method="post">
               {{-- @endif--}}

    <div class="row">
        <input type="hidden" name="uid" value="{{$memberProfile->uid}}">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-sm-6">
            <div class="form-group">
                <input type="text" name="work_place" placeholder="Enter your current work place?*" value="{{ old('work_place')  }}">
                @if ($errors->has('work_place'))
                    <span class="text text-danger">{{ $errors->first('work_place') }}</span>
                @endif
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <input type="text" name="position" placeholder="Enter your current Position?*" value="{{ old('position')  }}">
                @if ($errors->has('position'))
                    <span class="text text-danger">{{ $errors->first('position') }}</span>
                @endif
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Start Date:</label>
                <input type="date" name="start_work_date" placeholder="Start Date*" value="{{ old ('start_work_date') }}">
                @if ($errors->has('start_work_date'))
                    <span class="text text-danger">{{ $errors->first('start_work_date') }}</span>
                @endif
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>End Date: <i>(Live this blank if still work here)</i></label>
                <input type="date" name="end_work_date" placeholder="End Date*" value="{{ old('end_work_date') }}">
                @if ($errors->has('end_work_date'))
                    <span class="text text-danger">{{ $errors->first('end_work_date') }}</span>
                @endif
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Currently Work Here?</label>
                <input type="checkbox" name="current_work"  title="Currently Work Here?" value="Present">
                @if ($errors->has('current_work'))
                    <span class="text text-danger">{{ $errors->first('current_work') }}</span>
                @endif
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Select Privacy:</label>
                <input type="radio" name="privacy_work" title="Select Privacy" value="public">Public
                <input type="radio" name="privacy_work" title="Select Privacy" value="private">Private
                @if ($errors->has('privacy_work'))
                    <span class="text text-danger">{{ $errors->first('privacy_work') }}</span>
                @endif
            </div>
        </div>

        <div class="col-sm-12">
            <input type="submit" name="submit" value="Save" class="btn btn-send">
        </div>
    </div>
</form>