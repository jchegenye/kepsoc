
<form action='/add-profile/{{$memberProfile->uid}}' class='profile-form' method='post' enctype='multipart/form-data'>
    <div class="row">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-sm-6">
            <div class="form-group">
                <input type="text" name="phone_number" placeholder="What's your phone number?*" value="{{ old ('phone_number')  }}">
                @if ($errors->has('phone_number'))
                    <span class="text text-danger">{{ $errors->first('phone_number') }}</span>
                @endif
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
            <label>Select a passport size photo <em>(1mb Maximum)</em></label>
                <input type="file" name="profile_pic" id="input" placeholder="Enter Passport Photo">
                @if ($errors->has('profile_pic'))
                    <span class="text text-danger">{{ $errors->first('profile_pic') }}</span>
                @endif
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <input type="text" name="residence" placeholder="Place you live*" value="{{ old('residence') }}">
                @if ($errors->has('residence'))
                    <span class="text text-danger">{{ $errors->first('residence') }}</span>
                @endif
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Birthday: <em>(You can live this blank if you wish)</em></label>
                <input type="date" name="birthday" placeholder="Birthday" value="{{  old ('birthday') }}">
                @if ($errors->has('birthday'))
                    <span class="text text-danger">{{ $errors->first('birthday') }}</span>
                @endif
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <textarea name="languages" rows="3" placeholder="Languages both Written & Spoken*" >{{ old('languages') }}</textarea>
                @if ($errors->has('languages'))
                    <span class="text text-danger">{{ $errors->first('languages') }}</span>
                @endif
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Gender:</label>
                <input type="radio" name="gender" value="Male">Male
                <input type="radio" name="gender" value="Female">Female
                @if ($errors->has('gender'))
                    <span class="text text-danger">{{ $errors->first('gender') }}</span>
                @endif
            </div>
        </div>


        <div class="col-sm-12">
            <input type="submit" name="submit" value="Save" class="btn btn-send">
        </div>
    </div>
</form>