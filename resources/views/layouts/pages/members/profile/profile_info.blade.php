<div class="row">
    <div class="col-sm-12">
        <div class="profile-box-info">
            <h5>Work</h5><hr>
            <p>
                <i class="fa fa-briefcase"> works at</i>
                {{$updatedMemberProfile->work_place or ''}}
                <i class="fa">as</i>
                {{$updatedMemberProfile->position or ''}}
            </p>
            <p>
                <i class="fa fa-calendar"> since</i>
                {{$updatedMemberProfile->start_work_date or ''}}
                <i class="fa">to</i>
                {{$updatedMemberProfile->end_work_date or ''}}.
                {{$updatedMemberProfile->current_work or ''}}
            </p>
        </div>

        <div class="profile-box-info">
            <h5>Education</h5><hr>
            <p>
                <i class="fa fa-graduation-cap"> studied</i>
                {{$updatedMemberProfile->course or ''}}
                <i class="fa">at</i>
                {{$updatedMemberProfile->institution or ''}}
            </p>
            <p>
                <i class="fa fa-calendar"> since</i>
                {{$updatedMemberProfile->study_start_date or ''}}
                <i class="fa">to</i>
                {{$updatedMemberProfile->study_end_date or ''}}.
                {{$updatedMemberProfile->graduated or ''}}
            </p>
            <p>
                <i class="fa fa-info"></i>
                {{$updatedMemberProfile->education_description or ''}}
            </p>
        </div>

        <div class="profile-box-info">
            <h5>Professional Skills</h5><hr>
            <p>
                <i class="fa fa-tasks"> skilled in </i>
                {{$updatedMemberProfile->skills or ''}}
            </p>
        </div>

        <div class="profile-box-info">
            <h5>Basic Info</h5><hr>
            <p>
                <i class="fa fa-mobile-phone"> phone number</i>
                {{$updatedMemberProfile->phone_number or ''}}
            </p>
            <p>
                <i class="fa fa-home"> resides at</i>
                {{$updatedMemberProfile->residence or ''}}
            </p>
            <p>
                <i class="fa fa-birthday-cake"> born in</i>
                {{$updatedMemberProfile->birthday or ''}}
            </p>
            <p>
                <i class="fa fa-language"> languages</i>
                {{$updatedMemberProfile->languages or ''}}
            </p>
            <p>
                <i class="fa fa-genderless"> gender</i>
                {{$updatedMemberProfile->gender or ''}}
            </p>
        </div>

        <div class="profile-box-info">
            <h5>About {{$memberProfile->username or ''}}</h5><hr>
            <p>
                <i class="fa fa-user-times"></i>
                {{$updatedMemberProfile->about_you or ''}}
            </p>
        </div>
    </div>

</div>

