<!DOCTYPE html>
<html lang="en">
<head>

    <!--##################################### start of HEAD ########################################-->
    <link rel="canonical" href="http://kepsoc.org/members-dashboard" />

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf_token" content="{{ csrf_token() }}">

    <title>{!! Auth::user()->name !!} | Members Dashboard @ KEPSOC ({{get_current_user()}})</title>

    <meta name="copyright" content="j-tech is a registered trade mark Co. based in Kenya">
    <meta name="author" content="Chegenye Asumu Jackson">
    <meta name="reply-to" content="chegenyejackson@gmail.com">
    <meta name="web_author" content="https://www.j-tech.tech">
    <meta name="identifier-URL" content="https://kepsoc.org/members-dashboard">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <!-- Favicon -->
    <link rel="icon" href="{{ asset('http://kepsoc.org/favicon.ico') }}">
    <!--####################################### end of head ########################################-->

    <!--####################################### start of CSS #######################################-->
    @include('layouts.partials.css')
    <!--####################################### end of css #########################################-->

    <!--####################################### start of Google Analytics ##########################-->
    {!! Analytics::render() !!}
    <!--####################################### end of Google Analytics ############################-->

</head>

<body>

    <!-- ------------------------------------ start of PRELOADER ---------------------------------- -->
    @include('layouts.partials.preload')
    <!-- ------------------------------------ end of PRELOADER ------------------------------------ -->

    <div class="container">
        @if(Session::has('unsuccessful'))
            <div class="alert alert-danger danger">
                <img src="../../logo.png" style="position: absolute; left: 0px; height: 100%; top: 0px; opacity: 1.0; padding: 0px; margin: initial; background-color: #fff; margin-left: inherit;">
                <div class="alert-all-text">{{ Session::get('unsuccessful') }}</div>
            </div>
        @endif

        @if(Session::has('unauthorised'))
            <div class="alert alert-info info">
                <img src="../../logo.png" style="position: absolute; left: 0px; height: 100%; top: 0px; opacity: 1.0; padding: 0px; margin: initial; background-color: #fff; margin-left: inherit;">
                <div class="alert-all-text">{{ Session::get('unauthorised') }}</div>
            </div>
        @endif
        
        <div class="dashboard">

            <div class="row">
                <div class="col-md-3 ">
                    <a href="{{url('auth/logout')}}">
                        <div class="dashboard-logout">
                            <i class="fa fa-power-off">Logout</i>
                        </div>
                    </a>
                </div>
                <div class="col-md-6">

                    <div class="row text-center">
                        <img src="../../../logo.png">
                    </div>

                    <P class="text-center">
                        <i class="fa fa-dashboard "></i>
                    </P>
                    <P class="text-center">
                        <b>MEMBERS DASHBOARD</b>
                    </P>

                </div>
                <div class="col-md-3 dashboard-logout">
                    @if(Session::has('successful'))
                        {{ Session::get('successful') }} {{$user->username}}
                    @endif
                </div>
            </div>

                <div class="row text-center">

                    <div class="col-md-3"></div>

                    <a href="{{url('/members/list')}}">
                        <div class="col-md-2 dashboard-boxes-individual">
                            <i class="fa fa-users"></i>
                            <p>
                                Members List
                            </p>
                        </div>
                    </a>
                    <a href="{{url('members/profile/' . $user->uid)}}">
                        <div class="col-md-2 dashboard-boxes-individual">
                            <i class="fa fa-user"></i>
                            <p>
                                My Profile
                            </p>
                        </div>
                    </a>
                    <a href="#">
                        <div class="col-md-2 dashboard-boxes-individual">
                            <i class="fa fa-book"></i>
                            <p>
                                Members Library
                            </p>
                        </div>
                    </a>
                    <div class="col-md-3"></div>

                </div>

            </div>

    </div>

    @include('layouts.pages.members.footer')

    <!--####################################### start of SCRIPTS #######################################-->
    @include('layouts..partials.scripts')
    <!--####################################### end of SCRIPTS #########################################-->

</body>
</html>
