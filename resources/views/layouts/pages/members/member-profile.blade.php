<!DOCTYPE html>
<html lang="en">
<head>

    <!--##################################### start of HEAD ########################################-->
    <link rel="canonical" href="http://kepsoc.org/members-dashboard" />

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf_token" content="{{ csrf_token() }}">

    <title>{!! Auth::user()->name !!} | Members Profile @ KEPSOC ({{get_current_user()}})</title>

    <meta name="copyright" content="j-tech is a registered trade mark Co. based in Kenya">
    <meta name="author" content="Chegenye Asumu Jackson">
    <meta name="reply-to" content="chegenyejackson@gmail.com">
    <meta name="web_author" content="https://www.j-tech.tech">
    <meta name="identifier-URL" content="https://kepsoc.org/members-dashboard">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <!-- Favicon -->
    <link rel="icon" href="{{ asset('http://kepsoc.org/favicon.ico') }}">
    <!--####################################### end of head ########################################-->

    <!--####################################### start of CSS #######################################-->
    @include('layouts.partials.css')
            <!--####################################### end of css #########################################-->

    <!--####################################### start of Google Analytics ##########################-->
    {!! Analytics::render() !!}
    <!--####################################### end of Google Analytics ############################-->

</head>

<body>

<!-- ------------------------------------ start of PRELOADER ---------------------------------- -->
@include('layouts.partials.preload')
<!-- ------------------------------------ end of PRELOADER ------------------------------------ -->

<div class="container">

    <div class="">

        <div class="col-md-12">
            <a href="{{url('auth/logout')}}">
                <div class="dashboard-logout">
                    <i class="fa fa-power-off"></i> Logout
                </div>
            </a>
        {{--</div>
        <div class="col-md-6 col-md-push-5">--}}
            <a href="{{url('#')}}">
                <div class="profile-right-conner">
                    <i class="fa fa-bell"></i> Notification
                </div>
            </a>
        </div>
    </div>

    <div class="row ">

        <div class="col-md-2 text-center">
            <a href="{{url('members/dash')}}">
                <div class="dashboard-boxes-individual">
                    <i class="fa fa-dashboard"></i>
                    <p>
                        DashBoard
                    </p>
                </div>
            </a>
            <a href="{{url('/members/list')}}">
                <div class=" dashboard-boxes-individual">
                    <i class="fa fa-users"></i>
                    <p>
                        Members List
                    </p>
                </div>
            </a>

            <a href="{{url('#')}}">
                <div class=" dashboard-boxes-individual">
                    <i class="fa fa-book"></i>
                    <p>
                        Members Library
                    </p>
                </div>
            </a>
        </div>

        <div class="col-md-10 profile-box">
            <div class="col-md-2 ">
                @if(empty($updatedMemberProfile->profile_pic_file_name))
                    <img class="img-responsive" src="../../../images/members/sample.jpg" alt="">
                @else
                    <img class="img-responsive" src="{{$updatedMemberProfile->profile_pic->url()}}" alt="">
                @endif
                <p class="social">
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-whatsapp"></i></a>
                </p>
            </div>
            <div class="col-md-8 ">
                <h4>{{$memberProfile->name or ''}}</h4>
                <p><b>Username:</b> {{$memberProfile->username or ''}}</p>
                <p><b>Joined:</b> {{$memberProfile->signed_date or ''}}</p>
                <p><b>Email:</b> {{$memberProfile->email or ''}}</p>

                <p>{{ $updatedMemberProfile->percentage or '0' }}% Profile Completion</p>
                <div class="progress progress-striped active progress-sm">
                    <div class="progress-bar progress-bar-success"  role="progressbar" aria-valuenow="{{ $updatedMemberProfile->percentage or '0' }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ $updatedMemberProfile->percentage or '0'}}%">
                        <span class="sr-only">{{ $updatedMemberProfile->percentage or '0' }}% Profile Completion</span>
                    </div>
                </div>
                @if(Auth::user()->uid  == $memberProfile->uid)
                    <a class="btn btn-send" href="{{url('/member/edit/profile/' . $memberProfile->uid)}}">Edit Profile</a>
                @else

                @endif

            </div>
        </div>
        {{--<div class="col-md-12 text-center">

        </div>--}}
        <div class="col-md-10">
            <section class="profile-panel editProfile">
                <header class="panel-heading tab-bg-primary ">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#about_me" >Profile Info</a>
                        </li>
                        {{--<li class="">
                            <a data-toggle="tab" href="#other_profiles">Other Profiles</a>
                        </li>--}}
                    </ul>
                </header>

                @if(empty($updatedMemberProfile->mid))
                    <div class="panel-body">
                        <div class="tab-content">
                            <div id="about_me" class="tab-pane active">
                                <p class="alert alert-warning">
                                    Your profile is *Empty, Kindly complete your profile to
                                    appear in the member's list!
                                </p>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="panel-body">
                        <div class="tab-content">
                            <div id="about_me" class="tab-pane active">
                                @include('layouts.pages.members.profile.profile_info')
                            </div>
                            {{--<div id="other_profiles" class="tab-pane">

                            </div>--}}
                        </div>
                    </div>
                @endif
            </section>
        </div>

    </div>


</div>

@include('layouts.pages.members.footer')

<!--####################################### start of SCRIPTS #######################################-->
@include('layouts.partials.scripts')
<!--####################################### end of SCRIPTS #########################################-->

</body>
</html>
