<footer id="footer-2">
    <div class="container">
        <div class="row">
            <div class="col-md-4 "></div>
            <div class="col-md-4 text-center">
                <p>Copyright &copy; {{date('Y')}} Kepsoc. | All Rights Reserved.</p>
            </div>
        </div>
    </div>
</footer>