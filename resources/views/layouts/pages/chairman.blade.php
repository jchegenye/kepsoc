    <section id="chairman">
        <div class="container">
            <div class="row">
                <div class="overlay-core-chairman"></div>
                {{--<div class="col-md-12">
                    <h5 style="color: #fff; text-transform: lowercase;">Chair’s Welcome Message</h5>
                    <p class="header-line"><img src="images/p.png"></p>
                </div>--}}
                    <div class="col-sm-3">
                        <!-- <p class="chairmans-title">Chair’s Welcome Message</p> -->
                        <a href="#">
                          <img class="img-responsive" src="images/members/member1.jpg" alt="Chairman's photo">
                        </a>
                        <div class="member-info-chairman">
                            <h5>Munene Mugambi</h5>
                            <span>Chairman</span>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="chairmans-body">
                            <h4>Chair’s Welcome Message</h4>
                            <p>
                                <i class="fa fa-quote-left"></i>
                                Kenya Professional Society Of Criminology, KEPSOC , was founded in year 2011 and registered in the same under the 
                                Societies Act of Kenya.  Criminology and Security studies having 
                                started to gain mileage as courses on offer by Pubic universities 
                                and middle level training institutions opened  new thinking on how  
                                scholars pursuing those disciplines could network and relate better 
                                and a well structured platform. This thinking ushered in KEPSOC with 
                                the intention of benchmarking with other deep  rooted  bodies. 
                                Hence in the development of KEPSOC Constitution and Code of ethics 
                                we aligned our works with the progressive works of America Society 
                                Of Criminology, South Africa Society Of Criminology and Hong Kong 
                                Society of Criminology. Over the years we have been able ; albeit  
                                gradually and progressively to  a stamp our presence and felt by 
                                scholars in the said disciplines across all Kenya’s  universities 
                                and middle level colleges.  
                            </p>
                            <p>
                                As a society we aim to promote scholarly, scientific and professional 
                                owledge concerning the measurement, etiology, consequences, prevention, control, 
                                and treatment of crime and delinquency. We dwell with the establishment, 
                                recommendation and enforcement of professional and ethical standards amongst our 
                                members with the aim of ensuring sanity, professionalism, ethics and adherence to 
                                international practices of maintaining social order, administration of justice, 
                                constitutionalism and the due process of law, peaceful co-existence, 
                                liberty and the individual pursuit of happiness.
                            </p>
                            <p>
                                The founder Patron is Dr. Eric Bor , fmr COD PSSS Egeron University.  
                                <i class="fa fa-quote-right"></i>
                            </p>
                       </div>
                </div>


                

            </div>
        </div>
    </section>

<!-- <section id="reviews">

    <div class="container">

        <div class="row">

            <div style="top:7em; padding-left: 15px; position: fixed; max-width: 1150px; color: #000;">
                    
                <a style="color:#d94e33;" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample" style="color:#fff;"> 
                    <span class="glyphicon glyphicon-collapse-down fade in fade out" aria-hidden="true"></span> Chair’s Welcome Message Munene Mugambi. 
                </a>

                <div  class="collapse" id="collapseExample">
                    
            <div class="media-body" style="background-color:rgba(160, 60, 39, 0.98); padding:20px; ">

                        <div class="col-sm-3">
                            <a href="#">
                              <img style=" height:150px; width: 100%;" src="images/members/member1.jpg" alt="Chairman's photo">
                            </a>
                            <div class="member-info-chairman">
                                <h5>Munene Mugambi</h5>
                                <p>Chairman</p>
                            </div>
                        </div>

                        
                            <div class="col-sm-9">
                                <p>
                                    “Kenya Professional Society Of Criminology, KEPSOC , 
                                    was founded in year 2011 and registered in the same under the 
                                    Societies Act of Kenya.  Criminology and Security studies having 
                                    started to gain mileage as courses on offer by Pubic universities 
                                    and middle level training institutions opened  new thinking on how  
                                    scholars pursuing those disciplines could network and relate better 
                                    and a well structured platform. This thinking ushered in KEPSOC with 
                                    the intention of benchmarking with other deep  rooted  bodies. 
                                    Hence in the development of KEPSOC Constitution and Code of ethics 
                                    we aligned our works with the progressive works of America Society 
                                    Of Criminology, South Africa Society Of Criminology and Hong Kong 
                                    Society of Criminology. Over the years we have been able ; albeit  
                                    gradually and progressively to  a stamp our presence and felt by 
                                    scholars in the said disciplines across all Kenya’s  universities 
                                    and middle level colleges.  
                                </p>
                            </div>
                            <div class="col-md-12">
                                <p>
                                    As a society we aim to promote scholarly, scientific and professional 
                                    owledge concerning the measurement, etiology, consequences, prevention, control, 
                                    and treatment of crime and delinquency. We dwell with the establishment, 
                                    recommendation and enforcement of professional and ethical standards amongst our 
                                    members with the aim of ensuring sanity, professionalism, ethics and adherence to 
                                    international practices of maintaining social order, administration of justice, 
                                    constitutionalism and the due process of law, peaceful co-existence, 
                                    liberty and the individual pursuit of happiness.
                                </p>
                                <p>
                                    The founder Patron is Dr. Eric Bor , fmr COD PSSS Egeron University.  
                                    ”
                                </p>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</section>
 -->