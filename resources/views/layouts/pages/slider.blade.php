	    <section id="slider">
	      <div id="home-carousel" class="carousel slide" data-ride="carousel">            
	        <div class="carousel-inner">
	          <div class="item active" style="background-image: url(images/home.jpg)">
	              <div class="carousel-caption ">
	                <div class="row">
	                  <div class="col-sm-12">
	                      <h2>Kenya Professional Society Of Criminology</h2>
	                      <p style="opacity: 0.6;">
	                          The KEPSOC is an organisation whose aims and membership pursues and promotes scholarly, scientific and 
	                          professional knowledge concerning the measurement, etiology, consequences, prevention, control and treatment 
	                          of crime and delinquency.
	                      </p>
	                      <a href="{{'more-on-about-us'}}" class="btn subscribe-btn-send">
	                      	READ MORE
	                      	<!-- <br><i class="fa fa-angle-down"></i> -->
	                      </a>
	                  </div>
	                 <div class="container">
	                 <div class="col-md-3"></div>
					    <div class="col-md-6">
					        <div class="row">
					        <a href="#">
					        	<div class="home-stand">
	                                <a href=""><i class="fa fa-facebook"></i>acebook</a>
	                                <a href=""><i class="fa fa-twitter"></i>witter</a>
	                                <a href=""><i class="fa fa-google-plus"></i>oogle</a>
	                                <a href="whatsapp://send?text=Hi, kindly visit our website - Kenya Professional Society Of Criminology at http://kepsoc.org/"><i class="fa fa-whatsapp"></i>WhatsApp</a>
					        	</div>
					        </a>
					        </div>     
					    </div>
					<div class="col-md-3"></div>
					</div>
	                </div>
	              </div>                  
	            </div>
	            
	        </div>      
	      </div> 
	    </section>
