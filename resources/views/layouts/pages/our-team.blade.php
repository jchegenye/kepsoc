    <section id="our-team">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h1>EXECUTIVE BOARD</h1>
                        <span class="st-border"></span>
                    </div>
                </div>
            </div>


        <div id="myCarousel" class="carousel slide" data-ride="carousel">

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">

                <!-- 1. PAGE -->
                <div class="row">
                    {{--<div class="item active">--}}
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <div class="team-member">
                                    <div class="member-info-header">
                                        <h6>Munene M. Mugambi</h6>
                                        <p>Chairman</p>
                                    </div>
                                    <div class="member-image">
                                        <img class="" src="images/members/member1.jpg" alt="">
                                        <div class="member-social">
                                            <p><a href="{{'read-more'}}" class="btn btn-send-member">Read More</a></p>
                                            <a href=""><i class="fa fa-facebook"></i></a>
                                            <a href=""><i class="fa fa-twitter"></i></a>
                                            <a href=""><i class="fa fa-google-plus"></i></a>
                                            <a href=""><i class="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                        <div class="member-info">
                                            <br>
                                        </div>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="team-member">
                                    <div class="member-info-header">
                                        <h6>Beatrice Kiraguri</h6>
                                        <p>V/Chair</p>
                                    </div>
                                    <div class="member-image">
                                        <img class="" src="images/members/sample.jpg" alt="">
                                        <div class="member-social">
                                            <p><a href="{{'read-more'}}" class="btn btn-send-member">Read More</a></p>
                                            <a href=""><i class="fa fa-facebook"></i></a>
                                            <a href=""><i class="fa fa-twitter"></i></a>
                                            <a href=""><i class="fa fa-google-plus"></i></a>
                                            <a href=""><i class="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                        <div class="member-info">
                                            <br>
                                        </div>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="team-member">
                                    <div class="member-info-header">
                                        <h6>Clinton Lagoon obongo</h6>
                                        <p>Secretary Gen</p>
                                    </div>
                                    <div class="member-image">
                                        <img class="" src="images/members/sample.jpg" alt="">
                                        <div class="member-social">
                                            <p><a href="{{'read-more'}}" class="btn btn-send-member">Read More</a></p>
                                            <a href=""><i class="fa fa-facebook"></i></a>
                                            <a href=""><i class="fa fa-twitter"></i></a>
                                            <a href=""><i class="fa fa-google-plus"></i></a>
                                            <a href=""><i class="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                        <div class="member-info">
                                            <br>
                                        </div>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="team-member">
                                    <div class="member-info-header">
                                        <h6>David C. Kiboi</h6>
                                        <p>V/Sec</p>
                                    </div>
                                    <div class="member-image">
                                        <img class="" src="images/members/sample.jpg" alt="">
                                        <div class="member-social">
                                            <p><a href="{{'read-more'}}" class="btn btn-send-member">Read More</a></p>
                                            <a href=""><i class="fa fa-facebook"></i></a>
                                            <a href=""><i class="fa fa-twitter"></i></a>
                                            <a href=""><i class="fa fa-google-plus"></i></a>
                                            <a href=""><i class="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                        <div class="member-info">
                                            <br>
                                        </div>

                                </div>
                            </div>
                        </div>
                </div>

        <!-- -------------------------------------------------------------------------- -->
            <div class="row">
                <div class="col-md-12">
                    <br>
                    <div class="col-md-3">
                        <div class="team-member">
                            <div class="member-info-header">
                                <h6>George Omondi Arum</h6>
                                <p>Communication and Public Relations officer</p>
                            </div>
                            <div class="member-image">
                                <img class="" src="images/members/georgearum.jpg" alt="">
                                <div class="member-social">
                                    <p><a href="{{'read-more'}}" class="btn btn-send-member">Read More</a></p>
                                    <a href=""><i class="fa fa-facebook"></i></a>
                                    <a href=""><i class="fa fa-twitter"></i></a>
                                    <a href=""><i class="fa fa-google-plus"></i></a>
                                    <a href=""><i class="fa fa-linkedin"></i></a>
                                </div>
                                </div>
                                <div class="member-info">
                                    <br>
                                </div>

                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="team-member">
                            <div class="member-info-header">
                                <h6>Andrew Budembeshe</h6>
                                <p> Media, publications and IT<br><br></p>
                            </div>
                            <div class="member-image">
                                <img class="" src="images/members/andrew_budembeshe.jpg" alt="">
                                <div class="member-social">
                                    <p><a href="{{'read-more'}}" class="btn btn-send-member">Read More</a></p>
                                    <a href=""><i class="fa fa-facebook"></i></a>
                                    <a href=""><i class="fa fa-twitter"></i></a>
                                    <a href=""><i class="fa fa-google-plus"></i></a>
                                    <a href=""><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                            <div class="member-info">
                                <br>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="team-member">
                            <div class="member-info-header">
                                <h6>Elizabeth Karimi</h6>
                                <p>Treasurer<br><br></p>
                            </div>
                            <div class="member-image">
                                <img class="" src="images/members/sample.jpg" alt="">
                                <div class="member-social">
                                    <p><a href="{{'read-more'}}" class="btn btn-send-member">Read More</a></p>
                                    <a href=""><i class="fa fa-facebook"></i></a>
                                    <a href=""><i class="fa fa-twitter"></i></a>
                                    <a href=""><i class="fa fa-google-plus"></i></a>
                                    <a href=""><i class="fa fa-linkedin"></i></a>
                                </div>
                                </div>
                                <div class="member-info">
                                    <br>
                                </div>

                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="team-member">
                            <div class="member-info-header">
                                <h6>Joseph Karuri</h6>
                                <p>Legal Liasion officer<br><br></p>
                            </div>
                            <div class="member-image">
                                <img class="" src="images/members/joelkaruri.jpg" alt="">
                                <div class="member-social">
                                    <p><a href="{{'read-more'}}" class="btn btn-send-member">Read More</a></p>
                                    <a href=""><i class="fa fa-facebook"></i></a>
                                    <a href=""><i class="fa fa-twitter"></i></a>
                                    <a href=""><i class="fa fa-google-plus"></i></a>
                                    <a href=""><i class="fa fa-linkedin"></i></a>
                                </div>
                                </div>
                                <div class="member-info">
                                    <br>
                                </div>

                        </div>
                    </div>
                </div>
            </div>

            <!-- -------------------------------------------------------------------------- -->
            <div class="row">
                <div class="col-md-12">
                    <br>

                    <div class="col-md-3">
                         <div class="team-member">
                             <div class="member-info-header">
                                 <h6>Major (Rtd) Hosea Bowen</h6>
                                 <p>Committees Liason Officer</p>
                             </div>
                             <div class="member-image">
                                 <img class="" src="images/members/hoseabowen.jpg" alt="">
                                 <div class="member-social">
                                     <p><a href="{{'read-more'}}" class="btn btn-send-member">Read More</a></p>
                                     <a href=""><i class="fa fa-facebook"></i></a>
                                     <a href=""><i class="fa fa-twitter"></i></a>
                                     <a href=""><i class="fa fa-google-plus"></i></a>
                                     <a href=""><i class="fa fa-linkedin"></i></a>
                                 </div>
                                 </div>
                                 <div class="member-info">
                                     <br>
                                 </div>

                         </div>
                     </div>
                    <div class="col-md-3">
                        <div class="team-member">
                            <div class="member-info-header">
                                <h6>Emma Libwob</h6>
                                <p> Student liason officer</p>
                            </div>
                            <div class="member-image">
                                <img class="" src="images/members/sample.jpg" alt="">
                                <div class="member-social">
                                    <p><a href="{{'read-more'}}" class="btn btn-send-member">Read More</a></p>
                                    <a href=""><i class="fa fa-facebook"></i></a>
                                    <a href=""><i class="fa fa-twitter"></i></a>
                                    <a href=""><i class="fa fa-google-plus"></i></a>
                                    <a href=""><i class="fa fa-linkedin"></i></a>
                                </div>
                             </div>
                             <div class="member-info">
                                 <br>
                             </div>

                        </div>
                    </div>

                    <div class="col-md-3">
                         <div class="team-member">
                             <div class="member-info-header">
                                 <h6>Dickson G. Njiru</h6>
                                 <p>Chief Coordinator</p>
                             </div>
                             <div class="member-image">
                                 <img class="" src="images/members/sample.jpg" alt="">
                                 <div class="member-social">
                                     <p><a href="{{'read-more'}}" class="btn btn-send-member">Read More</a></p>
                                     <a href=""><i class="fa fa-facebook"></i></a>
                                     <a href=""><i class="fa fa-twitter"></i></a>
                                     <a href=""><i class="fa fa-google-plus"></i></a>
                                     <a href=""><i class="fa fa-linkedin"></i></a>
                                 </div>
                             </div>
                                 <div class="member-info">
                                     <br>
                                 </div>

                         </div>
                    </div>
                    <div class="col-md-3">
                         <div class="team-member">
                             <div class="member-info-header">
                                 <h6>Wangui G. Kariuki</h6>
                                 <p>Board technical Person</p>
                             </div>
                             <div class="member-image">
                                 <img class="" src="images/members/sample.jpg" alt="">
                                 <div class="member-social">
                                     <p><a href="{{'read-more'}}" class="btn btn-send-member">Read More</a></p>
                                     <a href=""><i class="fa fa-facebook"></i></a>
                                     <a href=""><i class="fa fa-twitter"></i></a>
                                     <a href=""><i class="fa fa-google-plus"></i></a>
                                     <a href=""><i class="fa fa-linkedin"></i></a>
                                 </div>
                             </div>
                                 <div class="member-info">
                                     <br>
                                 </div>
                         </div>
                    </div>
                </div>
            </div>
                   {{-- </div>--}}

                    <!-- Indicators -->
                {{--<ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                </ol>--}}

                <!-- Left and right controls -->
               {{-- <a class="left carousel-indicators" href="#myCarousel" role="button" data-slide="prev" style="padding-right:20px; bottom: 50%;">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-indicators" href="#myCarousel" role="button" data-slide="next" style="padding-left:20px; bottom: 50%;">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>--}}
                    </div>
                </div>
            </div>
        </div>

    </section>
