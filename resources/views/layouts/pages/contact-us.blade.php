    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 contact-info">
                    <div class="section-title">
                            <h1>Contact Info</h1>
                            <span class="st-border"></span>
                    </div>
                    <p class="contact-content">
                        We always stay in touch with you. You can ask us any questions you may have.</p>
                        <p>
                            <b>We are located </b> - Naivasha Road, Next to Equity Bank, Opp Precious Blood Riruta.
                            Mbugua Plaza,
                            First Floor, Rightwing.
                        </p>
                    
                    <p class="st-address"><i class="fa fa-map-marker"></i>
                        <strong> Kenya Professional Society of Criminology.</strong> 
                        <br>P.O Box 50272 - 00200
                        <br>Nairobi Kenya </br>
                    </p>
                    <p class="st-phone"><i class="fa fa-mobile"></i> 
                        <strong>0738290502, 0721481236, 0721259168</strong>
                    </p>
                    <p class="st-email"><i class="fa fa-envelope-o"></i>
                    <a href="mailto:kepsoc@gmail.com?cc=&bcc=&subject=Contact-Us/%20Inquiry&body=Hello Kepsoc?">kepsoc@gmail.com</a>
                    <p class="st-website"><i class="fa fa-globe"></i> <strong>www.kepsoc.org</strong></p>

                </div>
                <div class="col-sm-7 col-sm-offset-1">
                    @if(Session::has('successfull'))
                        <div class="alert alert-success text-center">
                          {{ Session::get('successfull') }}
                        </div>
                    @endif
                    <div class="section-title">
                        <h1>Contact Form</h1>
                        <span class="st-border"></span>
                    </div>
                    <form action="{{'send-contact'}}" class="contact-form" name="contact-form" method="post">
                        <div class="row">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="name" placeholder="Name*" value="{{ old('name') }}">
                                    @if ($errors->has('name')) 
                                      <span class="text text-danger">{{ $errors->first('name') }}</span> 
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="email" name="email" placeholder="Email*" value="{{ old('email') }}">
                                    @if ($errors->has('email')) 
                                      <span class="text text-danger">{{ $errors->first('email') }}</span> 
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="phonenumber" placeholder="Phone number*" value="{{ old('phonenumber') }}">
                                    @if ($errors->has('phonenumber')) 
                                      <span class="text text-danger">{{ $errors->first('phonenumber') }}</span> 
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="subject" placeholder="Subject*" value="{{ old('subject') }}">
                                    @if ($errors->has('subject')) 
                                      <span class="text text-danger">{{ $errors->first('subject') }}</span> 
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <textarea name="bodyMessage" rows="5" placeholder="Message*" value="{{ old('bodyMessage') }}"></textarea>
                                    @if ($errors->has('bodyMessage')) 
                                      <span class="text text-danger">{{ $errors->first('bodyMessage') }}</span> 
                                    @endif
                                </div>
                            </div>

                            @if(app()->environment() == 'production')
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Recaptcha::render() !!}
                                        @if ($errors->has('g-recaptcha-response'))
                                            <span class="text text-danger">{{ $errors->first('g-recaptcha-response') }}</span>
                                        @endif
                                    </div>
                                </div>
                            @endif

                            <div class="col-sm-12">
                                <input type="submit" name="submit" value="Send Message" class="btn btn-send">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>