    <section id="subscribe">
        <div class="container">
            <div class="row">
                <div class="subscribe-overlay"></div>
                <div class="col-md-8 col-md-offset-2 col-sm-12">
                    <div class="st-subscribe">

                        <div class="item active text-center">
                        @if(Session::has('successfull_subscription'))
                            <div class="alert alert-success text-center">
                              {{ Session::get('successfull_subscription') }}
                            </div>
                        @endif
                        <h2>Subscribe</h2>
                            <p>
                                "Keep up on our always evolving services and upcoming events. Enter your e-mail and subscribe to our newsletter."
                            </p>
                            <div class="st-border"></div>
                            <div class="client-info">
                                <form action="{{'send-email'}}" class="subscribe-form" name="subscribe-form" method="post">
                                    <input type="hidden" value="{{ csrf_token() }}" name="_token">
                                    <div class="input-group">
                                        <input type="text" name="sEmail" value="{{ old('sEmail') }}" placeholder="Email*" class="form-control" aria-label="glyphicon glyphicon-flag">
                                            <div class="input-group-btn">
                                                <input type="submit" name="submit" value="Send Email" class="btn subscribe-btn-send">
                                            </div>
                                    </div>
                                    {{--<div class="form-group">
                                        {!! Recaptcha::render() !!}
                                        @if ($errors->has('g-recaptcha-response'))
                                            <span class="text text-danger">{{ $errors->first('g-recaptcha-response') }}</span>
                                        @endif
                                    </div>--}}
                                    @if ($errors->has('sEmail'))
                                        <div class="alert alert-danger" role="alert">
                                            <strong>Oh snap!</strong>{{ $errors->first('sEmail') }}
                                        </div>
                                    @endif
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
