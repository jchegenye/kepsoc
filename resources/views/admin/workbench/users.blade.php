

    @include('admin.workbench.header')
  
    @include('admin.workbench.sidebar')

<!--main content start-->
    <section id="main-content">
        <section class="wrapper">

            <!-- <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><i class="fa fa-list-alt"></i> Components</h3>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="index.html">Home</a></li>
                        <li><i class="fa fa-desktop"></i>UI Fitures</li>
                        <li><i class="fa fa-list-alt"></i>Components</li>
                    </ol>
                </div>
            </div> -->

        <section class="panel">
            <header class="panel-heading tab-bg-users ">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#home">All Users</a>
                    </li>
                    <li class="">
                        <a data-toggle="tab" href="#about">#</a>
                    </li>
                </ul>
            </header>
            <div class="panel-body">
                <div class="tab-content">
                    <div id="home" class="tab-pane active">
                          @include('admin.workbench.data.user-list')
                    </div>
                    <div id="about" class="tab-pane">#</div>
                </div>
            </div>
        </section> 

    </section>
</section>
    

</body>
    
<!--####################################### start of SCRIPTS #######################################-->
@include('layouts.partials.scripts-workbench')
<!--####################################### end of SCRIPTS #########################################-->

</html>