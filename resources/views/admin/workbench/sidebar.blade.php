<!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu">                
                  <li class="active">
                      <a class="" href="{{'/workbench/home'}}">
                          <i class="fa fa-bar-chart"></i>
                          <span>Statistics</span>
                      </a>
                  </li>
                  <li class="active">
                      <a class="" href="{{'/workbench/users'}}">
                          <i class="fa fa-users"></i>
                          <span>Users</span>
                      </a>
                  </li>
                  <li class="active">
                      <a class="" href="{{'/workbench/members'}}">
                          <i class="fa fa-user-plus"></i>
                          <span>Members</span>
                      </a>
                  </li>
                  <li class="active">
                      <a class="" href="{{'/workbench/subscribers'}}">
                          <i class="fa fa-paper-plane"></i>
                          <span>Subscribers</span>
                      </a>
                  </li>
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->