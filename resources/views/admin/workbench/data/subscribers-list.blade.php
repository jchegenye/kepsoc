@if(Session::has('confirm'))
    <div class="alert alert-warning text-center">
        <i class="fa fa-spinner"></i>
        {{Session::get('confirm')}}
    </div>
@endif

          <header class="panel-heading">
              A total of  <span class="badge badge-info">{!! $subscriptions->total() !!} </span>subscription(s)
              <!--  search form start -->
              <div class="pull-right">
                  <form class="navbar-form">
                      <input class="form-control" name="Search_user" placeholder="Search" type="text">
                  </form>
              </div>
              <!--  search form end -->
          </header>
          
          <table class="table table-striped table-advance table-hover">
           <tbody>
              <tr>
                <th>#</th>
                <th><i class="fa fa-envelope"></i> Email</th>
                <th><i class="fa fa-calendar"></i> Date Subscribed</th>
                <th><i class="icon_cogs"></i> Action</th>
              </tr>
              @foreach($subscriptions as $index => $subscription)
                  <tr>
                    <td>{{$index +1}}</td>
                    <td>
                        @if($subscription->confirmation_code == 0)
                            <span data-placement="left" data-toggle="tooltip" class="badge
                            badge-success tooltips" data-original-title="The owner has subscribed. He/She is receiving NEWSLETTERS!">.</span>
                        @else
                            <span data-placement="left" data-toggle="tooltip" class="badge badge-danger tooltips" data-original-title="The owner has Un-subscribed and therefore, he/she will NOT receive NEWSLETTERS!">.</span>
                        @endif
                        {{$subscription->subscribed_email}}
                    </td>
                    <td>{{$subscription->subscribed_date}}</td>
                    <td>
                      <div class="btn-group">
                          @if($subscription->lock or 'no such field - lock')
                              <a href="{!! url('/lock/subscription/' . $subscription->_id) !!}" data-original-title="Unlock Email" data-content="Use this button to unlock this email!" data-placement="bottom" data-trigger="hover" class="btn btn-success popovers">
                                  <i class="fa fa-unlock"></i>
                              </a>
                          @else
                              @if($subscription->lock == 2)
                                <a href="{!! url('/lock/subscription/' . $subscription->_id) !!}" data-original-title="Unlock Email" data-content="Use this button to unlock this email!" data-placement="bottom" data-trigger="hover" class="btn btn-success popovers">
                                  <i class="fa fa-unlock"></i>
                                </a>
                              @elseif($subscription->lock == 3)
                                  <a href="{!! url('/lock/subscription/' . $subscription->_id) !!}" data-original-title="Lock Email" data-content="Use this button to lock this email!" data-placement="bottom" data-trigger="hover" class="btn btn-danger popovers">
                                      <i class="fa fa-lock"></i>
                                  </a>
                              @endif
                          @endif
                        <a href="{!! url('/trash/subscription/' . $subscription->_id) !!}"
                           data-original-title="Trash Email" data-content="Use this button to trash/delete this email completely!" data-placement="bottom" data-trigger="hover" class="btn btn-info popovers">
                          <i class="fa fa-trash"></i>
                        </a>

                        <a href="{!! url('/edit/subscription/' . $subscription->_id) !!}" data-original-title="Edit Email" data-content="Use this button to edit this email!" data-placement="bottom" data-trigger="hover" class="btn btn-info popovers">
                          <i class="fa fa-pencil-square-o"></i>
                        </a>

                      </div>
                    </td>
                  </tr>
              @endforeach
           </tbody>
              <tr>
                  <td colspan="5" >
                      Showing from {!! $subscriptions->firstItem() !!} to {!! $subscriptions->lastItem() !!} of {!! $subscriptions->total() !!} entries
                  </td>
                  <td colspan="2" class="text-right">{!! $subscriptions->links() !!}</td>
              </tr>
        </table>