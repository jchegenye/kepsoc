
          <header class="panel-heading">
              A total of  <span class="badge badge-info">{!! $member->total() !!}</span> member(s)
          </header>
          
          <table class="table table-striped table-advance table-hover">
           <tbody>
              <tr>
                <th>#</th>
                <th><i class="fa fa-user"></i> Full Name</th>
                <th><i class="fa fa-calendar"></i> Date Joined</th>
                  <th><i class="fa fa-bitbucket"></i> Occupation</th>
                  <th><i class="fa fa-graduation-cap"></i> Course</th>
                  <th><i class="fa fa-phone"></i> Phone Number</th>
                {{--<th><i class="fa fa-thumb-tack"></i> Role</th>--}}
                <th><i class="icon_cogs"></i> Action</th>
              </tr>
              @foreach($member as $key => $members)
                  @foreach($profile as  $profiles)

                      @if($profiles->uid == $members->uid)
                          <tr>
                            <td>{{$key +1 }}</td>
                              <td>
                                <span data-placement="left" data-toggle="tooltip" class="badge badge-success tooltips" data-original-title="User CAN access the members Dashboard">.</span>
                                @if(empty($members->profile_pic_file_name))
                                    <img class="img-circle" src="../images/members/sample.jpg" alt="">
                                @else
                                    <img class="img-circle" src="{{$members->profile_pic->url()}}"
                                         alt="">
                                @endif
                                  <a href="{{url('other/members/profile/' . $profiles->uid)}}">{{str_limit($profiles->name ,25)}}</a>
                              </td>
                              <td>{{$profiles->signed_date}}</td>
                              <td>{{str_limit($members->position ,35)}}</td>
                              <td>{{str_limit($members->course ,35)}}</td>
                              <td>{{str_limit($members->phone_number ,35)}}</td>
                              <td>
                                  <div class="btn-group">
                                    <a href="{!! url('/email/member/' . $profiles->uid) !!}" data-original-title="Email" data-content="Use this button to communicate to this user!" data-placement="bottom" data-trigger="hover" class="btn btn-info popovers">
                                      <i class="fa fa-envelope"></i>
                                    </a>
                                  </div>
                              </td>
                          </tr>
                      @endif

                  @endforeach
              @endforeach
           </tbody>
              <tr>
                  <td colspan="5" >
                      Showing from {!! $member->firstItem() !!} to {!! $member->lastItem() !!} of {!! $member->total() !!} entries
                  </td>
                  <td colspan="2" class="text-right">{!! $member->links() !!}</td>
              </tr>
        </table>
      