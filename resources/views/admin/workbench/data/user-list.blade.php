
          <header class="panel-heading">
              A total of  <span class="badge badge-info">{!! $users->total() !!}</span> user(s)
              <!--  search form start -->
              <div class="pull-right">
                  <form class="navbar-form">
                      <input class="form-control" name="Search_user" placeholder="Search" type="text">
                  </form>
              </div>
              <!--  search form end -->
          </header>

          @include('layouts.partials.messages')

      <div class="table-responsive">
          <table class="table table-striped table-advance table-hover">
           <tbody>
              <tr>
                <th>#</th>
                <th><i class="fa fa-user"></i> Full Name</th>
                <th><i class="fa fa-envelope"></i> Email</th>
                <th><i class="fa fa-calendar"></i>Date Joined</th>
                <th><i class="fa fa-thumb-tack"></i> Role</th>
                <th><i class="fa fa-calendar"></i> Date Confirmed</th>
                <th><i class="icon_cogs"></i> Action</th>
              </tr>

                @foreach($users as $index => $user)

                    @if($user->deleted_at)
                      <tr style="opacity: 0.3; z-index:000;">

                        <td>{{$index +1}}</td>
                        <td>
                            @if($user->confirmation_code == 1)
                                <span data-placement="left" data-toggle="tooltip" class="badge
                                badge-success tooltips" data-original-title="Account VERIFIED."> .</span>
                            @elseif($user->confirmation_code == 0)
                                <span data-placement="left" data-toggle="tooltip" class="badge
                                badge-danger tooltips" data-original-title="Account NOT verified.">.</span>
                            @endif
                                {{$user->name}}
                        </td>
                        <td>{{str_limit($user->email, 7)}} </td>
                        <td>{{$user->signed_date}}</td>
                        <td>
                            @foreach($user->role as $roles)
                                <p class="text text-danger">{{ ucwords(str_replace("_", " ", $roles)) }}<br>
                            @endforeach
                        </td>
                        <td>{{$user->confirmed_date}}</td>
                        <td>
                          <div class="btn-group">
                              @if(empty($user->user_status))
                                  <a href="{!! url('/user/status/' . $user->uid) !!}"
                                     data-original-title="Approve As A Member" data-content="Use this button to approve this person as a member!" data-placement="bottom" data-trigger="hover" class="btn btn-danger popovers">
                                      <i class="fa fa-xing"></i>
                                  </a>
                              @elseif($user->user_status == 'member')
                                  <a href="{!! url('/user/status/' . $user->uid) !!}"
                                     data-original-title="Decline As A Member" data-content="Use this button to decline this person as a member!" data-placement="bottom" data-trigger="hover" class="btn btn-success popovers">
                                      <i class="fa fa-xing"></i>
                                  </a>
                              @elseif($user->user_status == 'non_member')
                                  <a href="{!! url('/user/status/' . $user->uid) !!}"
                                     data-original-title="Approve As A Member" data-content="Use this button to approve this person as a member!" data-placement="bottom" data-trigger="hover" class="btn btn-danger popovers">
                                      <i class="fa fa-xing"></i>
                                  </a>
                              @endif

                              @if(empty($user->user_access))
                                <a href="{!! url('/lock/user/' . $user->uid) !!}" data-original-title="Lock User" data-content="Use this button to lock this user from logging in for sometime!" data-placement="bottom" data-trigger="hover" class="btn btn-warning popovers">
                                    <i class="fa fa-unlock"></i>
                                </a>
                              @elseif($user->user_access == 'locked')
                                  <a href="{!! url('/lock/user/' . $user->uid) !!}" data-original-title="Lock User" data-content="Use this button to lock this user from logging in for sometime!" data-placement="bottom" data-trigger="hover" class="btn btn-danger popovers">
                                      <i class="fa fa-lock"></i>
                                  </a>
                              @elseif($user->user_access == 'unlocked')
                                  <a href="{!! url('/lock/user/' . $user->uid) !!}" data-original-title="Unlock User" data-content="Use this button to unlock/allow this user to login!" data-placement="bottom" data-trigger="hover" class="btn btn-warning popovers">
                                      <i class="fa fa-unlock"></i>
                                  </a>
                              @endif

                              @foreach(Auth::user()->role as $individual_role)
                                  @if($individual_role == 'can_give_permissions')
                                      <a href="{!! url('give/permission/to/' . $user->uid) !!}" data-original-title="Permissions" data-content="Use this button to assign permissions to this user!" data-placement="bottom" data-trigger="hover" class="btn btn-info popovers">
                                          <i class="fa fa-key"></i>
                                      </a>
                                  @endif
                              @endforeach

                            <a href="{!! url('/trash/user/' . $user->uid) !!}"
                               data-original-title="Trash" data-content="Use this button to trash/delete this user completely!" data-placement="bottom" data-trigger="hover" class="btn btn-default popovers">
                              <i class="fa fa-trash"></i>
                            </a>

                            <a href="{!! url('/edit/user/' . $user->uid) !!}" data-original-title="Edit" data-content="Use this button to edit this user!" data-placement="bottom" data-trigger="hover" class="btn btn-inverse popovers">
                              <i class="fa fa-pencil-square-o"></i>
                            </a>

                            <a href="{!! url('/email/user/' . $user->uid) !!}" data-original-title="Email" data-content="Use this button to communicate to this user!" data-placement="bottom" data-trigger="hover" class="btn btn-info popovers">
                              <i class="fa fa-envelope"></i>
                            </a>
                          </div>
                        </td>


                      </tr>
                  @else
                        <tr>

                            <td>{{$index +1}}</td>
                            <td>
                                @if($user->confirmation_code == 1)
                                    <span data-placement="left" data-toggle="tooltip" class="badge
                            badge-success tooltips" data-original-title="Account VERIFIED."> .</span>
                                @elseif($user->confirmation_code == 0)
                                    <span data-placement="left" data-toggle="tooltip" class="badge
                            badge-danger tooltips" data-original-title="Account NOT verified.">.</span>
                                @endif
                                {{$user->name}}
                            </td>
                            <td>{{str_limit($user->email, 7)}} </td>
                            <td>{{$user->signed_date}}</td>
                            <td>
                                @foreach($user->role as $roles)
                                    <p class="text text-danger">{{ ucwords(str_replace("_", " ", $roles)) }}<br>
                                @endforeach
                            </td>
                            <td>{{$user->confirmed_date}}</td>
                            <td>
                                <div class="btn-group">
                                    @if(empty($user->user_status))
                                        <a href="{!! url('/user/status/' . $user->uid) !!}"
                                           data-original-title="Approve As A Member" data-content="Use this button to approve this person as a member!" data-placement="bottom" data-trigger="hover" class="btn btn-danger popovers">
                                            <i class="fa fa-xing"></i>
                                        </a>
                                    @elseif($user->user_status == 'member')
                                        <a href="{!! url('/user/status/' . $user->uid) !!}"
                                           data-original-title="Decline As A Member" data-content="Use this button to decline this person as a member!" data-placement="bottom" data-trigger="hover" class="btn btn-success popovers">
                                            <i class="fa fa-xing"></i>
                                        </a>
                                    @elseif($user->user_status == 'non_member')
                                        <a href="{!! url('/user/status/' . $user->uid) !!}"
                                           data-original-title="Approve As A Member" data-content="Use this button to approve this person as a member!" data-placement="bottom" data-trigger="hover" class="btn btn-danger popovers">
                                            <i class="fa fa-xing"></i>
                                        </a>
                                    @endif

                                    @if(empty($user->user_access))
                                        <a href="{!! url('/lock/user/' . $user->uid) !!}" data-original-title="Lock User" data-content="Use this button to lock this user from logging in for sometime!" data-placement="bottom" data-trigger="hover" class="btn btn-warning popovers">
                                            <i class="fa fa-unlock"></i>
                                        </a>
                                    @elseif($user->user_access == 'locked')
                                        <a href="{!! url('/lock/user/' . $user->uid) !!}" data-original-title="Lock User" data-content="Use this button to lock this user from logging in for sometime!" data-placement="bottom" data-trigger="hover" class="btn btn-danger popovers">
                                            <i class="fa fa-lock"></i>
                                        </a>
                                    @elseif($user->user_access == 'unlocked')
                                        <a href="{!! url('/lock/user/' . $user->uid) !!}" data-original-title="Unlock User" data-content="Use this button to unlock/allow this user to login!" data-placement="bottom" data-trigger="hover" class="btn btn-warning popovers">
                                            <i class="fa fa-unlock"></i>
                                        </a>
                                    @endif

                                    @foreach(Auth::user()->role as $individual_role)
                                        @if($individual_role == 'can_give_permissions')
                                            <a href="{!! url('give/permission/to/' . $user->uid) !!}" data-original-title="Permissions" data-content="Use this button to assign permissions to this user!" data-placement="bottom" data-trigger="hover" class="btn btn-info popovers">
                                                <i class="fa fa-key"></i>
                                            </a>
                                        @endif
                                    @endforeach

                                    <a href="{!! url('/trash/user/' . $user->uid) !!}"
                                       data-original-title="Trash" data-content="Use this button to trash/delete this user completely!" data-placement="bottom" data-trigger="hover" class="btn btn-default popovers">
                                        <i class="fa fa-trash"></i>
                                    </a>

                                    <a href="{!! url('/edit/user/' . $user->uid) !!}" data-original-title="Edit" data-content="Use this button to edit this user!" data-placement="bottom" data-trigger="hover" class="btn btn-inverse popovers">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </a>

                                    <a href="{!! url('/email/user/' . $user->uid) !!}" data-original-title="Email" data-content="Use this button to communicate to this user!" data-placement="bottom" data-trigger="hover" class="btn btn-info popovers">
                                        <i class="fa fa-envelope"></i>
                                    </a>
                                </div>
                            </td>


                        </tr>
                  @endif

                @endforeach
              <tr>
                  <td colspan="5" >
                      Showing from {!! $users->firstItem() !!} to {!! $users->lastItem() !!} of {!! $users->total() !!} entries
                  </td>
                  <td colspan="2" class="text-right">{!! $users->links() !!}</td>
              </tr>
           </tbody>
        </table>
      </div>


      