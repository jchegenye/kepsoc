
<!-- CKEditor -->
<section class="panel">
    <header class="panel-heading">
        Article Form
    </header>
    <div class="panel-body">
        <form action="{{'#'}}" method="post" class="form-horizontal" role="form">
            <input type="text" name="newsletter_author" value="jack">
            <div class="form-group">
                <label for="inputTitle" class="control-label">Title/Topic</label>
                <div class="col-lg-10">
                    <input type="text" name="news_title" class="form-control" id="inputTitle" placeholder="Title">
                </div>
            </div>
            <div class="form-group">
                <label for="inputFile" class="control-label">File Input/Image</label>
                <div class="col-sm-10">
                  <input type="file" name="news_file" class="form-control"  id="inputFile">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">Message Body</label>
                <div class="col-sm-10">
                    <textarea class="form-control ckeditor" name="news_body" rows="6"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">Tags</label>
                <div class="col-sm-10">
                    <input name="tagsinput" id="tagsinput" class="tagsinput" value="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-10">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</section>