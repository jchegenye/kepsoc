
          <header class="panel-heading">
              List of all newsletters
          </header>
          
          <table class="table table-striped table-advance table-hover">
           <tbody>
              <tr>
                <th>#</th>
                <th><i class="fa fa-user"></i> Title/Topic</th>
                <th><i class="fa fa-thumb-tack"></i> Body</th>
                <th><i class="fa fa-file"></i> File Uploaded</th>
                <th><i class="fa fa-tags"></i> Tags</th>
                <th><i class="fa fa-calendar"></i> Date Posted</th>
                <th><i class="fa fa-user"></i> Author</th>    
                <th><i class="fa fa-calendar"></i> Date Delivered</th>
                <th><i class="fa fa-cogs"></i> Action</th>
              </tr>
              <tr>
                <td>1.</td>
                <td>
                <span data-placement="left" data-toggle="tooltip" class="badge badge-success tooltips" data-original-title="New Article"> .</span>
                Why join kepsoc?</td>
                <td>Kepsoc is a kenyan organisation ...</td>
                <td>picture/photo</td>
                <td>Crime, Criminology, CID</td>
                <td>2016-12-26</td>
                <td>Jack</td>
                <td>2016-12-26</td>
                <td>
                  <div class="btn-group">
                    <a href="#" data-original-title="Unlock Article" data-content="Use this button to unlock this article!" data-placement="bottom" data-trigger="hover" class="btn btn-success popovers">
                      <i class="fa fa-unlock"></i>
                    </a>
                    <a href="#" data-original-title="Trash Article" data-content="Use this button to trash/delete this article completely!" data-placement="bottom" data-trigger="hover" class="btn btn-info popovers">
                      <i class="fa fa-trash"></i>
                    </a>
                    <a href="#" data-original-title="Edit Article" data-content="Use this button to edit this article!" data-placement="bottom" data-trigger="hover" class="btn btn-info popovers">
                      <i class="fa fa-pencil-square-o"></i>
                    </a>
                    <a href="#" data-original-title="Email Article" data-content="Use this button to broadcast to subscribers, this article as a newsletter!" data-placement="bottom" data-trigger="hover" class="btn btn-info popovers">
                      <i class="fa fa-share"></i>
                    </a>
                  </div>
                </td>
              </tr>
              <tr>
                <td>2.</td>
                 <td>
                <span data-placement="left" data-toggle="tooltip" class="badge badge-danger tooltips" data-original-title="Old Article"> .</span>
                Why join kepsoc?</td>
                <td>Kepsoc is a kenyan organisation ...</td>
                <td>picture/photo</td>
                <td>Crime, Criminology, CID</td>
                <td>2016-12-26</td>
                <td>Jack</td>
                <td>2016-12-26</td>
                <td>
                  <div class="btn-group">      
                    <a href="#" data-original-title="Lock Article" data-content="Use this button to lock this article!" data-placement="bottom" data-trigger="hover" class="btn btn-danger popovers">
                      <i class="fa fa-lock"></i>
                    </a>
                    <a href="#" data-original-title="Trash Article" data-content="Use this button to trash/delete this article completely!" data-placement="bottom" data-trigger="hover" class="btn btn-info popovers">
                      <i class="fa fa-trash"></i>
                    </a>
                    <a href="#" data-original-title="Edit Article" data-content="Use this button to edit this article!" data-placement="bottom" data-trigger="hover" class="btn btn-info popovers">
                      <i class="fa fa-pencil-square-o"></i>
                    </a>
                    <a href="#" data-original-title="Email Article" data-content="Use this button to broadcast to subscribers, this article as a newsletter!" data-placement="bottom" data-trigger="hover" class="btn btn-info popovers">
                      <i class="fa fa-share"></i>
                    </a>
                  </div>
                </td>
              </tr>                        
           </tbody>
        </table>
      