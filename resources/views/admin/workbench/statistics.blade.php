

    @include('admin.workbench.header')
  
    @include('admin.workbench.sidebar')

<!--main content start-->
    <section id="main-content">
        <section class="wrapper">

            <div class="row">

                <div class="col-md-4 ">
                    <div class="social-box ">
                        <i class="fa fa-users bg-users">
                            <span>{{$all_users->count()}}</span>
                            <span>User's</span>
                        </i>
                        <ul>
                            <li>
                                <strong>
                                    {{$verified->count()}}
                                </strong>
                                <span class="yes">verified</span>
                            </li>
                            <li>
                                <strong>
                                    {{$unverified->count()}}
                                </strong>
                                <span class="no">un-verified</span>
                            </li>
                        </ul>
                    </div><!--/social-box-->
                </div>
                <div class="col-md-4 ">
                    <div class="social-box ">
                        <i class="fa fa-user-plus bg-members">
                            <span>
                                {{$all_member->count()}}
                            </span>
                            <span>Members</span>
                        </i>
                        <ul>
                            <li>
                                <strong>0</strong>
                                <span class="yes">new</span>
                            </li>
                            <li>
                                <strong>0</strong>
                                <span class="no">old</span>
                            </li>
                        </ul>
                    </div><!--/social-box-->
                </div>
                <div class="col-md-4 ">
                    <div class="social-box ">
                        <i class="fa fa-paper-plane bg-subscribers">
                            <span>{{$all_subscriber->count()}}</span>
                            <span>Subscriber's</span>
                        </i>
                        <ul>
                            <li>
                                <strong>{{$verifiedSubscriber->count()}}</strong>
                                <span class="yes">subscribed</span>
                            </li>
                            <li>
                                <strong>{{$unverifiedSubscriber->count()}}</strong>
                                <span class="no">un-subscribed</span>
                            </li>
                        </ul>
                    </div><!--/social-box-->
                </div>

            </div>
            <!-- Pie -->
                <div class="col-lg-6">
                  <section class="panel">
                      <header class="panel-heading">
                          Pie
                      </header>
                      <div class="panel-body text-center">
                          <canvas id="pie" height="300" width="400"></canvas>
                      </div>
                  </section>
                </div>

        </section>
    </section>
    

</body>
    
<!--####################################### start of SCRIPTS #######################################-->
@include('layouts.partials.scripts-workbench')
<!--####################################### end of SCRIPTS #########################################-->

</html>