<!--####################################### start of CSS #######################################-->
@include('layouts.partials.css')
<!--####################################### end of css #########################################-->

<div class="container">
    <div class="col-md-12">

        <h2>User Roles</h2>
        <p>A list of user roles & permissions</p>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Created on</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
                @foreach($roles as $role)
                    <tr>
                        <td>{{$role->name}}</td>
                        <td>{{$role->created_at}}</td>
                        <td><a href="{{url('role/permissions/' . $role->id) }}">View Permission</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>

<!--####################################### start of SCRIPTS #######################################-->
@include('layouts..partials.scripts')
<!--####################################### end of SCRIPTS #########################################-->