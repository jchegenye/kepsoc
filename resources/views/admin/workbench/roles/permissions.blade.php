

<!--####################################### start of CSS #######################################-->
@include('layouts.partials.css')
<!--####################################### end of css #########################################-->

<div class="container">
    <div class="col-md-12">

        <h2>Roles Permission</h2>
        <p>A list of roles & its permissions</p>

        <div class="pull-left"><i>{{$role->name}}</i> Permissions</div>
        <a class="pull-right" href="/role/permissions/{{$role->id}}/edit">Edit Permissions</a>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
            </tr>
            </thead>
            <tbody>
                @if(empty($role->permission_name))
                    @else
                        @foreach ($role->permission_name as $key=> $roles)
                            <tr>
                                <td>{{$roles}} | {{$key}}</td>
                            </tr>
                        @endforeach
                @endif
            </tbody>
        </table>

    </div>
</div>

<!--####################################### start of SCRIPTS #######################################-->
@include('layouts..partials.scripts')
<!--####################################### end of SCRIPTS #########################################-->