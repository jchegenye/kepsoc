<!--####################################### start of CSS #######################################-->
@include('layouts.partials.css')
<!--####################################### end of css #########################################-->

<div class="container">
    <div class="col-md-12">
        <div class="row">
            @if(Session::has('successful'))
                <div class="alert alert-success text-center">
                    <div class="alert-all-text">{{ Session::get('successful') }}</div>
                </div>
            @endif
            @if(Session::has('unsuccessful'))
                <div class="alert alert-danger text-center">
                    <div class="alert-all-text">{{ Session::get('unsuccessful') }}</div>
                </div>
            @endif

            <form class="" method="post" action="{{url('save/role')}}">
                <span>Role Name</span>
                <div class="form-group">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="text" name="role" placeholder="Role*" value="{{ old('role') }}">
                    @if ($errors->has('role'))
                        <span class="text text-danger">{{ $errors->first('role') }}</span>
                    @endif
                </div>

                <button id="btn-login" class="btn btn-lg btn-danger" type="submit">
                    Save
                </button>
            </form>
        </div>
    </div>
</div>

<!--####################################### start of SCRIPTS #######################################-->
@include('layouts..partials.scripts')
<!--####################################### end of SCRIPTS #########################################-->