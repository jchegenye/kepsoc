<!--####################################### start of CSS #######################################-->
@include('layouts.partials.css')
<!--####################################### end of css #########################################-->

<div class="container">
    <div class="col.md-12">
        {{ Form::model($role, array('route' => array('role_update', $role->id), 'method' => 'PUT')) }}
        <table class="table table-striped">
            <thead>
                <tr><th>#</th><th>Name</th></tr>
            </thead>
            @foreach($permissions as $permission)
                <tbody>
                    <tr><td><input type="checkbox" name="permission_name[{{$permission->machine_name}}]" value="{{$permission->name}} - {{$permission->description}}"></td>
                        <td>{{$permission->name}}</td>
                        <td>{{$permission->description}}</td>
                    </tr>
                </tbody>
            @endforeach
        </table>
        {{ Form::submit('Update Permissions', array('class' => 'btn btn-primary')) }}
        {{ Form::close() }}

    </div>
</div>

<!--####################################### start of SCRIPTS #######################################-->
@include('layouts..partials.scripts')
<!--####################################### end of SCRIPTS #########################################-->