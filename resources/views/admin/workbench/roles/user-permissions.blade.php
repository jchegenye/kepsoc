<!--####################################### start of CSS #######################################-->
@include('layouts.partials.css')
        <!--####################################### end of css #########################################-->

<div class="container">
    <div class="col.md-12">

        {{ Form::model($user, array('route' => array('user_permissions_update', $user->uid), 'method' => 'PUT')) }}
        <table class="table table-striped">
            <thead>
            <tr><th>#</th><th>Name</th><th>Description</th></tr>
            </thead>
            @foreach($permissions as $permission)
                <tbody>
                <tr><td><input type="checkbox" name="permission[]" value="{{$permission->machine_name or $user->role}}"></td>
                    <td>{{$permission->name}}</td>
                    <td>{{$permission->description}}</td>
                </tr>
                </tbody>
            @endforeach
        </table>
        {{ Form::submit('Update Permissions', array('class' => 'btn btn-primary')) }}
        {{ Form::close() }}

        Give permissions to:<br>
            - {{$user->name}}<br>{{--{{$userPermission->name}}--}}
        @foreach($userPermission as $userPermissions)
            {{$userPermissions->name}} <br>
        @endforeach
    </div>
</div>

<!--####################################### start of SCRIPTS #######################################-->
@include('layouts..partials.scripts')
        <!--####################################### end of SCRIPTS #########################################-->