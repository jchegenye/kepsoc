<!DOCTYPE html>
<html lang="en">
<head>

    <!--##################################### start of HEAD ########################################-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf_token" content="{{ csrf_token() }}">

    <title>Kenya Professional Society Of Criminology | Admins Dashboard</title>

    <meta name="copyright" content="j-tech is a registered trade mark Co. based in Kenya">
    <meta name="author" content="Chegenye Asumu Jackson">
    <meta name="reply-to" content="chegenyejackson@gmail.com">
    <meta name="web_author" content="https://j-tech.tech">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <!-- Favicon -->
    <link rel="icon" href="{{ asset('http://kepsoc.org/favicon.ico') }}">
    <!--####################################### end of head ########################################-->

    <!--####################################### start of CSS #######################################-->
    @include('layouts.partials.css-workbench')
    <!--####################################### end of css #########################################-->

</head>

<body>

    <header class="header dark-bg">

        <div class="toggle-nav">
            <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
        </div>

       <!--logo start-->
    <a href="{{'/workbench/home'}}" class="logo">
        <!-- <img src="/logo.png" style="width:10%;"> --> <span class="lite">DashBoard</span>
    </a>
       <!--logo end-->

    <!--  search form start 
       <div class="nav search-row" id="top_menu">
           <ul class="nav top-menu">                    
               <li>
                   <form class="navbar-form">
                       <input class="form-control" placeholder="Search" type="text">
                   </form>
               </li>                    
           </ul>               
       </div>
    -->

    <div class="top-nav notification-row">                
          <!-- notificatoin dropdown start-->
        <ul class="nav pull-right top-menu">
                        
    <!-- inbox notificatoin start-->
        <li id="mail_notificatoin_bar" class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <i class="fa fa-envelope"></i>
                <span class="badge bg-important">1</span>
            </a>
            <ul class="dropdown-menu extended inbox">
                <div class="notify-arrow notify-arrow-blue"></div>
                <li>
                    <p class="blue">You have 1 new messages</p>
                </li>
                <li>
                    <a href="#">
                        <span class="photo"><img alt="avatar" src="../images/avatar1_small.jpg"></span>
                        <span class="subject">
                        <span class="from">Jack Kimani</span>
                        <span class="time">1 min</span>
                        </span>
                        <span class="message">
                            I would like to join kepsoc, how ....
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#">See all messages</a>
                </li>
            </ul>
        </li>
        <!-- inbox notificatoin end -->

        <!-- alert notification start-->
        <li id="alert_notificatoin_bar" class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">

                <i class="fa fa-bell"></i>
                <span class="badge bg-important">4</span>
            </a>
            <ul class="dropdown-menu extended notification">
                <div class="notify-arrow notify-arrow-blue"></div>
                <li>
                    <p class="blue">You have 4 new notifications</p>
                </li>
                <li>
                    <a href="#">
                        <span class="label label-danger"><i class="fa fa-envelope-o"></i></span>
                        contact us message(s)
                        <span class="small italic pull-right">1 hr</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="label label-primary"><i class="fa fa-users"></i></span>
                        New user's joined us
                        <span class="small italic pull-right">5 mins</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="label label-warning"><i class="fa fa-paper-plane"></i></span>
                        New member's on board
                        <span class="small italic pull-right">50 mins</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="label label-success"><i class="fa fa-user-plus"></i></span>
                        Mick appreciated your work.
                        <span class="small italic pull-right"> Today</span>
                    </a>
                </li>                            
                <li>
                    <a href="#">See all notifications</a>
                </li>
            </ul>
        </li>
        <!-- alert notification end-->


        <!-- user login dropdown start-->
        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <span class="profile-ava">
                    <img alt="" src="../images/avatar1_small.jpg">
                </span>
                <span class="username">{!! Auth::user()->name !!}</span>
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu extended logout">
                <div class="log-arrow-up"></div>
                <li class="eborder-top">
                    <a href="{{url('members/profile/' .Auth::user()->uid)}}"><i class="icon_profile"></i> My Profile</a>
                </li>
                <li>
                    <a href="{{'#'}}"><i class="icon_mail_alt"></i> My Inbox</a>
                </li>
                <li>
                    <a href="{{'#'}}"><i class="icon_clock_alt"></i> Timeline</a>
                </li>
                <li>
                    <a href="{{'#'}}"><i class="icon_chat_alt"></i> Chats</a>
                </li>
                <li>
                    <a href="{{'/auth/logout'}}"><i class="fa fa-power-off"></i> Log Out</a>
                </li>
                <!-- <li>
                    <a href="{{'#'}}"><i class="icon_key_alt"></i> Documentation</a>
                </li> -->
                <li>
                    <a href="{{'#'}}"><i class="icon_key_alt"></i> Documentation</a>
                </li>
            </ul>
        </li>
        <!-- user login drop down end -->
    </ul>
    <!-- notification drop down end-->
                </div>
          </header>      
          <!--header end-->
