<!DOCTYPE html>
<html lang="en">
    <head>

        <!--##################################### start of HEAD ########################################-->
        @include('layouts.partials.meta-tags')
        <!--####################################### end of head ########################################-->

        <!--####################################### start of CSS #######################################-->
        @include('layouts.partials.css')
        <!--####################################### end of css #########################################-->

        <!--####################################### start of Google Analytics ##########################-->
        {!! Analytics::render() !!}
        <!--####################################### end of Google Analytics ############################-->
        
    </head>

    <body>
        @if(Session::has('unsuccessful'))
            <div class="alert alert-danger danger">
                <img src="../logo.png" style="position: absolute; left: 0px; height: 100%; top: 0px; opacity: 1.0; padding: 0px; margin: initial; background-color: #fff; margin-left: inherit;">
                <div class="alert-all-text">{{ Session::get('unsuccessful') }}</div>
            </div>
        @endif
        @if(Session::has('unauthorised'))
            <div class="alert alert-info info">
                <img src="../logo.png" style="position: absolute; left: 0px; height: 100%; top: 0px; opacity: 1.0; padding: 0px; margin: initial; background-color: #fff; margin-left: inherit;">
                <div class="alert-all-text">{{ Session::get('unauthorised') }}</div>
            </div>
        @endif

        <!-- ------------------------------------ start of PRELOADER ---------------------------------- -->
        @include('layouts.partials.preload')
        <!-- ------------------------------------ end of PRELOADER ------------------------------------ -->

        <!-- ------------------------------------- start of Header ------------------------------------ -->
        @include('layouts.partials.pages.header')
        <!-- ------------------------------------- end of Header -------------------------------------- -->

        <!-- ------------------------------------- start of HOME SLIDER ------------------------------- -->
        @include('layouts.pages.slider')
        <!-- ------------------------------------- end of HOME SLIDER --------------------------------- -->

        <!-- ------------------------------------- start of ABOUT ------------------------------------- -->
        @include('layouts.pages.about')
        <!-- ------------------------------------- end of ABOUT --------------------------------------- -->

        <!-- ------------------------------------- start of chairman ---------------------------------- -->
        @include('layouts.pages.chairman')
        <!-- ------------------------------------- end of chairman ------------------------------------ -->

        <!-- ------------------------------------- start of SERVICES ---------------------------------- -->
        @include('layouts.pages.services')
        <!-- ------------------------------------- end of SERVICES ------------------------------------ -->

        <!-- ------------------------------------- start of SERVICES ---------------------------------- -->
        @include('layouts.pages.core-values')
        <!-- ------------------------------------- end of SERVICES ------------------------------------ -->

        <!-- ------------------------------------- start of ABOUT ------------------------------------- -->
        @include('layouts.pages.our-team')
        <!-- ------------------------------------- end of ABOUT --------------------------------------- -->

        <!-- ------------------------------------- start of CONTACT-US -------------------------------- -->
        @include('layouts.pages.contact-us')
        <!-- ------------------------------------- end of CONTACT-US ---------------------------------- -->

        <!-- ------------------------------------- start of SUBSCRIBE --------------------------------- -->
        @include('layouts.pages.subscribe')
        <!-- ------------------------------------- end of SUBSCRIBE ----------------------------------- -->

        <!-- ------------------------------------- start of Footer ------------------------------------ -->
        @include('layouts.partials.pages.footer')
        <!-- ------------------------------------- end of Footer -------------------------------------- -->

        <!-- ------------------------------------ start of Scroll-up ---------------------------------- -->
        <div class="scroll-up">
            <ul><li><a href="#slider"><i class="fa fa-angle-up"></i></a></li></ul>
        </div>
        <!-- ------------------------------------- end of Scroll-up ----------------------------------- -->

        <!--####################################### start of SCRIPTS #######################################-->
        @include('layouts.partials.scripts')
        <!--####################################### end of SCRIPTS #########################################-->

    </body>

</html>
