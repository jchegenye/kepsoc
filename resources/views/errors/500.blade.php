<!DOCTYPE html>
<html>
<head>

    <title>KEPSOC - ERROR 500 | {{$segment = Request::segment(1)}}</title>

    @include('../layouts.partials.css')

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf_token" content="{{ csrf_token() }}">
    <meta name="copyright" content="j-tech is a registered trade mark Co. based in Kenya">
    <meta name="author" content="Chegenye Asumu Jackson">
    <meta name="reply-to" content="chegenyejackson@gmail.com">

    <style>
        html, body {
            height: 100%;
        }
        body {
            background-image: url(../images/body1.jpg);
            margin: 0;
            padding: 0;
            width: 100%;
            color: rgb(0, 81, 119);
            display: table;
            font-weight: 700;
            font-family: 'Lato';
        }
        .container {
            display: table-cell;
            vertical-align: middle;
        }
        .title-error-msg{
            font-size: 60px;
            /*line-height: 0.1em;*/
        }
        .subtitle-error-msg{
            font-size: 40px;
            /* line-height:1.0em;*/
            letter-spacing: 0.1em;
        }
        .description-error-msg{
            font-size: 20px;
            /*line-height:1.0em;*/
            letter-spacing: 0.2em;
        }
        .question-error-msg{
            font-size: 14px;
            padding-top: 10px;
        }
        .horizontal-line-error-msg{
            margin-top: -5px;
            margin-bottom: 20px;
            border: 0;
            border-top: 1px solid#B0BEC5;
            width: 36%;
        }
        .bottom-menu-error-msg{
            /*letter-spacing: 0.1em;*/
        }
        .menu{
            background-color: #fff;
            color: rgb(0, 81, 119);
            font-weight: 700;
        }
        .menu:hover{
            /*color:rgb(0, 81, 119);
            font-weight: 700;*/
        }
        .copyright-footer-error-msg{
            margin-top:15px;
            font-size: smaller;
        }

    </style>
</head>

{{--Internal Server Error--}}
<body>
<div class="container">

    {{-- Main error message STARTS here --}}
    <div class="col-md-7">
        <div class="row">
            <p class="title-error-msg">
                Oops, Don't panic!
            </p>
            <p class="subtitle-error-msg">
                Error 500 | Internal Server error
            </p>
            <p class="description-error-msg">
                The server encountered an internal error or misconfiguration and was unable to complete your request!
            </p>
        </div>
    </div>
    <div class="col-md-5">
        <div class="row text-left">
            <h5>Possible Reason's:</h5>
            <ul>
                <li>bad URL</li>
                <li>maybe some data entry on your part</li>
            </ul>
            <h5>Suggested Solution's:</h5>
            <ul>
                <li>clear your browser cache</li>
                <li>clear cookies</li>
                <li>refresh the page</li>
                <li>try a different browser</li>
                <br>
                <p>*If this problem persist, Contact the Administrator below.</p>
            </ul>
        </div>
    </div>
    {{-- Main error message ENDS here--}}

    {{-- Question, Menu and Search STARTS here--}}
    <div class="col-sm-12">
        <div class="col-sm-6">
            <div class="row">
                <p class="question-error-msg ">
                    Would you like to search something else?
                <div class="horizontal-line-error-msg"></div>
                </p>
                <!-- Search Google -->
                <form action="{{'http://www.google.com/'}}{{Input::get('search')}}" name="search-form" method="Put">
                    <div class="input-group " style="padding-right: 10px;">
                        <input type="text" class="form-control" placeholder="Search again..." name="search" value="{{$segment = Request::segment(1)}}">
                                <span class="input-group-btn">
                                    <input class="btn btn-default menu" type="submit">Search!</input>
                                </span>
                    </div>
                </form>
                <!-- Search Google -->
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <p class="question-error-msg ">
                    What would you like to do next?
                <div class="horizontal-line-error-msg"></div>
                </p>
                {{--Horizontal line--}}
                {{--<div class="horizontal-line-error-msg "></div>--}}

                <div class="bottom-menu-error-msg ">
                    <a href="http://kepsoc.dev" class="btn btn-default btn-md menu">
                        Go Back Home
                    </a>
                    <a href="http://kepsoc.dev/#contact" class="btn btn-default btn-md menu">
                        Report This Issue
                    </a>
                    <a href="#" class="btn btn-default btn-md menu">
                        Read Our FAQ
                    </a>
                    <a href="http://kepsoc.dev/#contact" class="btn btn-default btn-md menu">
                        Call Us Now
                    </a>
                </div>
            </div>
        </div>
    </div>
    {{-- Question, Menu and Search ENDS here--}}

    {{-- Copyright footer STARTS here--}}
    <div class="col-md-12 copyright-footer-error-msg">
        <div class="row text-center">
            <p>Designed & Developed by <a href="{{'http://j-tech.tech'}}">j-tech.tech</a> </p>
            <p>Copyright by <a href="{{'http://kepsoc.org'}}">kepsoc.org</a></p> {{date('Y')}}
        </div>
    </div>
    {{-- Copyright footer ENDS here--}}

</div>
</body>

</html>

