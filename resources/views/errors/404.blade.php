<!DOCTYPE html>
<html>
<head>

    <title>KEPSOC - ERROR 404 | {{$segment = Request::segment(1)}}</title>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf_token" content="{{ csrf_token() }}">
        <meta name="copyright" content="j-tech is a registered trade mark Co. based in Kenya">
        <meta name="author" content="Chegenye Asumu Jackson">
        <meta name="reply-to" content="chegenyejackson@gmail.com">

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <!-- Main CSS files -->
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../css/owl.carousel.css" />
        <link rel="stylesheet" href="../css/magnific-popup.css" />
        <link rel="stylesheet" href="../css/font-awesome.css" />
        <link rel="stylesheet" href="../css/style.css" />
        <link rel="stylesheet" href="../css/responsive.css" />

    <style>
        html, body {
            height: 100%;
        }
        body {
            background-image: url(../images/body1.jpg);
            margin: 0;
            padding: 0;
            width: 100%;
            color: rgb(0, 81, 119);
            display: table;
            font-weight: 700;
            font-family: 'Lato';
        }
        .container {
            display: table-cell;
            vertical-align: middle;
        }
        .title-error-msg{
            font-size: 60px;
            /*line-height: 0.1em;*/
        }
        .subtitle-error-msg{
            font-size: 40px;
           /* line-height:1.0em;*/
            letter-spacing: 0.1em;
        }
        .description-error-msg{
            font-size: 20px;
            /*line-height:1.0em;*/
            letter-spacing: 0.2em;
        }
        .question-error-msg{
            font-size: 14px;
            padding-top: 10px;
        }
        .horizontal-line-error-msg{
            margin-top: -5px;
            margin-bottom: 20px;
            border: 0;
            border-top: 1px solid#B0BEC5;
            width: 36%;
        }
        .bottom-menu-error-msg{
            /*letter-spacing: 0.1em;*/
        }
        .menu{
            background-color: #fff;
            color: rgb(0, 81, 119);
            font-weight: 700;
        }
        .menu:hover{
            /*color:rgb(0, 81, 119);
            font-weight: 700;*/
        }
        .copyright-footer-error-msg{
            margin-top:15px;
            font-size: smaller;
        }

    </style>
</head>

    <body>
        <div class="container">

           {{-- Main error message STARTS here --}}
            <div class="col-md-7">
                <div class="row">
                    <p class="title-error-msg">
                        Oops, Don't panic!
                    </p>
                    <p class="subtitle-error-msg">
                        Error 404 | Page not found
                    </p>
                    <p class="description-error-msg">
                        You stumbled across a PAGE that does NOT exist !
                    </p>
                </div>
            </div>
            <div class="col-md-5">
                <div class="row text-left">
                    <h5>Possible Reason's:</h5>
                    <ul>
                        <li>it could have been removed, updated, moved or deleted</li>
                        <li>it's temporarily unavailable</li>
                        <li>typo error's</li>
                    </ul>
                    <h5>Suggested Solution's:</h5>
                    <ul>
                        <li>review your URL and make sure that it is spelled correctly</li>
                        <br>
                        <p>*If this problem persist, Contact the Administrator below.</p>
                    </ul>
                </div>
            </div>
            {{-- Main error message ENDS here--}}

            {{-- Question, Menu and Search STARTS here--}}
            <div class="col-sm-12">
                <div class="col-sm-6">
                    <div class="row">
                        <p class="question-error-msg ">
                            Would you like to Search again?
                            <div class="horizontal-line-error-msg"></div>
                        </p>
                        <!-- Search Google -->
                        <form action="{{'http://www.google.com/'}}{{Input::get('search')}}" name="search-form" method="Put">
                            <div class="input-group " style="padding-right: 10px;">
                                <input type="text" class="form-control" placeholder="Search again..." name="search" value="{{$segment = Request::segment(1)}}">
                                <span class="input-group-btn">
                                    <input class="btn btn-default menu" type="submit">Search</input>
                                </span>
                            </div>
                        </form>
                        <!-- Search Google -->
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <p class="question-error-msg ">
                            What would you like to do next?
                            <div class="horizontal-line-error-msg"></div>
                        </p>
                        {{--Horizontal line--}}
                        {{--<div class="horizontal-line-error-msg "></div>--}}

                        <div class="bottom-menu-error-msg ">
                            <a href="http://kepsoc.dev" class="btn btn-default btn-md menu">
                                Go Back Home
                            </a>
                            <a href="http://kepsoc.dev/#contact" class="btn btn-default btn-md menu">
                                Report This Issue
                            </a>
                            <a href="#" class="btn btn-default btn-md menu">
                                Read Our FAQ
                            </a>
                            <a href="http://kepsoc.dev/#contact" class="btn btn-default btn-md menu">
                                Call Us Now
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Question, Menu and Search ENDS here--}}

            {{-- Copyright footer STARTS here--}}
            <div class="col-md-12 copyright-footer-error-msg">
                <div class="row text-center">
                    <p>Designed & Developed by <a href="{{'http://j-tech.tech'}}">j-tech.tech</a> </p>
                    <p>Copyright - <a href="{{'http://kepsoc.org'}}">kepsoc.org</a></p> {{date('Y')}}
                </div>
            </div>
            {{-- Copyright footer ENDS here--}}

        </div>
    </body>

</html>

