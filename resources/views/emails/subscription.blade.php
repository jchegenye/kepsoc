<!DOCTYPE html>
<html lang="en">
<head>
    <title>Kenya Professional Society Of Criminology | Newsletter Subscription</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <style type="text/css">
        @import url("http://fonts.googleapis.com/css?family=Raleway:200,300,600,700&subset=latin,latin-ext");
        body, table{
            font-family:cursive;
        }
        .appleFooter a {
            color: #999999; font-size:12px;
            text-decoration: none;
        }
    </style>
</head>
<body style="margin: 0; padding: 0; background:rgb(245, 245, 245);">

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <div
                    style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">

            </div>
            <div align="center" style="padding: 0px 15px 0px 15px;">
                <table border="0" cellpadding="0" cellspacing="0" width="620" class="wrapper">
                    <tr>
                        <td style="padding:0px 0px 0px 0px;" class="logo">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td width="100%" align="left">
                                        <a href="#" target="_blank">
                                            <a style="
                                            display: block;
                                            color: #ffffff;
                                            font-size: 16px;
                                            cursor: pointer;"
                                               border="0"><!-- KEPSOC -->

                                            </a>
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" align="center">
    <tr>
        <td style="
        opacity: 0.4;
        padding: 30px 20px;
        background-image:url(http://kepsoc.org/../images/email-head.png);" >
            <a href="http://kepsoc.org" style="text-decoration: none;
    color: #FFFFFF;
    font-weight: 700;
    letter-spacing: 1.2em;font-size: 25px;">KEPSOC </a>
        </td>
    </tr>
    <tr>
        <td bgcolor="color: #B0BEC5;" align="center" style="padding:20px 20px; background-color: #fff;" class="section-padding">
            <table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="left"
                                                style="color: #675C5C; padding-bottom: 1.2em;"
                                                class="padding-copy">Dear Subscriber
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="color: #675C5C; ">
                                                Thank you for subscribing to our Newsletter.
                                                <div style="padding-top: 1.2em;">
                                                    <a href="{{$confirm_url}}" style="text-decoration:none;
                                                    background-color: #1CB1A5;
                                                    color: #fff;
                                                    padding: 5px;
                                                    border-radius: 5px;">
                                                        Click here to confirm your subscription
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left"
                                                style="color: #675C5C; padding-top: 1.2em;"
                                                class="padding-copy">Regards,<br>Kepsoc
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="color: #675C5C; padding-top: 1.1em; ">
                                                -------------------------------------<br>
                                                Subscription token: {{$_token}}
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" align="center" style="background:rgb(249, 249, 249);">
    <tr>
        <td align="center" style="padding:0px 20px;">
            <table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
                <tr>
                    <td>
                        <table border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr>
                                <td align="left"
                                    style="padding: 20px 0 0 0; font-size: 12px; color: #666666;"
                                    class="padding-copy">
                                    <span class="appleFooter" style="color:#666666;">
                                    <a href="www.j-tech.tech">Powered By, j-tech.tech</a> {{date
                                    ('Y')
                                    }}</span><br><span
                                            class="original-only"
                                            style=" color: #444444;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

</body>
</html>