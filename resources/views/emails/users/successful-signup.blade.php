<!DOCTYPE html>
<html lang="en">
<head>
    <title>Kenya Professional Society Of Criminology | Sign Up</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <style type="text/css">
        @import url("http://fonts.googleapis.com/css?family=Raleway:200,300,600,700&subset=latin,latin-ext");
        body, table{
            font-family:cursive;
        }
        .appleFooter a {
            color: #999999; font-size:12px;
            text-decoration: none;
        }
    </style>
</head>
<body style="margin: 0; padding: 0; background:rgb(245, 245, 245);">

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">

            </div>
            <div align="center" >
                <table border="0" cellpadding="0" cellspacing="0" width="620" class="wrapper">
                    <tr>
                        <td style="padding-top: 20px;" class="logo">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td width="100%" align="left">
                                        <a href="#" target="_blank">
                                            <a style="
                                            display: block;
                                            color: #ffffff;
                                            font-size: 16px;
                                            cursor: pointer;"
                                               border="0"><!-- KEPSOC -->

                                            </a>
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" align="center">
    <tr>
        <td style="
        opacity: 0.8;
        padding: 15px 20px;
        background-image:url(http://kepsoc.org/../images/email-head.png);" >
            <a href="http://kepsoc.org" style="text-decoration: none;
    color: #FFFFFF;
    font-weight: 700;
    letter-spacing: 1.2em;font-size: 25px;">KEPSOC </a>
        </td>
    </tr>
    <tr>
        <td bgcolor="color: #B0BEC5;" align="center" style="padding:20px 20px; background-color: #fff;" class="section-padding">
            <table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="left"
                                                style="color: #675C5C; padding-bottom: 1.2em; text-transform:capitalize;"
                                                class="padding-copy">Dear {{$username}},
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="color: #675C5C; ">
                                                Welcome, you have successfully joined us.
                                                <p>
                                                    We hope that this is your email <span style="color: #1CB1A5;">{{$email}}</span>.
                                                </p>
                                                <p>
                                                    <span style="color: red;"> ***Your password is Encrypted for security purposes***</span>.
                                                </p>
                                                <P> Kindly
                                                    <a href="{{$confirm_url}}" style="text-decoration:none;
                                                        background-color: #1CB1A5;
                                                        color: #fff;
                                                        padding: 5px;
                                                        border-radius: 5px;
                                                        margin:10px;">
                                                        click here
                                                    </a>to verify your account. We just need to know that your the owner of this account.
                                                </P>
                                                <p>
                                                    If you have any issues, kindly contact our <a href="http://kepsoc.org/#contact" style="text-decoration:none;">Support</a> Team
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left"
                                                style="color: #675C5C;"
                                                class="padding-copy">Regards,<br>Kepsoc Support Team
                                            </td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" align="center" style="background:rgb(249, 249, 249);">
    <tr>
        <td align="center" style="padding:0px 20px;">
            <table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
                <tr>
                    <td>
                        <table border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr>
                                <td align="left"
                                    style="padding: 20px 0 0 0; font-size: 12px; color: #666666;"
                                    class="padding-copy">
                                    <span class="appleFooter" style="color:#666666;">
                                    <a href="www.j-tech.tech">Powered By, j-tech.tech</a> {{date
                                    ('Y')
                                    }}</span><br><span
                                            class="original-only"
                                            style=" color: #444444;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

</body>
</html>