<!DOCTYPE html>
<html lang="en">
<head>
    <!--##################################### start of HEAD ########################################-->
    <link rel="canonical" href="http://kepsoc.org/login" />

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf_token" content="{{ csrf_token() }}">

    <title>Kenya Professional Society Of Criminology | Login</title>

    <meta name="copyright" content="j-tech is a registered trade mark Co. based in Kenya">
    <meta name="author" content="Chegenye Asumu Jackson">
    <meta name="reply-to" content="chegenyejackson@gmail.com">
    <meta name="web_author" content="https://www.j-tech.tech">
    <meta name="distribution" content="global">
    <meta name="robots" content="none, noindex, nofollow">
    <meta name="language" content="english, kiswahili">
    <meta name="rating" content="general, safe for kids">
    <meta name="identifier-URL" content="https://kepsoc.org/login">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <!-- Favicon -->
    <link rel="icon" href="{{ asset('http://kepsoc.org/favicon.ico') }}">
    <!--####################################### end of head ########################################-->

    <!--####################################### start of CSS #######################################-->
    @include('layouts.partials.css')

            <!--####################################### end of css #########################################-->

    <!--####################################### start of Google Analytics ##########################-->
    {!! Analytics::render() !!}
            <!--####################################### end of Google Analytics ############################-->

</head>

<body>

<!-- ------------------------------------ start of PRELOADER ---------------------------------- -->
@include('layouts.partials.preload')
        <!-- ------------------------------------ end of PRELOADER ------------------------------------ -->


<section id="sign-up">
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>

            <div class="col-md-4 sign-up-form"><img src="../logo.png">
                <br><br>
                <h5 class="text-center">
                    <a href="{{ url('password/reset/'.$token) }}" style="background-color:#0070FF; color: #fff; padding:10px;">
                        Click here to reset your password
                    </a>
                </h5>
                <br>
                <hr>
                <div class="col-md-12 text-center">
                    <p>Having issues? <a href="/#contact">Contact Us</a></p>
                </div>


            </div>

            <div class="col-md-4"></div>


        </div>
    </div>
</section>

<!--####################################### start of SCRIPTS #######################################-->
@include('layouts..partials.scripts')
        <!--####################################### end of SCRIPTS #########################################-->

</body>
</html>