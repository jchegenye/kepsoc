<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User/Members error & success Messages
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user.
    |
    */

    //global
    'unauthorised_access' => 'You are not authorized to access this resource!',

    //authentication
    'account_not_verified' => 'Sorry, your account is NOT verified, Kindly check your Email to do so!',
    'successful_login' => 'Welcome : !',

    //profile
    'successful_profile_update' => 'Your profile has been updated!',
    'unsuccessful_profile_update' => 'Sorry, your profile could NOT be updated!'
];
