<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication ERROR Password Reset User Messages
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user.
    |
    */

    'user_not_found' => 'We cant find a user with that e-mail address!',
    'email_error_connection' => 'Sorry, We could NOT send this form. Kindly check your INTERNET CONNECTIONS and try again!',
    'successful_pass_reset' => 'Your password has been reset!',

    /*
    |--------------------------------------------------------------------------
    | Authentication SUCCESS Password Reset User Messages
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user.
    |
    */

    'user_found' => 'We found your e-mail address, check your e-mail to reset!',
    'unsuccessful_pass_reset'=> 'Sorry, your password reset was unsuccessful!',
];
