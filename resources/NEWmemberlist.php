<!DOCTYPE html>
<html lang="en">
<head>

    <!--##################################### start of HEAD ########################################-->
    <link rel="canonical" href="http://kepsoc.org/members/list" />

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf_token" content="{{ csrf_token() }}">

    <title>{{$user->name or ''}} | Kenya Professional Society Of Criminology</title>

    <meta name="copyright" content="j-tech is a registered trade mark Co. based in Kenya">
    <meta name="author" content="Chegenye Asumu Jackson">
    <meta name="reply-to" content="chegenyejackson@gmail.com">
    <meta name="web_author" content="https://j-tech.tech">
    <meta name="distribution" content="global">
    <meta name="robots" content="none, noindex, nofollow">
    <meta name="googlebot" content="all, index, follow">
    <meta name="language" content="english, kiswahili">
    <meta name="rating" content="general, safe for kids">
    <meta name="identifier-URL" content="https://kepsoc.org/members/list">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <!-- Favicon -->
    <link rel="icon" href="{{ asset('http://kepsoc.org/favicon.ico') }}">
    <!--####################################### end of head ########################################-->

    <!--####################################### start of CSS #######################################-->
    @include('layouts.partials.css')
            <!--####################################### end of css #########################################-->

    <!--####################################### start of Google Analytics ##########################-->
    {!! Analytics::render() !!}
    <!--####################################### end of Google Analytics ############################-->

</head>

<body>

<!-- ------------------------------------ start of PRELOADER ---------------------------------- -->
    @include('layouts.partials.preload')
<!-- ------------------------------------ end of PRELOADER ------------------------------------ -->

<div class="container-fluid">
    <div class="col-md-12">
        <div class="dashboard-menu">
            <div class="col-md-3">
                <a href="{{url('/auth/logout')}}"><i class="fa fa-info">Logout</i></a>
                <a href="{{url('/members/dash')}}"><i class="fa fa-users">DashBoard</i></a>
            </div>
            <div class="col-md-6">
                <p class="text-center"><img src="../../../logo.png"></p>
                <h3 class="text-center">
                    <b>MEMBERS LIST</b>
                </h3>
                <P class="text-center">
                    <i class="fa fa-users "></i>
                </P><hr>
            </div>
            <div class="col-md-3 text-center">
                @if(Session::has('successful'))
                    {{ Session::get('successful') }} {{$name->username}}
                @endif
            </div>
        </div>
    </div>
</div>

    <section id="our-team">
        <div class="container-fluid">
        
        <div class="row">  

            <div class="col-md-4 members-box">  
                <div class="col-md-4">
                    <img class="img-responsive" src="../images/members/sample.jpg" alt="">
                </div>
                <div class="col-md-8">                  
                    <p>                 
                        Meet the brains behind Make It Happen.
                        A group of five dedicated individuals with diverse backgrounds and careers who share the same vision
                    </p>
                    <button class="btn btn-send">Read More</button>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-whatsapp"></i></a>
                </div>
            </div>
            <div class="col-md-4 members-box">  
                <div class="col-md-4">
                    <img class="img-responsive" src="../images/members/sample.jpg" alt="">
                </div>
                <div class="col-md-8">                  
                    <p>                 
                        Meet the brains behind Make It Happen.
                        A group of five dedicated individuals with diverse backgrounds and careers who share the same vision
                    </p>
                    <button class="btn btn-send">Read More</button>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-whatsapp"></i></a>
                </div>
            </div>
            <div class="col-md-4 members-box">  
                <div class="col-md-4">
                    <img class="img-responsive" src="../images/members/sample.jpg" alt="">
                </div>
                <div class="col-md-8">                  
                    <p>                 
                        Meet the brains behind Make It Happen.
                        A group of five dedicated individuals with diverse backgrounds and careers who share the same vision
                    </p>
                    <button class="btn btn-send">Read More</button>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-whatsapp"></i></a>
                </div>
            </div>

        </div>

        <div class="row">  
            <div class="col-md-4 members-box">  
                <div class="col-md-4">
                    <img class="img-responsive" src="../images/members/sample.jpg" alt="">
                </div>
                <div class="col-md-8">                  
                    <p>                 
                        Meet the brains behind Make It Happen.
                        A group of five dedicated individuals with diverse backgrounds and careers who share the same vision
                    </p>
                    <button class="btn btn-send">Read More</button>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-whatsapp"></i></a>
                </div>
            </div>
        </div>

    <!--div class="col-md-12">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">

            <div class="carousel-inner" role="listbox">

                <div class="item active">
                    @ include('layouts.pages.members.1(a)')
                </div>

                div class="item ">
                    @ include('layouts.pages.members.2(b)')
                </div>

                <div class="item ">
                    @ include('layouts.pages.members.3(c)')
                </div>

                <div class="item ">
                    @ include('layouts.pages.members.4(d)')
                </div>

                <div class="item">
                    @ include('layouts.pages.members.5(e)')
                </div>

                <div class="item">
                    @ include('layouts.pages.members.6(f)')
                </div>

                <div class="item">
                    @ include('layouts.pages.members.7(g)')
                </div>

                <div class="item">
                    @ include('layouts.pages.members.8(h)')
                </div>

                <div class="item">
                    @ include('layouts.pages.members.9(h)')
                </div>

                {{--<ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                </ol>--}}

                <a class="left carousel-indicators" href="#myCarousel" role="button" data-slide="prev" style="padding-right:20px; bottom: 50%;">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-indicators" href="#myCarousel" role="button" data-slide="next" style="padding-left:20px; bottom: 50%;">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>
        </div>
        <hr></div-->
        </div>
    </section>

<!-- ------------------------------------- start of Footer ------------------------------------ -->
@include('layouts.partials.pages.footer')
<!-- ------------------------------------- end of Footer -------------------------------------- -->

<!--####################################### start of SCRIPTS #######################################-->
@include('layouts..partials.scripts')
<!--####################################### end of SCRIPTS #########################################-->

</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>

    <!--##################################### start of HEAD ########################################-->
    <link rel="canonical" href="http://kepsoc.org/members/list" />

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf_token" content="{{ csrf_token() }}">

    <title>{{$user->name or ''}} | Kenya Professional Society Of Criminology</title>

    <meta name="copyright" content="j-tech is a registered trade mark Co. based in Kenya">
    <meta name="author" content="Chegenye Asumu Jackson">
    <meta name="reply-to" content="chegenyejackson@gmail.com">
    <meta name="web_author" content="https://j-tech.tech">
    <meta name="distribution" content="global">
    <meta name="robots" content="none, noindex, nofollow">
    <meta name="googlebot" content="all, index, follow">
    <meta name="language" content="english, kiswahili">
    <meta name="rating" content="general, safe for kids">
    <meta name="identifier-URL" content="https://kepsoc.org/members/list">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <!-- Favicon -->
    <link rel="icon" href="{{ asset('http://kepsoc.org/favicon.ico') }}">
    <!--####################################### end of head ########################################-->

    <!--####################################### start of CSS #######################################-->
    @include('layouts.partials.css')
    <!--####################################### end of css #########################################-->

    <!--####################################### start of Google Analytics ##########################-->
    {!! Analytics::render() !!}
    <!--####################################### end of Google Analytics ############################-->

</head>

<body>

<!-- ------------------------------------ start of PRELOADER ---------------------------------- -->
@include('layouts.partials.preload')
<!-- ------------------------------------ end of PRELOADER ------------------------------------ -->

<div class="container-fluid our-team-spacing">

    <div class="dashboard-menu">
        <div class="col-md-4">
            <a href="{{url('/auth/logout')}}"><i class="fa fa-power-off">Logout</i></a>
            <a href="{{url('/members/dash')}}"><i class="fa fa-dashboard">DashBoard</i></a>
            <a href="{{url('members/profile/' . $user->uid)}}"><i class="fa fa-user">My Profile</i></a>
        </div>
        <div class="col-md-4">
            <p class="text-center"><img src="../../../logo.png"></p>
            <h3 class="text-center">
                <b>MEMBERS LIST</b>
            </h3>
            <P class="text-center">
                <i class="fa fa-users "></i>
            </P><hr>
        </div>
        <div class="col-md-4 text-center">
            @if(Session::has('successful'))
            {{ Session::get('successful') }} {{$user->username}}
            @endif
        </div>
    </div>

    @foreach($member as $members)
    {{$members->about_you}}<br>
    @endforeach

    <section id="our-team">
        <div class="col-md-12">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">

                    <!-- 1. PAGE -->
                    <div class="item active">
                        @include('layouts.pages.members.1(a)')
                    </div>

                    <!-- 2. PAGE -->
                    <div class="item ">
                        @include('layouts.pages.members.2(b)')
                    </div>

                    <!-- 3. PAGE -->
                    <div class="item ">
                        @include('layouts.pages.members.3(c)')
                    </div>

                    <!-- 4. PAGE -->
                    <div class="item ">
                        @include('layouts.pages.members.4(d)')
                    </div>

                    <!-- 5. PAGE -->
                    <div class="item">
                        @include('layouts.pages.members.5(e)')
                    </div>

                    <!-- 6. PAGE -->
                    <div class="item">
                        @include('layouts.pages.members.6(f)')
                    </div>

                    <!-- 7. PAGE -->
                    <div class="item">
                        @include('layouts.pages.members.7(g)')
                    </div>

                    <!-- 8. PAGE -->
                    <div class="item">
                        @include('layouts.pages.members.8(h)')
                    </div>

                    <div class="item">
                        @include('layouts.pages.members.9(i)')
                    </div>

                    <!-- Indicators -->
                    {{--<ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                    </ol>--}}

                    <!-- Left and right controls -->
                    <a class="left carousel-indicators" href="#myCarousel" role="button" data-slide="prev" style="padding-right:20px; bottom: 50%;">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-indicators" href="#myCarousel" role="button" data-slide="next" style="padding-left:20px; bottom: 50%;">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>

                </div>
            </div>
            <hr></div>
    </section>
</div>
<!-- ------------------------------------- start of Footer ------------------------------------ -->
@include('layouts.partials.pages.footer')
<!-- ------------------------------------- end of Footer -------------------------------------- -->

<!--####################################### start of SCRIPTS #######################################-->
@include('layouts..partials.scripts')
<!--####################################### end of SCRIPTS #########################################-->

</body>
</html>