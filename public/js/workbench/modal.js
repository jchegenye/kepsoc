$(function() {
  //Improvements to the trash modal.
  $('#trashModal').on('show.bs.modal', function (event) {
    console.log('ticket');

    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('email') // Extract info from data-* attributes
    var title = button.data('title')
    var nid = button.data('nid')

    console.log(nid)
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)

    modal.find('.modal-title').text('Trash: '+title)
    modal.find('.modal-body #recipient-email').val(recipient)
    modal.find('.modal-body #article-nid').val(nid)

    //set href
    $('#delete-button').attr('href','http://www.hivisasa.com/article/' + nid +'/delete');
  });
  
  var width = document.documentElement.clientWidth;

  var workbench_left = $('#workbench-left');
  
  if (workbench_left.length) {
    var main_width = width-100;

    $('#workbench-right').css( "width", main_width );
  }

  //Highlight active menu link on the workbench
  var current_url = location.href.toLowerCase();

  if (current_url.search('photo') != -1) {
    disable_active_link();

    $('.dashboard-link-photos').addClass("workbench-active-link");
  }

  if (current_url.search('workbench/content') != -1){
    disable_active_link();

    $('.dashboard-link-content').addClass("workbench-active-link");
  }
  
  if (current_url.search('workbench/report') != -1){
    disable_active_link();

    $('.dashboard-link-report').addClass("workbench-active-link");
  }

  if (current_url.search('workbench/photos') != -1){
    disable_active_link();

    $('.dashboard-link-photos').addClass("workbench-active-link");
  }
  
  if (current_url.search('workbench/users') != -1){
    disable_active_link();

    $('.dashboard-link-user').addClass("workbench-active-link");
  }

  if (current_url.search('payment') != -1){
    disable_active_link();

    $('.dashboard-link-payment').addClass("workbench-active-link");
  }

  
  if(current_url === 'http://'+location.host+'/workbench'){
    disable_active_link();
    
    $('.dashboard-link-workbench').addClass("workbench-active-link");
  }

  function disable_active_link(){
    //Get the current active link.
    var active_link = $('.workbench-active-link');

    $(active_link).removeClass('workbench-active-link');
  }

  $('.reason-for-trashing,.trashing-reason').change(function(){

    if ($('.trashing-other-reason').css('display') === 'block') {
      $('.trashing-other-reason').toggle();
    };

    if ($(this).val() == 'other') {
      $('.trashing-other-reason').toggle();
    };

  });

  //Show the right location on the workbench
  var article_location = $("body").data('article-location');

  if (article_location) {
    $('.selected_location option[value='+article_location+']').attr("selected",true);
  };


  
});
