/* var eventapp = angular.module('EventManager',['ui.bootstrap','ngTagsInput','ngFileUpload','ngCropper','uploadphoto','ngImgCrop']);
 var hivisasa = angular.module('hivisasaApp',['EventManager','ui.bootstrap',
 	'ngTagsInput','ngFileUpload','ngCropper','uploadphoto','ngImgCrop']);*/

workbench.factory('RoutingConfig', function () {
	// The api and the app are currently in the same folder, so move up one folder to access it
	return {
        baseUrl: 'http://'+window.location.hostname+'/',
	};
});

workbench.factory('CreateEventService',[ '$http','RoutingConfig','$q', function ($http,RoutingConfig,$q) {
	// The api and the app are currently in the same folder, so move up one folder to access it
	return {
		getLocation: function($query){
			var request = $http.get(RoutingConfig.baseUrl + "locations?query="+ $query)
			var deferred = $q.defer();
			deferred.resolve(request)
			return deferred.promise
		},
		createEvent:function(events){
			var eventrequest = $http.post(RoutingConfig.baseUrl + "events/new", events)
			return eventrequest;
		},
		getEvent:function(id){
			var eventrequest = $http.get(RoutingConfig.baseUrl + "events/details/"+id)
			return eventrequest;
		}
	}
}]);

workbench.controller('CreateEventController',function($scope, $http,RoutingConfig, CreateEventService,$timeout,Upload,cfpLoadingBar){

	$scope.error ={}
	$scope.success ={}
	$scope.eventdetails ={}
	$scope.category_selected = true
	$scope.eventdetails = {}

	$scope.tab_status = {
		eventdetails: true,
		images:false,
		ticketing:false,
		additional: false,
		where: false
	}

	angular.element(document).ready(function () {
		var id = $("#event_id").val()
		//hide location
		$('.location').hide()
		if(id != 0){
			$scope.category_selected = false
			CreateEventService.getEvent(id).success(function(response){
    			$scope.eventdetails = response
    			$scope.eventdetails.location = []
    			$scope.eventdetails.organizers = []

    			$scope.eventdetails.location.push({"text":$scope.eventdetails.venue})
    			angular.forEach($scope.eventdetails.organiser,function(value,index){
    				$scope.eventdetails.organizers.push({"text":value})

    			})
    		})
		var bodyEditor = new MediumEditor('.event-description', {
                      buttonLabels: 'fontawesome',
                      placeholder: false,
                      contentEditable: true
                 });
		$('.event-description').html($scope.eventdetails.description)
		}else{
			var bodyEditor = new MediumEditor('.event-description', {

                      buttonLabels: 'fontawesome',
                       contentEditable: true
                 });

		}



	});
 $scope.eventdetails.country = "ke"
	$scope.$watch('eventdetails.county', function(val){
			if($scope.eventdetails.county == 'nairobi'){
				$('.location').show();
			}else{
				$('.location').hide();
			}
	})
	$scope.eventdetails.recurring = false
	$scope.step_one = function(){

		if($scope.eventdetails.name == "" || !$scope.eventdetails.name ){
			$scope.error_handling("Name of event is Required")
			return
		}
		//get description
		$scope.eventdetails.description = $('.event-description').html()

		if($scope.eventdetails.description == "" ||  !$scope.eventdetails.description){
			$scope.error_handling("Description of event is Required")
			return
		}
		if($scope.eventdetails.venue == ""||  !$scope.eventdetails.venue){
			$scope.error_handling("Please Specify a venue")
			return
		}

		$scope.tab_status.where = true

	}

	$scope.step_two = function(){
		if($scope.eventdetails.county == "" ||  !$scope.eventdetails.county){
			$scope.error_handling("Please Specify county")
			return
		}
		if(!$scope.eventdetails.recurring){
			$scope.eventdetails.start_date = $('input[name="estart_date"]').val()
			$scope.eventdetails.start_time = $('#starttime').val()
			$scope.eventdetails.end_time = $('#endtime').val()
			$scope.eventdetails.end_date = $('input[name="eend_date"]').val()

			if($scope.eventdetails.start_date == "" ||  !$scope.eventdetails.start_date){
				$scope.error_handling("Please Specify Start date")
				return
			}
			if($scope.eventdetails.start_time == "" ||  !$scope.eventdetails.start_time){
				$scope.error_handling("Please Specify Start time ")
				return
			}
		}else{
			if($scope.eventdetails.frequency == "" || !$scope.eventdetails.frequency ){
				$scope.error_handling("Please Specify Frequency ")
				return
			}
		}


		$scope.tab_status.additional = true
	}

	$scope.getEvent = function(id){
		alert(id)
	}


	$scope.validate_event = function(){
		$scope.eventdetails.start_date = $('input[name="estart_date"]').val()
		$scope.eventdetails.start_time = $('#starttime').val()
		$scope.eventdetails.end_time = $('#endtime').val()
		$scope.eventdetails.end_date = $('input[name="eend_date"]').val()
		if($scope.eventdetails.county == "" ||  !$scope.eventdetails.county){
			$scope.error_handling("Please Specify county")
			return
		}
		if(!$scope.eventdetails.recurring){
			if($scope.eventdetails.start_date == "" ||  !$scope.eventdetails.start_date){
				$scope.error_handling("Please Specify Start date")
				return
			}
			if($scope.eventdetails.start_time == "" ||  !$scope.eventdetails.start_time){
				$scope.error_handling("Please Specify Start time ")
				return
			}

			if($scope.eventdetails.county == "" ||  !$scope.eventdetails.county){
				$scope.error_handling("Please Specify county")
				return
			}
			if($scope.eventdetails.start_date == "" ||  !$scope.eventdetails.start_date){
				$scope.error_handling("Please Specify Start date")
				return
			}
			if($scope.eventdetails.start_time == "" ||  !$scope.eventdetails.start_time){
				$scope.error_handling("Please Specify Start time ")
				return
			}
		}else{
			if($scope.eventdetails.frequency == "" || !$scope.eventdetails.frequency ){
				$scope.error_handling("Please Specify Frequency ")
				return
			}
			$scope.eventdetails.start_time = $('#starttime_f').val()
			$scope.eventdetails.end_time = $('#endtime_f').val()
		}

	}

	$scope.save_event = function(){
		$scope.validate_event()
		/*CreateEventService.createEvent($scope.eventdetails).success(function(response){
			$scope.eventdetails = response
			$scope.success_handling("Event  Created Successfull")
			$scope.tab_status.images= true
		})*/
		$scope.tab_status.images= true
	}

$scope.save_event_without_image = function(){
	cfpLoadingBar.start();
	cfpLoadingBar.inc();
	CreateEventService.createEvent($scope.eventdetails).success(function(response){
		$scope.eventdetails = response
		$scope.success_handling("Event  Created Successfull")
		cfpLoadingBar.complete()

		window.location.href=RoutingConfig.baseUrl+response.country+"/"+response.default_location+"/event/"+response.event_id

	})


}

	$scope.upload = function (files) {

        if (files && files.length) {
        	//$scope.tab_status.ticketing = true
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                Upload.upload({
                    url: RoutingConfig.baseUrl +'events/edit',
                    fields: $scope.eventdetails,
                    headers: {'Content-Type': file.type},
                    data:file,
                    file: file
                }).progress(function (evt) {
									cfpLoadingBar.start();
									cfpLoadingBar.inc();
                   // var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    //console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
                }).success(function (data, status, headers, config) {
                   // console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                   $scope.success_handling("Image Uploaded Successfull")

								 	 cfpLoadingBar.complete();
								   window.location.href=RoutingConfig.baseUrl+data.country+"/"+data.default_location+"/event/"+data.event_id

                }).error(function(data, status, headers, config){

                });
            }
        }
    };
	$scope.eventdetails.location = []

	$scope.get_location = function($query){
	  return CreateEventService.getLocation($query)
	}

	$scope.update_to_venue = function(){

		if($scope.eventdetails.location.length > 1){
			$scope.error.status = true
			$scope.error.msg = "You can only select one location at a time"
			$scope.eventdetails.location = []
			var duration = 5000
			$timeout(function () {
					$scope.error = {};

				}, duration);
			return
		}

		if($scope.eventdetails.location.length == 1){
			$scope.eventdetails.venue = $scope.eventdetails.location[0].text
		}

	}

	$scope.error_handling = function(msg){
		$scope.error.status = true
			$scope.error.msg = msg
			var duration = 5000
			$timeout(function () {
					$scope.error = {};

				}, duration);
	}

	$scope.success_handling = function(msg){
		$scope.success.status = true
			$scope.success.msg = msg
			var duration = 5000
			$timeout(function () {
					$scope.success = {};

				}, duration);
	}





})
